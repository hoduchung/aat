using System;
using System.Data;
using AAT.Model;
using System.Data.SqlClient;
using AAT.Data.Utils;
using AAT.Model.Utils;
using System.Collections.Generic;

namespace AAT.Data
{
    public class TrainerRepository
    {
        public int CreateNewTrainer(TrainerDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@TrainerId", SqlDbType = SqlDbType.Text, Value = dto.TrainerId },
                    new SqlParameter() { ParameterName = "@TrainerName", SqlDbType = SqlDbType.Text, Value = dto.TrainerName }
                };

                RepositoryHelper repository = new RepositoryHelper();
                var re = repository.ExecuteScalar("Trainer_Create", sp);
                return (re == null) ? -1 : int.Parse(re.ToString());
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateTrainer(TrainerDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@TrainerId", SqlDbType = SqlDbType.Text, Value = dto.TrainerId },
                    new SqlParameter() { ParameterName = "@TrainerName", SqlDbType = SqlDbType.Text, Value = dto.TrainerName }
                };

                RepositoryHelper repository = new RepositoryHelper();

                return repository.ExecuseNonQuery("Trainer_Update", sp);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<TrainerDto> GetTrainerList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                List<TrainerDto> re = new List<TrainerDto>();
                List<SqlParameter> sp = new List<SqlParameter>();

                if (criterial.Page != null && criterial.PageSize != null)
                {
                    sp.Add(new SqlParameter() { ParameterName = "@recordPerPage", SqlDbType = SqlDbType.Int, Value = criterial.PageSize });
                    sp.Add(new SqlParameter() { ParameterName = "@page", SqlDbType = SqlDbType.Int, Value = criterial.Page });
                };

                foreach (KeyDto dto in criterial.SearchFieldList)
                {
                    sp.Add(new SqlParameter()
                    {
                        ParameterName = "@" + dto.Key.ToString(),
                        Value = dto.Value
                    });
                }

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("Trainer_GetList", sp);

                if (result != null && result.Tables[0] != null)
                {
                    totalRecord = (int)result.Tables[1].Rows[0][0];

                    re = ConvertDataTableToTrainer(result.Tables[0]);
                }
                else
                {
                    totalRecord = 0;
                }
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<TrainerDto> ConvertDataTableToTrainer(DataTable dt)
        {
            List<TrainerDto> re = new List<TrainerDto>();
            TrainerDto dto;
            int index = 1;

            foreach (DataRow row in dt.Rows)
            {
                dto = new TrainerDto();
                if (!String.IsNullOrWhiteSpace(row["TrainerId"].ToString()))
                {
                    dto.TrainerId = row["TrainerId"].ToString();
                }

                if (row.Table.Columns.Contains("AcknowledgementDate") &&
                    !String.IsNullOrWhiteSpace(row["AcknowledgementDate"].ToString()))
                {
                    dto.AcknowledgementDate = DateHelper.ConvertToDate(row["AcknowledgementDate"]);
                }

                dto.RowNum = index++;
                
                dto.TrainerName = row["TrainerName"].ToString();
                re.Add(dto);
            }
            return re;
        }

    }
}
