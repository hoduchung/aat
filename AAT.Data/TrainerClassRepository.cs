using System;
using System.Data;
using AAT.Model;
using System.Data.SqlClient;
using AAT.Data.Utils;
using AAT.Model.Utils;
using System.Collections.Generic;

namespace AAT.Data
{
    public class TrainerClassRepository
    {
        public bool CreateNewTrainerClass(TrainerClassDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@TrainerId", SqlDbType = SqlDbType.Text, Value = dto.TrainerId },
                    new SqlParameter() { ParameterName = "@ClassId", SqlDbType = SqlDbType.Text, Value = dto.ClassId }
                };

                RepositoryHelper repository = new RepositoryHelper();

                var re = repository.ExecuteScalar("TrainerClass_Create", sp);
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool DeleteTrainerClass(string ClassId, string trainerId)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@ClassId", Value = ClassId },
                    new SqlParameter() { ParameterName = "@TrainerId", Value = trainerId }

                };

                RepositoryHelper repository = new RepositoryHelper();

                var re = repository.ExecuseNonQuery("TrainerClass_Delete", sp);
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateTrainerClass(TrainerClassDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@TrainerClassId", SqlDbType = SqlDbType.Decimal, Value = dto.TrainerClassId },
                    new SqlParameter() { ParameterName = "@TrainerId", SqlDbType = SqlDbType.Text, Value = dto.TrainerId },
                    new SqlParameter() { ParameterName = "@ClassId", SqlDbType = SqlDbType.Text, Value = dto.ClassId }
                };

                RepositoryHelper repository = new RepositoryHelper();

                return repository.ExecuseNonQuery("TrainerClass_Update", sp);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool DeleteTrainerClass(int trainerclassID)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@TrainerClassId ", SqlDbType = SqlDbType.Int, Value = trainerclassID }
                };

                RepositoryHelper repository = new RepositoryHelper();

                var re = repository.ExecuseNonQuery("TrainerClass_Delete", sp);
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }
    }
}
