using System;
using System.Data;
using AAT_SIA.Model;
using System.Data.SqlClient;
using System.Collections.Generic;
using AAT.Data.Utils;
using AAT.Model.Utils;
using AAT.Model;

namespace AAT_SIA.Data
{
    public class AcknowledgementRepository
    {
        public int CreateNewAcknowledgement(string classId, string trainerId)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@TrainerId", SqlDbType = SqlDbType.Text, Value = trainerId },
                    new SqlParameter() { ParameterName = "@ClassId", SqlDbType = SqlDbType.Text, Value = classId }
                };

                RepositoryHelper repository = new RepositoryHelper();

                var re = repository.ExecuteScalar("Acknowledgement_Create", sp);
                return (re == null) ? -1 : int.Parse(re.ToString());
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateAcknowledgement(AcknowledgementDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@AcknowledgementId", SqlDbType = SqlDbType.Int, Value = dto.AcknowledgementId },
                    new SqlParameter() { ParameterName = "@AcknowledgementDate", SqlDbType = SqlDbType.DateTime, Value = dto.AcknowledgementDate },
                    new SqlParameter() { ParameterName = "@TrainerId", SqlDbType = SqlDbType.Text, Value = dto.TrainerId },
                    new SqlParameter() { ParameterName = "@ClassId", SqlDbType = SqlDbType.Text, Value = dto.ClassId }
                };

                RepositoryHelper repository = new RepositoryHelper();

                return repository.ExecuseNonQuery("Acknowledgement_Update", sp);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool DeleteAcknowledgement(int acknowledgementID)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
{
new SqlParameter() { ParameterName = "@AcknowledgementId ", SqlDbType = SqlDbType.Int, Value = acknowledgementID }
};

                RepositoryHelper repository = new RepositoryHelper();

                var re = repository.ExecuseNonQuery("Acknowledgement_Delete", sp);
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<AcknowledgementDto> GetAcknowledgementList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                List<AcknowledgementDto> re = new List<AcknowledgementDto>();
                List<SqlParameter> sp = new List<SqlParameter>();

                if (criterial.Page != null && criterial.PageSize != null)
                {
                    sp.Add(new SqlParameter() { ParameterName = "@recordPerPage", SqlDbType = SqlDbType.Int, Value = criterial.PageSize });
                    sp.Add(new SqlParameter() { ParameterName = "@page", SqlDbType = SqlDbType.Int, Value = criterial.Page });
                };

                foreach (KeyDto dto in criterial.SearchFieldList)
                {
                    sp.Add(new SqlParameter()
                    {
                        ParameterName = "@" + dto.Key.ToString(),
                        Value = dto.Value
                    });
                }

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("Acknowledgement_GetList", sp);

                if (result != null && result.Tables[0] != null)
                {
                    totalRecord = (int)result.Tables[1].Rows[0][0];

                    re = ConvertDataTableToAcknowledgement(result.Tables[0]);
                }
                else
                {
                    totalRecord = 0;
                }
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<AcknowledgementDto> ConvertDataTableToAcknowledgement(DataTable dt)
        {
            List<AcknowledgementDto> re = new List<AcknowledgementDto>();
            AcknowledgementDto dto;

            foreach (DataRow row in dt.Rows)
            {
                dto = new AcknowledgementDto();
                if (!String.IsNullOrWhiteSpace(row["AcknowledgementId"].ToString()))
                {
                    dto.AcknowledgementId = (int)row["AcknowledgementId"];
                }
                dto.AcknowledgementDate = DateHelper.ConvertToDate(row["AcknowledgementDate"]);
                dto.TrainerId = row["TrainerId"].ToString();
                dto.ClassId = row["ClassId"].ToString();
                if (row.Table.Columns.Contains("RowNum"))
                {
                    if (!String.IsNullOrWhiteSpace(row["RowNum"].ToString()))
                    {
                        dto.RowNum = int.Parse(row["RowNum"].ToString());
                    }
                }
                re.Add(dto);
            }
            return re;
        }

    }
}
