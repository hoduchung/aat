﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using AAT.Model.Utils;

namespace AAT.Data.Utils
{
	public class RepositoryHelper
	{
        private SqlConnection m_connection = null;

        #region Properties
        protected SqlConnection Connect
        {
            get
            {
                if (m_connection == null)
                {
                    m_connection = new SqlConnection(ConfigHelper.ConnectionStr);
                }
                if (m_connection.State == ConnectionState.Closed)
                {
                    m_connection.Open();
                }
                return m_connection;
            }
        }
        #endregion Properties

        public RepositoryHelper()
		{
			
		}

        public DataSet ExecuseDataSet(string spName, List<SqlParameter> parameters)
		{
			try
			{
				DataSet ds = new DataSet();

				SqlCommand cmd = new SqlCommand(spName, Connect);
				cmd.CommandType = CommandType.StoredProcedure;

                if (parameters != null)
                {
                    foreach (SqlParameter param in parameters)
                    {
                        cmd.Parameters.Add(param);
                    }
                }

				SqlDataAdapter da = new SqlDataAdapter(cmd);
				da.Fill(ds);
                Connect.Close();
				return ds;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        public bool ExecuseNonQuery(string spName, List<SqlParameter> parameters)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(spName, Connect);
                cmd.CommandType = CommandType.StoredProcedure;

                if (parameters != null)
                {
                    foreach (SqlParameter param in parameters)
                    {
                        cmd.Parameters.Add(param);
                    }
                }

                cmd.ExecuteNonQuery();
                Connect.Close();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object ExecuteScalar(string spName, List<SqlParameter> parameters)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(spName, Connect);
                cmd.CommandType = CommandType.StoredProcedure;

                if (parameters != null)
                {
                    foreach (SqlParameter param in parameters)
                    {
                        cmd.Parameters.Add(param);
                    }
                }

                var re = cmd.ExecuteScalar();
                
                Connect.Close();
                return re;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
