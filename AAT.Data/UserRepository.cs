using System;
using System.Data;
using AAT.Model;
using System.Data.SqlClient;
using AAT.Data.Utils;
using AAT.Model.Utils;
using System.Collections.Generic;
using System.Linq;

namespace AAT.Data
{
    public class UserRepository
    {
        public int CreateNewUser(UserDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@LoginName", SqlDbType = SqlDbType.Text, Value = dto.LoginName },
                    new SqlParameter() { ParameterName = "@UserTypeId", SqlDbType = SqlDbType.Int, Value = dto.UserType.GetHashCode() },
                    new SqlParameter() { ParameterName = "@StatusId", SqlDbType = SqlDbType.Int, Value = dto.Status.GetHashCode() },
                    new SqlParameter() { ParameterName = "@RoleId", SqlDbType = SqlDbType.Int, Value = dto.Role.GetHashCode() },
                    new SqlParameter() { ParameterName = "@DepartmentId", SqlDbType = SqlDbType.Text, Value = dto.DepartmentId },
                    new SqlParameter() { ParameterName = "@Name", SqlDbType = SqlDbType.Text, Value = dto.Name },
                    new SqlParameter() { ParameterName = "@Password", SqlDbType = SqlDbType.Text, Value = dto.Password },
                    new SqlParameter() { ParameterName = "@Email", SqlDbType = SqlDbType.Text, Value = dto.Email },
                    new SqlParameter() { ParameterName = "@Phone", SqlDbType = SqlDbType.Text, Value = dto.Phone }

                };

                RepositoryHelper repository = new RepositoryHelper();

                var re = repository.ExecuteScalar("User_Create", sp);
                return (re == null) ? -1 : int.Parse(re.ToString());
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateUser(UserDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@UserId", SqlDbType = SqlDbType.Int, Value = dto.UserId },
                    new SqlParameter() { ParameterName = "@LoginName", SqlDbType = SqlDbType.Text, Value = dto.LoginName },
                    new SqlParameter() { ParameterName = "@UserTypeId", SqlDbType = SqlDbType.Int, Value = dto.UserType.GetHashCode() },
                    new SqlParameter() { ParameterName = "@StatusId", SqlDbType = SqlDbType.Int, Value = dto.Status.GetHashCode() },
                    new SqlParameter() { ParameterName = "@RoleId", SqlDbType = SqlDbType.Int, Value = dto.Role.GetHashCode() },
                    new SqlParameter() { ParameterName = "@DepartmentId", SqlDbType = SqlDbType.Text, Value = dto.DepartmentId },
                    new SqlParameter() { ParameterName = "@Name", SqlDbType = SqlDbType.Text, Value = dto.Name },
                    new SqlParameter() { ParameterName = "@Password", SqlDbType = SqlDbType.Text, Value = dto.Password },
                    new SqlParameter() { ParameterName = "@Email", SqlDbType = SqlDbType.Text, Value = dto.Email },
                    new SqlParameter() { ParameterName = "@PassIncorrectCount", Value = dto.PassIncorrectCount },

                    
                    new SqlParameter() { ParameterName = "@Phone", SqlDbType = SqlDbType.Text, Value = dto.Phone }

                };

                RepositoryHelper repository = new RepositoryHelper();

                return repository.ExecuseNonQuery("User_Update", sp);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public UserDto GetUserLogin(string loginName)
        {
            try
            {
                UserDto re = null;
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@LoginName", SqlDbType = SqlDbType.Text, Value= loginName}
                };

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("User_GetByLoginName", sp);

                if (result != null && result.Tables[0] != null && result.Tables[0].Rows.Count > 0)
                {
                    var userList = ConvertDataTableToUser(result.Tables[0]);
                    re = userList[0];
                }

                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }



        public List<UserDto> GetUserList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                List<UserDto> re = new List<UserDto>();
                List<SqlParameter> sp = new List<SqlParameter>();

                if (criterial.Page != null && criterial.PageSize != null)
                {
                    sp.Add(new SqlParameter() { ParameterName = "@recordPerPage", SqlDbType = SqlDbType.Int, Value = criterial.PageSize });
                    sp.Add(new SqlParameter() { ParameterName = "@page", SqlDbType = SqlDbType.Int, Value = criterial.Page });
                };

                foreach (KeyDto dto in criterial.SearchFieldList)
                {
                    sp.Add(new SqlParameter()
                    {
                        ParameterName = "@" + dto.Key.ToString(),
                        Value = dto.Value
                    });
                }

                List<KeyDto> roleFilters = FilterHelper.Instance.GetFilterUserListPage();
                foreach (var filter in roleFilters)
                {
                    var existed = criterial.SearchFieldList.FirstOrDefault(x => StringHelper.IsEquals(x.Key.ToString(), filter.Key.ToString()));

                    if(existed==null)
                    {
                        sp.Add(new SqlParameter() { ParameterName = "@" + filter.Key, Value = filter.Value });
                    }
                }

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("User_GetList", sp);

                if (result != null && result.Tables[0] != null)
                {
                    totalRecord = (int)result.Tables[1].Rows[0][0];

                    re = ConvertDataTableToUser(result.Tables[0]);
                }
                else
                {
                    totalRecord = 0;
                }
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<UserDto> ConvertDataTableToUser(DataTable dt)
        {
            List<UserDto> re = new List<UserDto>();
            UserDto dto;

            foreach (DataRow row in dt.Rows)
            {
                dto = new UserDto();
                dto.UserId = (int)row["UserId"];
                dto.LoginName = row["LoginName"].ToString();
                dto.Name = row["Name"].ToString();
                dto.Password = row["Password"].ToString();
                dto.Phone = row["Phone"].ToString();
                dto.Email = row["Email"].ToString();
                if (!String.IsNullOrWhiteSpace(row["UserTypeId"].ToString()))
                {
                    dto.UserType = (UserType)(int)row["UserTypeId"];
                }
                dto.Status = (StatusType)(short)row["StatusId"];
                dto.Role = (UserRole)(int)row["RoleId"];

                dto.DepartmentId = row["DepartmentId"].ToString();
                if (row.Table.Columns.Contains("Department"))
                {
                    dto.Department = row["Department"].ToString();
                }

                if (row.Table.Columns.Contains("RowNum"))
                {
                    if (!String.IsNullOrWhiteSpace(row["RowNum"].ToString()))
                    {
                        dto.RowNum = int.Parse(row["RowNum"].ToString());
                    }
                }

                if (row.Table.Columns.Contains("PassIncorrectCount"))
                {
                    dto.PassIncorrectCount = int.Parse(row["PassIncorrectCount"].ToString());
                }
                
                re.Add(dto);
            }
            return re;
        }

    }
}
