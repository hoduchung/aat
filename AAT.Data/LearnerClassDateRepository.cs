using System;
using System.Data;
using AAT.Model;
using System.Data.SqlClient;
using AAT.Data.Utils;
using AAT.Model.Utils;
using System.Collections.Generic;

namespace AAT.Data
{
    public class LearnerClassDateRepository
    {
        public decimal CreateNewLearnerClassDate(LearnerClassDateDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@StaffId", SqlDbType = SqlDbType.Text, Value = dto.StaffId },
                    new SqlParameter() { ParameterName = "@ClassId", SqlDbType = SqlDbType.Text, Value = dto.ClassId },
                    new SqlParameter() { ParameterName = "@ClassDate", SqlDbType = SqlDbType.DateTime, Value = dto.ClassDate },
                    new SqlParameter() { ParameterName = "@CheckInTime", SqlDbType = SqlDbType.DateTime, Value = dto.CheckInTime },
                    new SqlParameter() { ParameterName = "@Remark", SqlDbType = SqlDbType.Text, Value = dto.Remark },
                    new SqlParameter() { ParameterName = "@UpdateBy", SqlDbType = SqlDbType.Text, Value = dto.UpdateBy }

                };

                RepositoryHelper repository = new RepositoryHelper();

                var re = repository.ExecuteScalar("LearnerClassDate_Create", sp);
                return (re == null) ? -1 : int.Parse(re.ToString());
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

       public bool UpdateLearnerClassDate(LearnerClassDateDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@LearnerClassDateId", SqlDbType = SqlDbType.Decimal, Value = dto.LearnerClassDateId },
                    new SqlParameter() { ParameterName = "@StaffId", SqlDbType = SqlDbType.Text, Value = dto.StaffId },
                    new SqlParameter() { ParameterName = "@ClassId", SqlDbType = SqlDbType.Text, Value = dto.ClassId },
                    new SqlParameter() { ParameterName = "@ClassDate", SqlDbType = SqlDbType.DateTime, Value = dto.ClassDate },
                    new SqlParameter() { ParameterName = "@CheckInTime", SqlDbType = SqlDbType.DateTime, Value = dto.CheckInTime },
                    new SqlParameter() { ParameterName = "@Remark", SqlDbType = SqlDbType.Text, Value = dto.Remark },
                    new SqlParameter() { ParameterName = "@UpdateBy", SqlDbType = SqlDbType.Text, Value = dto.UpdateBy }
                };

                RepositoryHelper repository = new RepositoryHelper();

                return repository.ExecuseNonQuery("LearnerClassDate_Update", sp);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateSkiesExported(string learnerClassDateIds)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@LearnerClassDateIds", SqlDbType = SqlDbType.Text, Value = learnerClassDateIds }
                };

                RepositoryHelper repository = new RepositoryHelper();

                return repository.ExecuseNonQuery("LearnerClassDate_UpdateSkiesExported", sp);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<LearnerClassDateDto> GetLearnerClassDateList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                List<LearnerClassDateDto> re = new List<LearnerClassDateDto>();
                List<SqlParameter> sp = new List<SqlParameter>();

                if (criterial.Page != null && criterial.PageSize != null)
                {
                    sp.Add(new SqlParameter() { ParameterName = "@recordPerPage", SqlDbType = SqlDbType.Int, Value = criterial.PageSize });
                    sp.Add(new SqlParameter() { ParameterName = "@page", SqlDbType = SqlDbType.Int, Value = criterial.Page });
                };

                foreach (KeyDto dto in criterial.SearchFieldList)
                {
                    sp.Add(new SqlParameter()
                    {
                        ParameterName = "@" + dto.Key.ToString(),
                        Value = dto.Value
                    });
                }

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("LearnerClassDate_GetList", sp);

                if (result != null && result.Tables[0] != null)
                {
                    totalRecord = (int)result.Tables[1].Rows[0][0];

                    re = ConvertDataTableToLearnerClassDate(result.Tables[0]);
                }
                else
                {
                    totalRecord = 0;
                }
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<LearnerClassDateDto> GetSkiesReport()
        {
            try
            {
                List<LearnerClassDateDto> re = new List<LearnerClassDateDto>();
                List<SqlParameter> sp = new List<SqlParameter>();

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("LearnerClassDate_SkiesReport", sp);

                if (result != null && result.Tables[0] != null)
                {
                    re = ConvertDataTableToLearnerClassDate(result.Tables[0]);
                }
               
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public DataSet GetLearnerClassPresents(string classId, string staffId)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>();
                sp.Add(new SqlParameter() { ParameterName = "@StaffID", Value = staffId });
                sp.Add(new SqlParameter() { ParameterName = "@ClassID", Value = classId });

                RepositoryHelper repository = new RepositoryHelper();
                DataSet re = repository.ExecuseDataSet("LearnerClass_Attending", sp);

                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<LearnerClassDateDto> ConvertDataTableToLearnerClassDate(DataTable dt)
        {
            List<LearnerClassDateDto> re = new List<LearnerClassDateDto>();
            LearnerClassDateDto dto;
            int index = 1;

            foreach (DataRow row in dt.Rows)
            {
                dto = new LearnerClassDateDto();
                if (row.Table.Columns.Contains("LearnerClassDateId") 
                    && !String.IsNullOrWhiteSpace(row["LearnerClassDateId"].ToString()))
                {
                    dto.LearnerClassDateId = (Decimal)row["LearnerClassDateId"];
                }

                if (row.Table.Columns.Contains("LearnerName"))
                {
                    dto.LearnerName = row["LearnerName"].ToString();
                }

                dto.StaffId = row["StaffId"].ToString();
                dto.ClassId = row["ClassId"].ToString();
                if (row.Table.Columns.Contains("ClassName"))
                {
                    dto.ClassName = row["ClassName"].ToString();
                }

                if (row.Table.Columns.Contains("CourseId"))
                {
                    dto.CourseId = row["CourseId"].ToString();
                }
                dto.ClassDate = DateHelper.ConvertToDate(row["ClassDate"]);

                if (!String.IsNullOrWhiteSpace(row["CheckInTime"].ToString()))
                {
                    dto.CheckInTime = DateHelper.ConvertToDate(row["CheckInTime"]);
                }

                if (row.Table.Columns.Contains("Success"))
                {
                    dto.Success = row["Success"].ToString();
                }

                dto.Remark = row["Remark"].ToString();

                if (row.Table.Columns.Contains("UpdateBy"))
                {
                    dto.UpdateBy = row["UpdateBy"].ToString();
                }

                if (row.Table.Columns.Contains("RowNum") && !String.IsNullOrWhiteSpace(row["RowNum"].ToString()))
                {
                    dto.RowNum = int.Parse(row["RowNum"].ToString());
                }
                else
                {
                    dto.RowNum = index++;
                }

                re.Add(dto);
            }
            return re;
        }



        public List<ClassDateSummaryDto> ConvertTableToClassDateSummary(DataTable dt, string classId, int totalRegister)
        {
            List<ClassDateSummaryDto> re = new List<ClassDateSummaryDto>();
            ClassDateSummaryDto dto;

            foreach (DataRow row in dt.Rows)
            {
                dto = new ClassDateSummaryDto();
                dto.ClassDate = DateHelper.ConvertToDate(row["ClassDate"]);
                dto.Class.ClassID = classId;
                if (row.Table.Columns.Contains("Trainers"))
                {
                    dto.Class.Trainers = row["Trainers"].ToString();
                }

                dto.TotalRegister = totalRegister;
                dto.Present = int.Parse(row["Present"].ToString());
                dto.Absent = dto.TotalRegister - dto.Present;

                if (row.Table.Columns.Contains("RowNum"))
                {
                    if (!String.IsNullOrWhiteSpace(row["RowNum"].ToString()))
                    {
                        dto.RowNum = int.Parse(row["RowNum"].ToString());
                    }
                }
                re.Add(dto);
            }
            return re;
        }

    }
}
