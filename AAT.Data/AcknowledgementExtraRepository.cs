using System;
using System.Data;
using AAT_SIA.Model;
using System.Data.SqlClient;
using System.Collections.Generic;
using AAT.Data.Utils;
using AAT.Model.Utils;
using AAT.Model;

namespace AAT.Data
{
    public class AcknowledgementExtraRepository
    {
        public int CreateNewAcknowledgementExtra(AcknowledgementExtraDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@ClassId", SqlDbType = SqlDbType.Text, Value = dto.ClassId },
                    new SqlParameter() { ParameterName = "@TrainerName", SqlDbType = SqlDbType.Text, Value = dto.TrainerName },
                    new SqlParameter() { ParameterName = "@AcknowledgementDate", SqlDbType = SqlDbType.DateTime, Value = dto.AcknowledgementDate }
                };

                RepositoryHelper repository = new RepositoryHelper();

                var re = repository.ExecuteScalar("AcknowledgementExtra_Create", sp);
                return (re == null) ? -1 : int.Parse(re.ToString());
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateAcknowledgementExtra(int AcknowledgementExtraId, string TrainerName)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@AcknowledgementExtraId",  Value =AcknowledgementExtraId },
                    new SqlParameter() { ParameterName = "@TrainerName", Value = TrainerName }
                };

                RepositoryHelper repository = new RepositoryHelper();

                return repository.ExecuseNonQuery("AcknowledgementExtra_Update", sp);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }


        public List<AcknowledgementExtraDto> GetAcknowledgementExtraList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                List<AcknowledgementExtraDto> re = new List<AcknowledgementExtraDto>();
                List<SqlParameter> sp = new List<SqlParameter>();

                if (criterial.Page != null && criterial.PageSize != null)
                {
                    sp.Add(new SqlParameter() { ParameterName = "@recordPerPage", SqlDbType = SqlDbType.Int, Value = criterial.PageSize });
                    sp.Add(new SqlParameter() { ParameterName = "@page", SqlDbType = SqlDbType.Int, Value = criterial.Page });
                };

                foreach (KeyDto dto in criterial.SearchFieldList)
                {
                    sp.Add(new SqlParameter()
                    {
                        ParameterName = "@" + dto.Key.ToString(),
                        Value = dto.Value
                    });
                }

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("AcknowledgementExtra_GetList", sp);

                if (result != null && result.Tables[0] != null)
                {
                    totalRecord = (int)result.Tables[1].Rows[0][0];

                    re = ConvertDataTableToAcknowledgementExtra(result.Tables[0]);
                }
                else
                {
                    totalRecord = 0;
                }
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<AcknowledgementExtraDto> ConvertDataTableToAcknowledgementExtra(DataTable dt)
        {
            List<AcknowledgementExtraDto> re = new List<AcknowledgementExtraDto>();
            AcknowledgementExtraDto dto;

            foreach (DataRow row in dt.Rows)
            {
                dto = new AcknowledgementExtraDto();
                if (!String.IsNullOrWhiteSpace(row["AcknowledgementExtraId"].ToString()))
                {
                    dto.AcknowledgementExtraId = (int)row["AcknowledgementExtraId"];
                }
                dto.ClassId = row["ClassId"].ToString();
                dto.TrainerName = row["TrainerName"].ToString();
                dto.AcknowledgementDate = DateHelper.ConvertToDate(row["AcknowledgementDate"]);
                if (row.Table.Columns.Contains("RowNum"))
                {
                    if (!String.IsNullOrWhiteSpace(row["RowNum"].ToString()))
                    {
                        dto.RowNum = int.Parse(row["RowNum"].ToString());
                    }
                }
                re.Add(dto);
            }
            return re;
        }

    }
}
