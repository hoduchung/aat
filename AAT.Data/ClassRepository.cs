using System;
using System.Data;
using AAT.Model;
using System.Data.SqlClient;
using AAT.Data.Utils;
using AAT.Model.Utils;
using System.Collections.Generic;
using System.Linq;

namespace AAT.Data
{
    public class ClassRepository
    {
        public bool CreateNewClass(ClassDto dto)
        {
            try
            {
                
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@ClassID", SqlDbType = SqlDbType.Text, Value = dto.ClassID },
                    new SqlParameter() { ParameterName = "@ClassName", SqlDbType = SqlDbType.Text, Value = dto.ClassName },
                    new SqlParameter() { ParameterName = "@CourseId", SqlDbType = SqlDbType.Text, Value = dto.CourseId },
                    new SqlParameter() { ParameterName = "@Location", SqlDbType = SqlDbType.Text, Value = dto.Location },
                    new SqlParameter() { ParameterName = "@StartDate", SqlDbType = SqlDbType.DateTime, Value = dto.StartDate },
                    new SqlParameter() { ParameterName = "@EndDate", SqlDbType = SqlDbType.DateTime, Value = dto.EndDate },
                    new SqlParameter() { ParameterName = "@ClassStatusID", SqlDbType = SqlDbType.Text, Value = dto.ClassStatusID }
                };

                RepositoryHelper repository = new RepositoryHelper();

                repository.ExecuteScalar("Class_Create", sp);
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateClascsSkiesExported(string ClassIds)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@ClassIds", SqlDbType = SqlDbType.Text, Value = ClassIds }
                };

                RepositoryHelper repository = new RepositoryHelper();

                return repository.ExecuseNonQuery("Class_UpdateSkiesExported", sp);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateClass(ClassDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@ClassID", SqlDbType = SqlDbType.Text, Value = dto.ClassID },
                    new SqlParameter() { ParameterName = "@ClassName", SqlDbType = SqlDbType.Text, Value = dto.ClassName },
                    new SqlParameter() { ParameterName = "@CourseId", SqlDbType = SqlDbType.Text, Value = dto.CourseId },
                    new SqlParameter() { ParameterName = "@StartDate", SqlDbType = SqlDbType.DateTime, Value = dto.StartDate },
                    new SqlParameter() { ParameterName = "@EndDate", SqlDbType = SqlDbType.DateTime, Value = dto.EndDate },
                    new SqlParameter() { ParameterName = "@ClassStatusID", SqlDbType = SqlDbType.Text, Value = dto.ClassStatusID }
                };

                RepositoryHelper repository = new RepositoryHelper();

                return repository.ExecuseNonQuery("Class_Update", sp);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<ClassDateSummaryDto> GetClassDateSumList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                List<ClassDateSummaryDto> re = new List<ClassDateSummaryDto>();
                List<SqlParameter> sp = new List<SqlParameter>();

                if (criterial.Page != null && criterial.PageSize != null)
                {
                    sp.Add(new SqlParameter() { ParameterName = "@recordPerPage", SqlDbType = SqlDbType.Int, Value = criterial.PageSize });
                    sp.Add(new SqlParameter() { ParameterName = "@page", SqlDbType = SqlDbType.Int, Value = criterial.Page });
                };

                foreach (KeyDto dto in criterial.SearchFieldList)
                {
                    sp.Add(new SqlParameter()
                    {
                        ParameterName = "@" + dto.Key.ToString(),
                        Value = dto.Value
                    });
                }

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("Class_GetSummary", sp);

                if (result != null && result.Tables[0] != null)
                {
                    totalRecord = (int)result.Tables[1].Rows[0][0];

                    re = ConvertTableToClassDateSummary(result.Tables[0]);
                }
                else
                {
                    totalRecord = 0;
                }
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public ClassDto GetClassByClassId(string ClassID)
        {
            try
            {
                ClassDto re = new ClassDto();
                List<SqlParameter> sp = new List<SqlParameter>();

                sp.Add(new SqlParameter()
                {
                    ParameterName = "@ClassID" ,Value = ClassID
                });

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("Class_GetByClassId", sp);

                if (result != null && result.Tables[0] != null)
                {
                    re = ConvertDataTableToClass(result.Tables[0]).FirstOrDefault();
                }
               
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<ClassDto> GetClassList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                List<ClassDto> re = new List<ClassDto>();
                List<SqlParameter> sp = new List<SqlParameter>();

                if (criterial.Page != null && criterial.PageSize != null)
                {
                    sp.Add(new SqlParameter() { ParameterName = "@recordPerPage", SqlDbType = SqlDbType.Int, Value = criterial.PageSize });
                    sp.Add(new SqlParameter() { ParameterName = "@page", SqlDbType = SqlDbType.Int, Value = criterial.Page });
                };

                foreach (KeyDto dto in criterial.SearchFieldList)
                {
                    sp.Add(new SqlParameter()
                    {
                        ParameterName = "@" + dto.Key.ToString(),
                        Value = dto.Value
                    });
                }

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("Class_GetList", sp);

                if (result != null && result.Tables[0] != null)
                {
                    totalRecord = (int)result.Tables[1].Rows[0][0];

                    re = ConvertDataTableToClass(result.Tables[0]);
                }
                else
                {
                    totalRecord = 0;
                }
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public DataSet GetClassTimingDeail(string classId)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>();
                sp.Add(new SqlParameter() { ParameterName = "@ClassID", SqlDbType = SqlDbType.Text, Value = classId });

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("Class_GetTimingDetail", sp);
                return result;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public DataSet GetClassReport(string classId)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>();
                sp.Add(new SqlParameter() { ParameterName = "@ClassID", SqlDbType = SqlDbType.Text, Value = classId });

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("Class_GetReport", sp);
                return result;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public DataSet GetClassDetailByDate(string classId, DateTime classDate)
        {
            //ClassDto re = new ClassDto();
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>();
                sp.Add(new SqlParameter() { ParameterName = "@ClassID", SqlDbType = SqlDbType.Text, Value = classId });
                sp.Add(new SqlParameter() { ParameterName = "@ClassDate", SqlDbType = SqlDbType.DateTime, Value = classDate });

                List<KeyDto> roleFilters = FilterHelper.Instance.GetFilterAdtendancingPage();
                foreach(var filter in roleFilters)
                {
                    sp.Add(new SqlParameter() { ParameterName = "@" + filter.Key, Value = filter.Value });
                }

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("Class_GetDetailByDate", sp);
                return result;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public ClassDto GetClassDetail(string classId)
        {
            ClassDto re = new ClassDto();
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>();
                sp.Add(new SqlParameter() { ParameterName = "@ClassID", SqlDbType = SqlDbType.Text, Value = classId });

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("Class_GetDetail", sp);

                if (result != null && result.Tables[0] != null && result.Tables[0].Rows.Count > 0)
                {
                    re = ConvertDataTableToClass(result.Tables[0])[0];

                    //Trainer
                    if(result.Tables[1] != null && result.Tables[1].Rows.Count > 0)
                    {
                        TrainerRepository trainerData = new TrainerRepository();
                        re.TrainerList = trainerData.ConvertDataTableToTrainer(result.Tables[1]);
                    }

                    if (result.Tables[2] != null && result.Tables[2].Rows.Count > 0)
                    {
                        LearnerRepository learnerData = new LearnerRepository();
                        re.LearnerList = learnerData.ConvertDataTableToLearner(result.Tables[2]);
                    }
                }
                
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt">dt Class Summary</param>
        /// <param name="dtLCD">dt Learner Class Date</param>
        /// <returns></returns>
        public List<ClassDateSummaryDto> ConvertTableToClassDateSummary(DataTable dt)
        {
            List<ClassDateSummaryDto> re = new List<ClassDateSummaryDto>();
            ClassDateSummaryDto dto;

            int duration = 0;
            int totalSuccess = 0;
            int totalUncess = 0;

            foreach (DataRow row in dt.Rows)
            {
                dto = new ClassDateSummaryDto();
                dto.ClassDate = DateHelper.ConvertToDate(row["ClassDate"]);
                dto.Class.ClassID = row["ClassID"].ToString(); ;
                if (row.Table.Columns.Contains("Trainers"))
                {
                    dto.Class.Trainers = row["Trainers"].ToString();
                }

                if (row.Table.Columns.Contains("TrainerIds"))
                {
                    dto.Class.TrainerIDs = row["TrainerIds"].ToString();
                }

                if (row.Table.Columns.Contains("CourseName"))
                {
                    dto.Class.CourseName = row["CourseName"].ToString();

                }

                if (!String.IsNullOrWhiteSpace(row["ClassStatusID"].ToString()))
                {
                    dto.Class.ClassStatusID = (int)row["ClassStatusID"];
                    dto.TotalRegister = int.Parse(row["TotalRegister"].ToString());

                    if (dto.Class.ClassStatusID == ClassStatus.OnGoing.GetHashCode()
                        || dto.Class.ClassStatusID == ClassStatus.Completed.GetHashCode()
                        || dto.Class.ClassStatusID == ClassStatus.PendingAcknowledge.GetHashCode())
                    {
                        dto.Present = int.Parse(row["Present"].ToString());
                        dto.Absent = int.Parse(row["Absent"].ToString());
                    }
                    //else if (dtLCD != null && dtLCD.Rows.Count > 0 
                    //    && ( dto.Class.ClassStatusID == ClassStatus.Completed.GetHashCode() ||
                    //    dto.Class.ClassStatusID == ClassStatus.PendingAcknowledge.GetHashCode()))
                    //{
                    //    Total Success, Fail
                    //    duration = dtLCD.AsEnumerable().Where(x1 => x1.Field<string>("ClassID") == dto.Class.ClassID)
                    //        .Select(
                    //            x => new
                    //            {
                    //                attribute1_name = x.Field<string>("ClassDate")
                    //            }
                    //        ).Distinct().Count();

                    //}


                }

                if (row.Table.Columns.Contains("RowNum"))
                {
                    if (!String.IsNullOrWhiteSpace(row["RowNum"].ToString()))
                    {
                        dto.RowNum = int.Parse(row["RowNum"].ToString());
                    }
                }
                re.Add(dto);
            }
            return re;
        }

        public List<ClassDto> ConvertDataTableToClass(DataTable dt)
        {
            List<ClassDto> re = new List<ClassDto>();
            ClassDto dto;

            foreach (DataRow row in dt.Rows)
            {
                dto = new ClassDto();
                if (!String.IsNullOrWhiteSpace(row["ClassID"].ToString()))
                {
                    dto.ClassID = row["ClassID"].ToString();
                }
                dto.ClassName = row["ClassName"].ToString();
                if (!String.IsNullOrWhiteSpace(row["CourseId"].ToString()))
                {
                    dto.CourseId = row["CourseId"].ToString();
                }

                if(row.Table.Columns.Contains("CourseName"))
                {
                    dto.CourseName = row["CourseName"].ToString();
                }
                dto.StartDate = DateHelper.ConvertToDate(row["StartDate"]);
                dto.EndDate = DateHelper.ConvertToDate(row["EndDate"]);

                if (row.Table.Columns.Contains("ClassStatusID") && !String.IsNullOrWhiteSpace(row["ClassStatusID"].ToString()))
                {
                    dto.ClassStatusID = (int)row["ClassStatusID"];
                }

                if (row.Table.Columns.Contains("Trainers"))
                {
                    dto.Trainers = row["Trainers"].ToString();
                }

                if (row.Table.Columns.Contains("TrainerIds"))
                {
                    dto.TrainerIDs = row["TrainerIds"].ToString();
                }

                if (row.Table.Columns.Contains("TotalRegister"))
                {
                    dto.TotalRegister = (int)row["TotalRegister"];
                }

                
                re.Add(dto);
            }
            return re;
        }

    }
}
