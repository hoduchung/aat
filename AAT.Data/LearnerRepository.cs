using System;
using System.Data;
using AAT.Model;
using System.Data.SqlClient;
using AAT.Data.Utils;
using AAT.Model.Utils;
using System.Collections.Generic;
using System.Linq;

namespace AAT.Data
{
    public class LearnerRepository
    {
        public int CreateNewLearner(LearnerDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@LearnerName", SqlDbType = SqlDbType.Text, Value = dto.LearnerName },
                    new SqlParameter() { ParameterName = "@StaffID", SqlDbType = SqlDbType.Text, Value = dto.StaffID }
                };

                RepositoryHelper repository = new RepositoryHelper();

                var re = repository.ExecuteScalar("Learner_Create", sp);
                return (re == null) ? -1 : int.Parse(re.ToString());
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateLearner(LearnerDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@LearnerName", SqlDbType = SqlDbType.Text, Value = dto.LearnerName },
                    new SqlParameter() { ParameterName = "@StaffID", SqlDbType = SqlDbType.Text, Value = dto.StaffID }
                };

                RepositoryHelper repository = new RepositoryHelper();

                return repository.ExecuseNonQuery("Learner_Update", sp);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }


        public LearnerDto GetLearnerByStaffID(string staffID)
        {
            try
            {
                LearnerDto re = new LearnerDto();
                List<SqlParameter> sp = new List<SqlParameter>();

                sp.Add(new SqlParameter()
                {
                    ParameterName = "@StaffID",
                    Value = staffID
                });

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("Learner_GetByStaffId", sp);

                if (result != null && result.Tables[0] != null)
                {
                    re = ConvertDataTableToLearner(result.Tables[0]).FirstOrDefault();
                }
               
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<LearnerDto> GetLearnerHistory(SearchCriterialDto criterial)
        {
            try
            {
                List<LearnerDto> re = new List<LearnerDto>();
                List<SqlParameter> sp = new List<SqlParameter>();

                foreach (KeyDto dto in criterial.SearchFieldList)
                {
                    sp.Add(new SqlParameter()
                    {
                        ParameterName = "@" + dto.Key.ToString(),
                        Value = dto.Value
                    });
                }

                List<KeyDto> roleFilters = FilterHelper.Instance.GetFilterLearnerListPage();
                foreach (var filter in roleFilters)
                {
                    sp.Add(new SqlParameter() { ParameterName = "@" + filter.Key, Value = filter.Value });
                }

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("Learner_GetReport", sp);

                if (result != null && result.Tables[0] != null)
                {
                    re = ConvertDataTableToLearner(result.Tables[0]);
                }
               
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<LearnerDto> GetLearnerList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                List<LearnerDto> re = new List<LearnerDto>();
                List<SqlParameter> sp = new List<SqlParameter>();

                if (criterial.Page != null && criterial.PageSize != null)
                {
                    sp.Add(new SqlParameter() { ParameterName = "@recordPerPage", SqlDbType = SqlDbType.Int, Value = criterial.PageSize });
                    sp.Add(new SqlParameter() { ParameterName = "@page", SqlDbType = SqlDbType.Int, Value = criterial.Page });
                };

                foreach (KeyDto dto in criterial.SearchFieldList)
                {
                    sp.Add(new SqlParameter()
                    {
                        ParameterName = "@" + dto.Key.ToString(),
                        Value = dto.Value
                    });
                }

                List<KeyDto> roleFilters = FilterHelper.Instance.GetFilterLearnerListPage();
                foreach (var filter in roleFilters)
                {
                    sp.Add(new SqlParameter() { ParameterName = "@" + filter.Key, Value = filter.Value });
                }

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("Learner_GetList", sp);

                if (result != null && result.Tables[0] != null)
                {
                    totalRecord = (int)result.Tables[1].Rows[0][0];

                    re = ConvertDataTableToLearner(result.Tables[0]);
                }
                else
                {
                    totalRecord = 0;
                }
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<LearnerDto> LearnerGetLateReport(SearchCriterialDto criterial)
        {
            try
            {
                List<LearnerDto> re = new List<LearnerDto>();
                List<SqlParameter> sp = new List<SqlParameter>();

                foreach (KeyDto dto in criterial.SearchFieldList)
                {
                    sp.Add(new SqlParameter()
                    {
                        ParameterName = "@" + dto.Key.ToString(),
                        Value = dto.Value
                    });
                }

                List<KeyDto> roleFilters = FilterHelper.Instance.GetFilterLearnerListPage();
                foreach (var filter in roleFilters)
                {
                    sp.Add(new SqlParameter() { ParameterName = "@" + filter.Key, Value = filter.Value });
                }

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("Learner_GetLateReport", sp);

                if (result != null && result.Tables[0] != null)
                {
                    re = ConvertDataTableToLearner(result.Tables[0]);
                }
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<LearnerDto> ConvertDataTableToLearner(DataTable dt)
        {
            List<LearnerDto> re = new List<LearnerDto>();
            LearnerDto dto;
            int index = 1;
            foreach (DataRow row in dt.Rows)
            {
                dto = new LearnerDto();
                if (row.Table.Columns.Contains("LearnerId") && !String.IsNullOrWhiteSpace(row["LearnerId"].ToString()))
                {
                    dto.LearnerId = (int)row["LearnerId"];
                }
                dto.LearnerName = row["LearnerName"].ToString();
                dto.StaffID = row["StaffID"].ToString();

                if (row.Table.Columns.Contains("CheckInTime"))
                {
                    dto.CheckInTime = DateHelper.ConvertToDate(row["CheckInTime"]);
                }

                if (row.Table.Columns.Contains("ClassID"))
                {
                    dto.Class.ClassID = row["ClassID"].ToString();
                }

                if (row.Table.Columns.Contains("ClassName"))
                {
                    dto.Class.ClassName = row["ClassName"].ToString();
                }

                if (row.Table.Columns.Contains("ClassDate") && !String.IsNullOrWhiteSpace(row["ClassDate"].ToString()))
                {
                    dto.Class.ClassDate = DateHelper.ConvertToDate(row["ClassDate"]);
                }

                if (row.Table.Columns.Contains("StartDate") && !String.IsNullOrWhiteSpace(row["StartDate"].ToString()))
                {
                    dto.Class.StartDate = DateHelper.ConvertToDate(row["StartDate"]);
                }

                if (row.Table.Columns.Contains("EndDate") && !String.IsNullOrWhiteSpace(row["EndDate"].ToString()))
                {
                    dto.Class.EndDate = DateHelper.ConvertToDate(row["EndDate"]);
                }

                if (row.Table.Columns.Contains("CourseId"))
                {
                    dto.Class.CourseId = row["CourseId"].ToString();
                }

                if (row.Table.Columns.Contains("CourseName"))
                {
                    dto.Class.CourseName = row["CourseName"].ToString();
                }

                if (row.Table.Columns.Contains("Trainers"))
                {
                    dto.Class.Trainers = row["Trainers"].ToString();
                }

                if (row.Table.Columns.Contains("ClassStatusID"))
                {
                    dto.Class.ClassStatusID = int.Parse(row["ClassStatusID"].ToString());
                }
                
                if (row.Table.Columns.Contains("RowNum") && !String.IsNullOrWhiteSpace(row["RowNum"].ToString()))
                {
                    dto.RowNum = int.Parse(row["RowNum"].ToString());
                }
                else
                {
                    dto.RowNum = index++;
                }

                if (row.Table.Columns.Contains("Remark"))
                {
                    dto.Remark = row["Remark"].ToString();
                }

                if (row.Table.Columns.Contains("SuccessRemark"))
                {
                    dto.SuccessRemark = row["SuccessRemark"].ToString();
                }

                if (row.Table.Columns.Contains("Success"))
                {
                    dto.Success = row["Success"].ToString();
                }

                if (row.Table.Columns.Contains("UpdateBy"))
                {
                    dto.UpdateBy = row["UpdateBy"].ToString();
                }
                re.Add(dto);
            }
            return re;
        }

    }
}
