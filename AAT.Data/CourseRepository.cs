using System;
using System.Data;
using AAT.Model;
using System.Data.SqlClient;
using AAT.Data.Utils;
using AAT.Model.Utils;
using System.Collections.Generic;

namespace AAT.Data
{
    public class CourseRepository
    {
        public int CreateNewCourse(CourseDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@CourseId", SqlDbType = SqlDbType.Text, Value = dto.CourseId },
                    new SqlParameter() { ParameterName = "@CourseName", SqlDbType = SqlDbType.Text, Value = dto.CourseName },
                    new SqlParameter() { ParameterName = "@DepartmentID", SqlDbType = SqlDbType.Text, Value = dto.DepartmentID }
                };

                RepositoryHelper repository = new RepositoryHelper();

                var re = repository.ExecuteScalar("Course_Create", sp);
                return (re == null) ? -1 : int.Parse(re.ToString());
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateCourse(CourseDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@CourseId", SqlDbType = SqlDbType.Text, Value = dto.CourseId },
                    new SqlParameter() { ParameterName = "@CourseName", SqlDbType = SqlDbType.Text, Value = dto.CourseName },
                    new SqlParameter() { ParameterName = "@DepartmentID", SqlDbType = SqlDbType.Int, Value = dto.DepartmentID }
                };

                RepositoryHelper repository = new RepositoryHelper();

                return repository.ExecuseNonQuery("Course_Update", sp);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

    }
}
