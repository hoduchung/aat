using System;
using System.Data;
using AAT.Model;
using System.Data.SqlClient;
using AAT.Data.Utils;
using AAT.Model.Utils;
using System.Collections.Generic;

namespace AAT.Data
{
    public class LearnerClassRepository
    {
        public bool CreateNewLearnerClass(LearnerClassDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@StaffId", SqlDbType = SqlDbType.Text, Value = dto.StaffId },
                    new SqlParameter() { ParameterName = "@ClassId", SqlDbType = SqlDbType.Text, Value = dto.ClassId },
                    new SqlParameter() { ParameterName = "@IsRemoved", SqlDbType = SqlDbType.Bit, Value = dto.IsRemoved }
                };

                RepositoryHelper repository = new RepositoryHelper();

                var re = repository.ExecuteScalar("LearnerClass_Create", sp);
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateLearnerClass(LearnerClassDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@StaffId", SqlDbType = SqlDbType.Text, Value = dto.StaffId },
                    new SqlParameter() { ParameterName = "@ClassId", SqlDbType = SqlDbType.Text, Value = dto.ClassId },
                    new SqlParameter() { ParameterName = "@IsRemoved", SqlDbType = SqlDbType.Bit, Value = dto.IsRemoved }
                };

                RepositoryHelper repository = new RepositoryHelper();

                return repository.ExecuseNonQuery("LearnerClass_Update", sp);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateLearnerClassSuccessfulStatus(LearnerClassDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@StaffId", Value = dto.StaffId },
                    new SqlParameter() { ParameterName = "@ClassId", Value = dto.ClassId },
                    new SqlParameter() { ParameterName = "@Success", Value = dto.Success },
                    new SqlParameter() { ParameterName = "@Remark", Value = dto.Remark },
                    new SqlParameter() { ParameterName = "@UpdateBy", Value = dto.UpdateBy }
                };

                RepositoryHelper repository = new RepositoryHelper();

                return repository.ExecuseNonQuery("LearnerClass_UpdateSuccessfulStatus", sp);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool IsValidLearner(string classId, string staffId)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@ClassId ", Value = classId },
                    new SqlParameter() { ParameterName = "@StaffId ", Value = staffId }
                };

                RepositoryHelper repository = new RepositoryHelper();

                DataSet ds = repository.ExecuseDataSet("LearnerClass_IsValidLearner", sp);
                if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    string re = ds.Tables[0].Rows[0][0].ToString();

                    if(re == "1")
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }


        public bool DeleteLearnerClass(int learnerclassID)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                new SqlParameter() { ParameterName = "@LearnerClassId ", SqlDbType = SqlDbType.Int, Value = learnerclassID }
                };

                RepositoryHelper repository = new RepositoryHelper();

                var re = repository.ExecuseNonQuery("LearnerClass_Delete", sp);
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

    }
}
