using System;
using System.Data;
using AAT.Model;
using System.Data.SqlClient;
using AAT.Data.Utils;
using AAT.Model.Utils;
using System.Collections.Generic;

namespace AAT.Data
{
    public class DepartmentRepository
    {
        public bool CreateNewDepartment(DepartmentDto dto)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@DepartmentId", SqlDbType = SqlDbType.Text, Value = dto.DepartmentId },
                    new SqlParameter() { ParameterName = "@DepartmentName", SqlDbType = SqlDbType.Text, Value = dto.DepartmentName }
                };

                RepositoryHelper repository = new RepositoryHelper();

                repository.ExecuteScalar("Department_Create", sp);
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateDepartment(DepartmentDto dto, string oldDepartmentId)
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@OldDepartmentId", SqlDbType = SqlDbType.Text, Value = oldDepartmentId },
                    new SqlParameter() { ParameterName = "@NewDepartmentId", SqlDbType = SqlDbType.Text, Value = dto.DepartmentId },
                    new SqlParameter() { ParameterName = "@DepartmentName", SqlDbType = SqlDbType.Text, Value = dto.DepartmentName },
                };

                RepositoryHelper repository = new RepositoryHelper();

                return repository.ExecuseNonQuery("Department_Update", sp);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public int GetNumberDepartmentCourse(string departmentId)
        {
            try
            {
                int totalRecord;
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@DepartmentId", SqlDbType = SqlDbType.Text, Value = departmentId }
                };

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("Department_CountCourse", sp);

                if (result != null && result.Tables[0] != null)
                {
                    totalRecord = (int)result.Tables[0].Rows[0][0];
                }
                else
                {
                    totalRecord = 0;
                }
                return totalRecord;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<DepartmentDto> GetDepartmentList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                List<DepartmentDto> re = new List<DepartmentDto>();
                List<SqlParameter> sp = new List<SqlParameter>();

                if (criterial.Page != null && criterial.PageSize != null)
                {
                    sp.Add(new SqlParameter() { ParameterName = "@recordPerPage", SqlDbType = SqlDbType.Int, Value = criterial.PageSize });
                    sp.Add(new SqlParameter() { ParameterName = "@page", SqlDbType = SqlDbType.Int, Value = criterial.Page });
                };

                foreach (KeyDto dto in criterial.SearchFieldList)
                {
                    sp.Add(new SqlParameter()
                    {
                        ParameterName = "@" + dto.Key.ToString(),
                        Value = dto.Value
                    });
                }

                RepositoryHelper repository = new RepositoryHelper();
                var result = repository.ExecuseDataSet("Department_GetList", sp);

                if (result != null && result.Tables[0] != null)
                {
                    totalRecord = (int)result.Tables[1].Rows[0][0];

                    re = ConvertDataTableToDepartment(result.Tables[0]);
                }
                else
                {
                    totalRecord = 0;
                }
                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<DepartmentDto> ConvertDataTableToDepartment(DataTable dt)
        {
            List<DepartmentDto> re = new List<DepartmentDto>();
            DepartmentDto dto;

            foreach (DataRow row in dt.Rows)
            {
                dto = new DepartmentDto();
                dto.DepartmentId = row["DepartmentId"].ToString();
                dto.DepartmentName = row["DepartmentName"].ToString();
                dto.IsActive = (Boolean)row["IsActive"];
                if (row.Table.Columns.Contains("RowNum"))
                {
                    if (!String.IsNullOrWhiteSpace(row["RowNum"].ToString()))
                    {
                        dto.RowNum = int.Parse(row["RowNum"].ToString());
                    }
                }
                re.Add(dto);
            }
            return re;
        }

    }
}
