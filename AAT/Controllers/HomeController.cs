﻿using AAT.Business;
using AAT.Model;
using AAT.Model.Utils;
using AAT.Model.ViewModel;
using AAT.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAT.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            if (SessionHelper.Session.User.Role == UserRole.ExternalTrainer ||
                SessionHelper.Session.User.Role == UserRole.Staff)
            {
                return RedirectToAction("ClassList", "Class");
            }

            SearchCriterialDto criterial = new SearchCriterialDto
            {
                Page = 1,
                PageSize = ConfigHelper.PageSize
            };

            criterial.SearchFieldList.Add(new KeyDto { Key = "ClassDate", Value = DateTime.Now });
            criterial.SearchFieldList.Add(new KeyDto { Key = "SortColumn", Value = "ClassID" });
            criterial.SearchFieldList.Add(new KeyDto { Key = "SortOrder", Value = "ASC" });

            ClassDateSumListViewModel vm = GetClassDateSumListViewModel(criterial);

            vm.SortColumn = "ClassID";
            vm.SortOrder = "ASC";
            vm.SearchStatus = 2.ToString();
            vm.SearchDate = DateHelper.FormatDate(DateTime.Now);
            vm.ClassIds = string.Join(",", vm.ClassDateSumList.Select(x => x.Class.ClassID).ToList());
            vm.ActionId = PageAction.Pagging.GetHashCode().ToString();

            return View(vm);
        }

        //JsonResult
        public ActionResult BulkExport(int fileExtention, string classIds)
        {
            string re = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(classIds))
                {
                    return null;
                }

                string folderPath = System.AppDomain.CurrentDomain.BaseDirectory + "BulkExport\\";
                string filePath = folderPath + "BulkExport-" + DateTime.Now.ToString("yyyyMMdd hh-mm-ss");

                if (!System.IO.Directory.Exists(folderPath))
                    System.IO.Directory.CreateDirectory(folderPath);

                if (!System.IO.Directory.Exists(filePath))
                    System.IO.Directory.CreateDirectory(filePath);

                FileExtenstion fielType = (FileExtenstion)fileExtention;

                string[] classArr = classIds.Split(',');
                ClassReportDto classReport;
                Byte[] fileBytes = null;
                string wordHtml = string.Empty;

                foreach (string classID in classArr)
                {
                    //Write file to folder
                    string docName = filePath + "\\" + ExportHelper.GetClassExportFilename(classID, fielType);
                    if (System.IO.File.Exists(docName))
                    {
                        System.IO.File.Delete(docName);
                    }

                    //Step 1: GetData 
                    classReport = ClassService.Instance.GetClassReport(classID);

                    switch (fielType)
                    {
                        case FileExtenstion.PDF:
                            PdfExport pdfHelper = new PdfExport();
                            fileBytes = pdfHelper.ClassExportPdf(classReport);
                            break;
                        case FileExtenstion.WORD:
                            ExportHelper wordHelper = new ExportHelper();
                            wordHtml = wordHelper.GetClassHTMLContent(classReport, FileExtenstion.WORD);
                            break;
                        case FileExtenstion.EXCEL:
                            //S2: Export Excel
                            ExcelExport excelHelper = new ExcelExport();
                            fileBytes = excelHelper.ExportClassExcel(classReport);
                            break;
                    }

                    try
                    {
                        if (fielType == FileExtenstion.WORD)
                        {
                            System.IO.File.WriteAllText(docName, wordHtml);
                        }
                        else
                        {
                            using (var fs = new FileStream(docName, FileMode.Create, FileAccess.Write))
                            {
                                fs.Write(fileBytes, 0, fileBytes.Length);
                                fs.Close();
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        LogHelper.Info(DateTime.Now.ToString() + "Bulk Export Fail: " + docName + " Error: " + ex.Message);
                    }
                }


                // Return Export
                DirectoryInfo d = new DirectoryInfo(filePath); //filePath
                FileInfo[] Files = d.GetFiles(); //Getting Text files

                using (var memoryStream = new MemoryStream())
                {
                    using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                    {
                        foreach (FileInfo file in Files)
                        {
                            ziparchive.CreateEntryFromFile(filePath + "\\" + file.Name, file.Name);
                        }
                    }

                    try
                    {
                        //Detete tmp folder
                        Directory.Delete(filePath, true);
                    }
                    catch (Exception ex1)
                    {
                        LogHelper.Error("Unable delete bulk export Exception: " + ex1.Message, ex1);
                    }

                    return File(memoryStream.ToArray(), "application/zip", "AAT Class Bulk Export.zip");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(DateTime.Now.ToString() + "Method: BulkExport - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                if (ex.InnerException != null)
                {
                    LogHelper.Error("Exception: " + ex.InnerException.InnerException.StackTrace, ex);
                }
                //return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            return null;

        }


        public ActionResult ClassDateSumListAction(FormCollection collection)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(collection["ActionId"]))
                {
                    return null;
                }

                int actionId = int.Parse(collection["ActionId"]);
                string searchDate = string.Empty;
                string searchStartDate = string.Empty;
                string searchEndDate = string.Empty;
                string searchCourseId = string.Empty;
                string searchCourseName = string.Empty;
                string searchClassId = string.Empty;
                string searchClassName = string.Empty;
                string searchTrainerId = string.Empty;
                string searchTrainerName = string.Empty;
                string searchStatus = string.Empty;
                string sortColumn = string.Empty;
                string sortOrder = string.Empty;

                SearchCriterialDto criterial = new SearchCriterialDto();

                if (actionId == (int)PageAction.Pagging)
                {
                    criterial.Page = (!string.IsNullOrEmpty(collection["Page"])) ? Int16.Parse(collection["Page"]) : (short)1;
                    criterial.PageSize = ConfigHelper.PageSize;
                    searchDate = collection["hidDate"];
                    searchStartDate = collection["hidStartDate"];
                    searchEndDate = collection["hidEndDate"];
                    searchCourseId = collection["hidCourseId"];
                    searchCourseName = collection["hidCourseName"];
                    searchClassId = collection["hidClassId"];
                    searchClassName = collection["hidClassName"];
                    searchTrainerId = collection["hidTrainerId"];
                    searchTrainerName = collection["hidTrainerName"];
                    searchStatus = collection["hidSearchStatus"];
                    sortColumn = collection["hidSortColumn"];
                    sortOrder = collection["hidSortOrder"];

                }
                else if (actionId == (int)PageAction.Search)
                {
                    criterial.Page = 1;
                    criterial.PageSize = ConfigHelper.PageSize;
                    searchDate = string.Empty;
                    searchStartDate = collection["txtStartDate"];
                    searchEndDate = collection["txtEndDate"];
                    searchCourseId = collection["txtCourseId"];
                    searchCourseName = collection["txtCourseName"];
                    searchClassId = collection["txtClassId"];
                    searchClassName = collection["txtClassName"];
                    searchTrainerId = collection["txtTrainerId"];
                    searchTrainerName = collection["txtTrainerName"];
                    searchStatus = collection["ddlStatus"];

                }
                else if (actionId == (int)PageAction.Sorting)
                {
                    criterial.Page = 1;
                    criterial.PageSize = ConfigHelper.PageSize;
                    searchDate = collection["hidDate"];
                    searchStartDate = collection["hidStartDate"];
                    searchEndDate = collection["hidEndDate"];
                    searchCourseId = collection["hidCourseId"];
                    searchCourseName = collection["hidCourseName"];
                    searchClassId = collection["hidClassId"];
                    searchClassName = collection["hidClassName"];
                    searchTrainerId = collection["hidTrainerId"];
                    searchTrainerName = collection["hidTrainerName"];
                    searchStatus = collection["hidSearchStatus"];
                    sortColumn = collection["hidSortColumn"];
                    sortOrder = collection["hidSortOrder"];
                }
                else if (actionId == (int)PageAction.Export)
                {
                    criterial.Page = (!string.IsNullOrEmpty(collection["Page"])) ? Int16.Parse(collection["Page"]) : (short)1;
                    criterial.PageSize = ConfigHelper.PageSize;
                    searchDate = collection["hidDate"];
                    searchStartDate = collection["hidStartDate"];
                    searchEndDate = collection["hidEndDate"];
                    searchCourseId = collection["hidCourseId"];
                    searchCourseName = collection["hidCourseName"];
                    searchClassId = collection["hidClassId"];
                    searchClassName = collection["hidClassName"];
                    searchTrainerId = collection["hidTrainerId"];
                    searchTrainerName = collection["hidTrainerName"];
                    searchStatus = collection["hidSearchStatus"];
                    sortColumn = collection["hidSortColumn"];
                    sortOrder = collection["hidSortOrder"];

                }

                if (!String.IsNullOrWhiteSpace(searchCourseId))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "CourseId", Value = searchCourseId.Trim() });
                }

                if (!String.IsNullOrWhiteSpace(searchCourseName))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "CourseName", Value = searchCourseName.Trim() });
                }

                if (!String.IsNullOrWhiteSpace(searchClassId))
                {
                    searchClassId = searchClassId.Replace(" ", "");
                    if (searchClassId.IndexOf(',') > -1)
                    {
                        criterial.SearchFieldList.Add(new KeyDto { Key = "ClassIds", Value = searchClassId.Trim() });
                    }
                    else
                    {
                        criterial.SearchFieldList.Add(new KeyDto { Key = "ClassId", Value = searchClassId.Trim() });
                    }
                }

                if (!String.IsNullOrWhiteSpace(searchClassName))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "ClassName", Value = searchClassName.Trim() });
                }

                if (!String.IsNullOrWhiteSpace(searchTrainerId))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "TrainerId", Value = searchTrainerId.Trim() });
                }

                if (!String.IsNullOrWhiteSpace(searchTrainerName))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "TrainerName", Value = searchTrainerName.Trim() });
                }

                if (!String.IsNullOrWhiteSpace(searchStatus))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "ClassStatusID", Value = searchStatus });
                }

                if (actionId == (int)PageAction.Export)
                {
                    //string timeLate = string.Empty; //Late Report
                    //timeLate = collection["hidTimeLate"];

                    string hidTimeLate = collection["hidTimeLate"];
                    int hidTimeLateHH = int.Parse(collection["hidTimeLateHH"]);
                    int hidTimeLateMM = int.Parse(collection["hidTimeLateMM"]);

                    DateTime timeLate = DateHelper.ParseDate(hidTimeLate);
                    timeLate = timeLate.AddHours(hidTimeLateHH);
                    timeLate = timeLate.AddMinutes(hidTimeLateMM);

                    criterial.SearchFieldList.Add(new KeyDto { Key = "TimeLate", Value = timeLate });

                    List<LearnerDto> learnerExportList = null;
                    learnerExportList = LearnerService.Instance.LearnerGetLateReport(criterial);

                    if (learnerExportList.Any())
                    {
                        string exportType = string.Empty;
                        exportType = collection["hidExportType"];
                        switch (exportType)
                        {
                            case "PDF": return LearnerLate_ExportPDF(learnerExportList, criterial, timeLate);
                            case "WORD": return LearnerLate_ExportWord(learnerExportList, criterial, timeLate);
                            case "EXCEL": return LearnerLate_ExportExcel(learnerExportList, criterial, timeLate);
                        }
                    }

                }

                //Creterial Not for Export
                if (!String.IsNullOrWhiteSpace(searchDate))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "ClassDate", Value = DateHelper.ParseDate(searchDate) });
                }

                if (!String.IsNullOrWhiteSpace(searchStartDate))
                {
                    DateTime startDate = DateHelper.ParseDate(searchStartDate);
                    criterial.SearchFieldList.Add(new KeyDto { Key = "StartDate", Value = startDate });
                }

                if (!String.IsNullOrWhiteSpace(searchEndDate))
                {
                    DateTime endDate = DateHelper.ParseDate(searchEndDate);
                    criterial.SearchFieldList.Add(new KeyDto { Key = "EndDate", Value = endDate });
                }

                if (!String.IsNullOrWhiteSpace(sortColumn))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "SortColumn", Value = sortColumn });
                }

                if (!String.IsNullOrWhiteSpace(sortOrder))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "SortOrder", Value = sortOrder });
                }

                //Get Data
                KeyDto timeLateKey = criterial.SearchFieldList.FirstOrDefault(x => x.Key.ToString() == "TimeLate");
                if (timeLateKey != null)
                {
                    criterial.SearchFieldList.Remove(timeLateKey);
                }

                ClassDateSumListViewModel vm = GetClassDateSumListViewModel(criterial);
                vm.SearchDate = searchDate;
                vm.SearchStartDate = searchStartDate;
                vm.SearchEndtDate = searchEndDate;
                vm.SearchCourseId = searchCourseId;
                vm.SearchCourseName = searchCourseName;
                vm.SearchClassId = searchClassId;
                vm.SearchClassName = searchClassName;
                vm.SearchTrainerId = searchTrainerId;
                vm.SearchTrainerName = searchTrainerName;
                vm.SearchStatus = searchStatus;
                vm.PageSize = criterial.PageSize.Value;
                vm.SortColumn = sortColumn;
                vm.SortOrder = sortOrder;
                vm.ClassIds = string.Join(",", vm.ClassDateSumList.Select(x => x.Class.ClassID).ToList());
                
                if (actionId == (int)PageAction.Export)
                {
                    vm.ErrorMessage = "No data found!";
                    vm.ActionId = collection["OrgActionId"];
                }
                else
                {
                    vm.ActionId = actionId.ToString();
                }
                return View("Index", vm);
            }
            catch (Exception ex)
            {
                LogHelper.Error("ServiceCall - : " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        public ActionResult LearnerLateReport(string scandate, int scanHH, int scanMM, string exportType)
        {
            string msg = string.Empty;

            try
            {
                SearchCriterialDto criterial = new SearchCriterialDto();

                DateTime timeLate = DateHelper.ParseDate(scandate);
                timeLate.AddHours(scanHH);
                timeLate.AddMinutes(scanMM);

                criterial.SearchFieldList.Add(new KeyDto { Key = "TimeLate", Value = timeLate });

                List<LearnerDto> learnerExportList = null;
                learnerExportList = LearnerService.Instance.LearnerGetLateReport(criterial);

                if (learnerExportList.Any())
                {
                    switch (exportType)
                    {
                        case "PDF": return LearnerLate_ExportPDF(learnerExportList, criterial, timeLate);
                        case "WORD": return LearnerLate_ExportWord(learnerExportList, criterial, timeLate);
                        case "EXCEL": return LearnerLate_ExportExcel(learnerExportList, criterial, timeLate);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);

                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            msg = "No data found!";
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LearnerLate_ExportPDF(List<LearnerDto> learnerList, SearchCriterialDto criterial, DateTime classDate)
        {
            try
            {

                ExportHelper export = new ExportHelper();
                string html_string = export.GetLearnerLateHTML(learnerList, criterial, FileExtenstion.PDF, classDate);

                //S2: Export PDF
                PdfExport helper = new PdfExport();
                Byte[] fileBytes = helper.GetPDFBytes_Landscape(html_string);
                string docName = "Learners' Attendance Report " + classDate.ToString("yyyy MM dd") + ".pdf";
                string mimeType = MimeMapping.GetMimeMapping(docName);

                return File(fileBytes, mimeType, docName);

            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }

            return null;
        }

        public ActionResult LearnerLate_ExportExcel(List<LearnerDto> learnerList, SearchCriterialDto criterial, DateTime classDate)
        {
            try
            {
                //S2: Export Excel
                ExcelExport helper = new ExcelExport();
                Byte[] fileBytes = helper.ExportLearnerLate(learnerList, criterial, classDate);
                string docName = "Learners' Attendance Report " + classDate.ToString("yyyy MM dd") + ".xlsx";

                string mimeType = MimeMapping.GetMimeMapping(docName);
                return File(fileBytes, mimeType, docName);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }

            return null;
        }

        public ActionResult LearnerLate_ExportWord(List<LearnerDto> learnerList, SearchCriterialDto criterial, DateTime classDate)
        {
            try
            {

                ExportHelper export = new ExportHelper();
                string strBody = export.GetLearnerLateHTML(learnerList, criterial, FileExtenstion.WORD, classDate);

                // Force this content to be downloaded 
                // as a Word document with the name of your choice
                string docName = "Learners' Attendance Report " + classDate.ToString("yyyy MM dd") + ".doc";
                string mimeType = MimeMapping.GetMimeMapping(docName);
                Response.AppendHeader("Content-Type", "application/msword");

                Response.AppendHeader("Content-disposition", "attachment; filename=" + docName);
                Response.Write(strBody);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }

            return null;
        }

        private ClassDateSumListViewModel GetClassDateSumListViewModel(SearchCriterialDto criterial)
        {
            ClassDateSumListViewModel vm = new ClassDateSumListViewModel();

            ClassService srv = new ClassService();

            int totalRecord = 0;

            List<KeyDto> roleFilters = FilterHelper.Instance.GetFilterClassListPage(null);
            criterial.SearchFieldList.AddRange(roleFilters);

            vm.ClassDateSumList = srv.GetClassDateSumList(criterial, ref totalRecord);

            if (criterial.Page != null && criterial.PageSize != null)
            {
                PageViewModel paging = new PageViewModel(criterial.Page.Value,
                    criterial.PageSize.Value, totalRecord, "ClassDateSumPaging");
                vm.PagingModel = paging;
            }

            return vm;
        }

        public ActionResult Dashboard()
        {
            //return RedirectToAction("ClassList", "Class");
            return View();

        }


    }
}