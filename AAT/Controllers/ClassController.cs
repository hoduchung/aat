﻿using Microsoft.Office.Interop.Word;
using AAT.Business;
using AAT.Model;
using AAT.Model.Utils;
using AAT.Model.ViewModel;
using AAT.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using AAT_SIA.Business;
using AAT.UnisonService;
using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

namespace AAT.Controllers
{
    public class ClassController : BaseController
    {

        public JsonResult UpdateAcknowledgement(string classId, string trainers, string extratrainers)
        {

            if (classId == null || classId.Trim().Length == 0 || (trainers == null && extratrainers == null))
            {
                return Json("success", JsonRequestBehavior.AllowGet);
            }

            try
            {
                string[] trainerIdArr = trainers.Split(',');

                foreach (string trainerId in trainerIdArr)
                {
                    if (!string.IsNullOrEmpty(trainerId))
                    {
                        AcknowledgementService.Instance.CreateNewAcknowledgement(classId, trainerId);
                    }
                }

                if (!string.IsNullOrEmpty(extratrainers))
                {
                    string[] trainerExtraNameArr = extratrainers.Split(',');

                    foreach (string extraTrainerName in trainerExtraNameArr)
                    {
                        AcknowledgementExtraDto dto = new AcknowledgementExtraDto();
                        dto.ClassId = classId;
                        dto.TrainerName = extraTrainerName;
                        dto.AcknowledgementDate = DateTime.Now;
                        AcknowledgementExtraService.Instance.CreateNewAcknowledgementExtra(dto);
                    }
                }

                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportPDF(string classID)
        {
            try
            {
                //Step 1: GetData 
                ClassReportDto classReport = ClassService.Instance.GetClassReport(classID);

                //S2: Export Excel
                PdfExport helper = new PdfExport();
                Byte[] fileBytes = helper.ClassExportPdf(classReport);
                string docName = ExportHelper.GetClassExportFilename(classID, FileExtenstion.PDF);
                string mimeType = MimeMapping.GetMimeMapping(docName);

                return File(fileBytes, mimeType, docName);

            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }

            return null;
        }

        public ActionResult ExportExcel(string classID)
        {
            try
            {
                //Step 1: GetData 
                ClassReportDto classReport = ClassService.Instance.GetClassReport(classID);

                //S2: Export Excel
                ExcelExport helper = new ExcelExport();
                Byte[] fileBytes = helper.ExportClassExcel(classReport);

                string docName = ExportHelper.GetClassExportFilename(classID, FileExtenstion.EXCEL);
                string mimeType = MimeMapping.GetMimeMapping(docName);
                return File(fileBytes, mimeType, docName);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }
            return null;
        }

        public ActionResult ExportWord(string classID)
        {
            try
            {
                ExportHelper export = new ExportHelper();
                //Step 1: GetData 
                ClassReportDto classReport = ClassService.Instance.GetClassReport(classID);
                string strBody = export.GetClassHTMLContent(classReport, FileExtenstion.WORD);

                // Force this content to be downloaded 
                // as a Word document with the name of your choice
                string docName = ExportHelper.GetClassExportFilename(classID, FileExtenstion.WORD);
                string mimeType = MimeMapping.GetMimeMapping(docName);
                Response.AppendHeader("Content-Type", "application/msword");
                Response.AppendHeader("Content-disposition", "attachment; filename=" + docName);
                Response.Write(strBody);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }
            return null;
        }

        public ActionResult ClassList()
        {
            SearchCriterialDto criterial = new SearchCriterialDto
            {
                Page = 1,
                PageSize = ConfigHelper.PageSize
            };

            UserRole role = SessionHelper.Session.User.Role;
            UserType userType = SessionHelper.Session.User.UserType;

            if (role == UserRole.ExternalTrainer || role == UserRole.Staff)
            {
                string trainerInitFilter = "";
                if (userType == UserType.Window)
                {
                    if(!string.IsNullOrEmpty( SessionHelper.Session.User.PersonalNum))
                    {
                        trainerInitFilter = SessionHelper.Session.User.PersonalNum;
                    }
                    else
                    {
                        trainerInitFilter = SessionHelper.Session.User.LoginName;
                    }
                }
                else
                {
                    trainerInitFilter = SessionHelper.Session.User.LoginName;
                }

                if (!string.IsNullOrEmpty(trainerInitFilter))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "TrainerInitFilter", Value = trainerInitFilter });
                }
            }
            //else
            //{
            //    criterial.SearchFieldList.Add(new KeyDto { Key = "ClassDate", Value = DateTime.Now });
            //}

            criterial.SearchFieldList.Add(new KeyDto { Key = "ClassDate", Value = DateTime.Now });

            criterial.SearchFieldList.Add(new KeyDto { Key = "SortColumn", Value = "ClassID" });
            criterial.SearchFieldList.Add(new KeyDto { Key = "SortOrder", Value = "ASC" });
            ClassListViewModel vm = GetClassListViewModel(criterial);
            vm.SortColumn = "ClassID";
            vm.SortOrder = "ASC";
            vm.SearchStatus = 2.ToString();
            vm.SearchDate = DateHelper.FormatDate(DateTime.Now);
            return View(vm);
        }

        // GET: Class/Details/5
        public ActionResult Details(string id, DateTime? reqDate, string staffId = null)
        {
            //LearnerClassDateService srv = new LearnerClassDateService();
            DateTime daterequest = (reqDate != null) ? reqDate.Value : DateTime.Now;

            //ClassViewModel vm = srv.GetClassViewDetailByDate(id, daterequest);
            //vm.ClassDate = daterequest;

            ClassViewModel vm = GetClassViewModel(id, daterequest, "LearnerName", "ASC", "CheckInTime", "DESC");
            return View(vm);
        }

        private ClassListViewModel GetClassListViewModel(SearchCriterialDto criterial)
        {
            ClassListViewModel vm = new ClassListViewModel();
            ClassService srv = new ClassService();
            int totalRecord = 0;

            List<KeyDto> roleFilters = FilterHelper.Instance.GetFilterClassListPage(criterial);
            criterial.SearchFieldList.AddRange(roleFilters);
            vm.ClassList = srv.GetClassList(criterial, ref totalRecord);

            if (criterial.Page != null && criterial.PageSize != null)
            {
                PageViewModel paging = new PageViewModel(criterial.Page.Value,
                    criterial.PageSize.Value, totalRecord, "CardClassPaging");
                vm.PagingModel = paging;
            }

            return vm;
        }

        public JsonResult RFIDAttendingUpdate(string classId, DateTime classDate, string cardId)
        {
            //if (classId == null || classId.Trim().Length == 0 || trainers == null || trainers.Trim().Length == 0)
            //{
            //    return Json("success", JsonRequestBehavior.AllowGet);
            //}

            try
            {
                CardDto re = new CardDto();
                re.Status = "Learner Invalid!";
                byte[] photo;
                //TODO TESTING
                //photo = ImageHelper.GetDemoPhotoByteArray();
                //re.Base64Image = "data:image/gif;base64," + ImageHelper.ConvertToBase64FromByteArr(photo);

                //if (!string.IsNullOrEmpty(cardId))
                //{
                //    //Card Image 
                //    cardId = cardId.Trim();
                //    re.StaffId = cardId;

                //    LearnerDto learner = LearnerService.Instance.GetLearnerById(re.StaffId);
                //    if (learner != null)
                //    {
                //        re.Name = learner.LearnerName;

                //        SearchCriterialDto criterial = new SearchCriterialDto();
                //        criterial.SearchFieldList.Add(new KeyDto { Key = "ClassId", Value = classId });
                //        criterial.SearchFieldList.Add(new KeyDto { Key = "ClassDate", Value = DateTime.Now });
                //        int totalRecord = 0;
                //        List<LearnerClassDateDto> existedLearnerList =
                //       LearnerClassDateService.Instance.GetLearnerClassDateList(criterial, ref totalRecord);

                //        bool isValidLearner = LearnerClassService.Instance.IsValidLearner(classId, re.StaffId);

                //        //var existedLearner = existedLearnerList.FirstOrDefault(x=>x.)
                //        if (isValidLearner)
                //        {
                //            //Add Learner to Class
                //            LearnerClassDateDto dto = new LearnerClassDateDto();
                //            dto.ClassId = classId;
                //            dto.ClassDate = classDate;
                //            dto.StaffId = re.StaffId;
                //            dto.CheckInTime = DateTime.Now;
                //            dto.Remark = string.Empty;
                //            decimal learnerClassDateId = LearnerClassDateService.Instance.CreateNewLearnerClassDate(dto);
                //            re.LearnerClassDateId = learnerClassDateId;
                //            re.TimeIn = DateHelper.FormatTime(DateTime.Now);
                //            if (learnerClassDateId == -999)
                //            {
                //                re.Status = "Learner Attended!";
                //            }
                //            else
                //            {
                //                re.Status = "Valid";
                //            }
                //        }
                //    }

                //    return Json(re, JsonRequestBehavior.AllowGet);
                //}

                //END TESTING
                //RFID Checking

                if (!string.IsNullOrEmpty(cardId)) // && cardId.Length == 11
                {
                    cardId = cardId.Trim();

                    if (cardId.Length < 10)
                    {
                        int addinLength = 10 - cardId.Length;
                        for (int i = 0; i < addinLength; i++)
                        {
                            cardId = "0" + cardId.ToString();
                        }
                    }
                    //Card Image 

                    LearnerDto learner = null;
                    CardInfo cardInfo = GlobalData.UnisonService.GetCardByNumber(cardId, "", "", "");

                    if (cardInfo != null)
                    {
                        UserInfo user = GlobalData.UnisonService.GetUserByKey(cardInfo.UserKey);
                        if (user != null)
                        {
                            re.Name = user.FirstName + " " + user.LastName;
                            
                            UserImagesInfo image = GlobalData.UnisonService.GetUserImagesByUserKey(user.Key);
                            if (image != null)
                            {
                                string base64Str = ImageHelper.ConvertToBase64FromByteArr(image.Photo);
                                if (!string.IsNullOrEmpty(base64Str))
                                {
                                    re.Base64Image = "data:image/gif;base64," + base64Str;
                                }
                            }
                            re.StaffId = user.UserID;
                            learner = LearnerService.Instance.GetLearnerByStaffID(re.StaffId);

                        }
                    }
                    else
                    {
                        //Log file
                        LogHelper.Info(DateTime.Now.ToString() + "Card Id Not found: " + cardId);
                    }

                    if (learner != null)
                    {
                        //Use learner name form AAT DB
                        re.Name = learner.LearnerName;
                        SearchCriterialDto criterial = new SearchCriterialDto();
                        criterial.SearchFieldList.Add(new KeyDto { Key = "ClassId", Value = classId });
                        criterial.SearchFieldList.Add(new KeyDto { Key = "ClassDate", Value = DateTime.Now });
                        int totalRecord = 0;
                        List<LearnerClassDateDto> existedLearnerList =
                       LearnerClassDateService.Instance.GetLearnerClassDateList(criterial, ref totalRecord);

                        bool isValidLearner = LearnerClassService.Instance.IsValidLearner(classId, re.StaffId);

                        if (isValidLearner)
                        {
                            //Add Learner to Class
                            LearnerClassDateDto dto = new LearnerClassDateDto();
                            dto.ClassId = classId;
                            dto.ClassDate = classDate;
                            dto.StaffId = re.StaffId;
                            dto.CheckInTime = DateTime.Now;
                            dto.Remark = string.Empty;
                            decimal learnerClassDateId = LearnerClassDateService.Instance.CreateNewLearnerClassDate(dto);
                            re.LearnerClassDateId = learnerClassDateId;
                            if (learnerClassDateId == -999)
                            {
                                re.Status = "Learner Attended!";
                            }
                            else
                            {
                                re.Status = "Valid";
                            }
                        }


                    }
                }

                return Json(re, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogHelper.Error(DateTime.Now.ToString() + "Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);

                if(ex.InnerException != null)
                {
                    LogHelper.Error("Exception: " + ex.InnerException.InnerException.StackTrace, ex);

                }
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

        }

        private void AddLearnerAttending(FormCollection collection)
        {
            string classId = collection["ClassId"].ToString();
            DateTime classDate = DateHelper.ParseDate(collection["ClassDate"]);

            string[] staffIds = collection["chkAttendAvailable"].Split(',');
            DateTime checkinTime = DateTime.Now;

            string remark = collection["hidAttendingRemark"].ToString();
            foreach (string staffId in staffIds)
            {
                LearnerClassDateDto dto = new LearnerClassDateDto();
                dto.ClassId = classId;
                dto.ClassDate = classDate;
                dto.StaffId = staffId;
                dto.CheckInTime = checkinTime;
                dto.Remark = remark;
                dto.UpdateBy = SessionHelper.Session.User.LoginName;
                LearnerClassDateService.Instance.CreateNewLearnerClassDate(dto);
            }
        }

        private void UpdateLearnerClassDateRemark(int learnerClassDateId, string remark)
        {
            try
            {
                SearchCriterialDto criterial = new SearchCriterialDto();
                criterial.SearchFieldList.Add(new KeyDto { Key = "LearnerClassDateId", Value = learnerClassDateId });

                int totalRecord = 0;
                LearnerClassDateDto dto = LearnerClassDateService.Instance.GetLearnerClassDateList(criterial, ref totalRecord).FirstOrDefault();

                dto.Remark = remark;
                dto.UpdateBy = SessionHelper.Session.User.LoginName;
                LearnerClassDateService.Instance.UpdateLearnerClassDate(dto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

      


        public ActionResult ClassAttendingAction(FormCollection collection)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(collection["ActionId"]))
                {
                    return null;
                }

                int actionId = int.Parse(collection["ActionId"]);
                string classId = collection["ClassId"].ToString();
                DateTime classDate = DateHelper.ParseDate(collection["ClassDate"]);

                string hidLearnerNotPresentSort = collection["hidLearnerNotPresentSort"].ToString();
                string hidLearnerNotPresentSortDir = collection["hidLearnerNotPresentSortDir"].ToString();
                string hidLearnerPresentSort = collection["hidLearnerPresentSort"].ToString();
                string hidLearnerPresentSortDir = collection["hidLearnerPresentSortDir"].ToString();


                if (actionId == PageAction.Attending.GetHashCode())
                {
                    if (String.IsNullOrWhiteSpace(collection["hidRemarkAction"]))
                    {
                        return null;
                    }

                    string remarkAction = collection["hidRemarkAction"];
                    if (remarkAction == "ADD")
                    {
                        AddLearnerAttending(collection);
                    }
                    else
                    {
                        int learnerClassDateId = int.Parse(collection["hidLearnerClassDateId"]);
                        string remark = collection["hidAttendingRemark"].ToString();
                        UpdateLearnerClassDateRemark(learnerClassDateId, remark);
                    }

                    //return RedirectToAction("Details", new { id = classId, reqDate = classDate });
                }
                else if (actionId == PageAction.Sorting.GetHashCode())
                {
                }

                ClassViewModel vm = GetClassViewModel(classId, classDate,
                    hidLearnerNotPresentSort, hidLearnerNotPresentSortDir, hidLearnerPresentSort, hidLearnerPresentSortDir);
                return View("Details", vm);
                
            }
            catch (Exception ex)
            {
                LogHelper.Error("ServiceCall - : " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        public ActionResult EditAcknowledgementExtraAction(FormCollection collection)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(collection["ActionId"]))
                {
                    return null;
                }

                int actionId = int.Parse(collection["ActionId"]);
                decimal hidAcknowledgementExtraId = decimal.Parse(collection["hidAcknowledgementExtraId"].ToString());
                string hidAcknowledgementExtraName = collection["hidAcknowledgementExtraName"].ToString();
                string learnerSoft = collection["hidLearnerSort"].ToString();
                string learnerSoftDir = collection["hidLearnerSortDir"].ToString();
              

                string classId = collection["ClassId"].ToString();

                if (actionId == PageAction.Update.GetHashCode())
                {
                    if (String.IsNullOrWhiteSpace(collection["hidAcknowledgementExtraAction"]))
                    {
                        return null;
                    }

                    string remarkAction = collection["hidAcknowledgementExtraAction"];
                    if (remarkAction == "EDIT")
                    {
                        int AcknowledgementExtraId = int.Parse(collection["hidAcknowledgementExtraId"]);
                        string TrainerName = collection["hidAcknowledgementExtraName"].ToString();

                        AcknowledgementExtraService.Instance.UpdateAcknowledgementExtra(AcknowledgementExtraId, TrainerName);
                    }

                    //return RedirectToAction("Details", new { id = classId, reqDate = classDate });
                }


                //ClassTimingViewModel vm = GetClassTimingViewModel(classId, learnerSoft, learnerSoftDir);
                ClassTimingViewModel vm = GetClassTimingViewModel(classId, learnerSoft, learnerSoftDir);
                return View("Timing" ,vm);

            }
            catch (Exception ex)
            {
                LogHelper.Error("ServiceCall - : " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        public ClassViewModel GetClassViewModel(string classId, DateTime daterequest, string LearnerNotPresentSort,
            string LearnerNotPresentSortDir, string LearnerPresentSort, string LearnerPresentSortDir)
        {
            LearnerClassDateService srv = new LearnerClassDateService();
            ClassViewModel vm = srv.GetClassViewDetailByDate(classId, daterequest);

            if(LearnerNotPresentSortDir.Trim().ToUpper() == "ASC")
            {
                vm.LearnerNotPresent = vm.LearnerNotPresent.OrderBy(x => 
                (LearnerNotPresentSort == "LearnerName") ? x.LearnerName : x.StaffID).ToList();
            }
            else
            {
                vm.LearnerNotPresent = vm.LearnerNotPresent.OrderByDescending(x =>
               (LearnerNotPresentSort == "LearnerName") ? x.LearnerName : x.StaffID).ToList();
            }

            if (LearnerPresentSort == "CheckInTime")
            {
                if (LearnerPresentSortDir.Trim().ToUpper() == "ASC")
                {
                    vm.LearnerPresent = vm.LearnerPresent.OrderBy(x => x.CheckInTime).ToList();
                }
                else
                {
                    vm.LearnerPresent = vm.LearnerPresent.OrderByDescending(x => x.CheckInTime).ToList();
                }
            }
            else
            {
                if (LearnerPresentSortDir.Trim().ToUpper() == "ASC")
                {
                    vm.LearnerPresent = vm.LearnerPresent.OrderBy(x =>
                    (LearnerPresentSort == "LearnerName") ? x.LearnerName : x.StaffId).ToList();
                }
                else
                {
                    vm.LearnerPresent = vm.LearnerPresent.OrderByDescending(x =>
                   (LearnerPresentSort == "LearnerName") ? x.LearnerName : x.StaffId).ToList();
                }
            }

            vm.LearnerNotPresentSort = LearnerNotPresentSort;
            vm.LearnerNotPresentSortDir = LearnerNotPresentSortDir;
            vm.LearnerPresentSort = LearnerPresentSort;
            vm.LearnerPresentSortDir = LearnerPresentSortDir;
            vm.ClassDate = daterequest;
            return vm;
        }

        public ActionResult ClassListAction(FormCollection collection)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(collection["ActionId"]))
                {
                    return null;
                }

                int actionId = int.Parse(collection["ActionId"]);
                string searchDate = string.Empty;
                string searchStartDate = string.Empty;
                string searchEndDate = string.Empty;
                string searchCourseId = string.Empty;
                string searchCourseName = string.Empty;
                string searchClassId = string.Empty;
                string searchClassName = string.Empty;
                string searchTrainerId = string.Empty;
                string searchTrainerName = string.Empty;
                string searchStatus = string.Empty;
                string sortColumn = string.Empty;
                string sortOrder = string.Empty;

                SearchCriterialDto criterial = new SearchCriterialDto();

                if (actionId == (int)PageAction.Pagging)
                {
                    if (String.IsNullOrWhiteSpace(collection["Page"]))
                    {
                        return null;
                    }

                    criterial.Page = Int16.Parse(collection["Page"]);
                    criterial.PageSize = ConfigHelper.PageSize;
                    searchDate = collection["hidDate"];
                    searchStartDate = collection["hidStartDate"];
                    searchEndDate = collection["hidEndDate"];
                    searchCourseId = collection["hidCourseId"];
                    searchCourseName = collection["hidCourseName"];
                    searchClassId = collection["hidClassId"];
                    searchClassName = collection["hidClassName"];
                    searchTrainerId = collection["hidTrainerId"];
                    searchTrainerName = collection["hidTrainerName"];
                    searchStatus = collection["hidSearchStatus"];
                    sortColumn = collection["hidSortColumn"];
                    sortOrder = collection["hidSortOrder"];
                }
                else if (actionId == (int)PageAction.Search)
                {
                    criterial.Page = 1;
                    criterial.PageSize = ConfigHelper.PageSize;
                    searchDate = string.Empty;
                    searchStartDate = collection["txtStartDate"];
                    searchEndDate = collection["txtEndDate"];
                    searchCourseId = collection["txtCourseId"];
                    searchCourseName = collection["txtCourseName"];
                    searchClassId = collection["txtClassId"];
                    searchClassName = collection["txtClassName"];
                    searchTrainerId = collection["txtTrainerId"];
                    searchTrainerName = collection["txtTrainerName"];
                    searchStatus = collection["ddlStatus"];

                }
                else if (actionId == (int)PageAction.Sorting)
                {
                    criterial.Page = 1;
                    criterial.PageSize = ConfigHelper.PageSize;
                    searchDate = collection["hidDate"];
                    searchStartDate = collection["hidStartDate"];
                    searchEndDate = collection["hidEndDate"];
                    searchCourseId = collection["hidCourseId"];
                    searchCourseName = collection["hidCourseName"];
                    searchClassId = collection["hidClassId"];
                    searchClassName = collection["hidClassName"];
                    searchTrainerId = collection["hidTrainerId"];
                    searchTrainerName = collection["hidTrainerName"];
                    searchStatus = collection["hidSearchStatus"];
                    sortColumn = collection["hidSortColumn"];
                    sortOrder = collection["hidSortOrder"];
                }

                if (!String.IsNullOrWhiteSpace(searchDate))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "ClassDate", Value = DateHelper.ParseDate(searchDate) });
                }

                if (!String.IsNullOrWhiteSpace(searchStartDate))
                {
                    DateTime startDate = DateHelper.ParseDate(searchStartDate);
                    criterial.SearchFieldList.Add(new KeyDto { Key = "StartDate", Value = startDate });
                }

                if (!String.IsNullOrWhiteSpace(searchEndDate))
                {
                    DateTime endDate = DateHelper.ParseDate(searchEndDate);
                    criterial.SearchFieldList.Add(new KeyDto { Key = "EndDate", Value = endDate });
                }

                //if (String.IsNullOrWhiteSpace(searchStartDate) && String.IsNullOrWhiteSpace(searchEndDate))
                //{
                //    criterial.SearchFieldList.Add(new KeyDto { Key = "ClassDate", Value = DateTime.Now });
                //}

                if (!String.IsNullOrWhiteSpace(searchCourseId))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "CourseId", Value = searchCourseId.Trim() });
                }

                if (!String.IsNullOrWhiteSpace(searchCourseName))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "CourseName", Value = searchCourseName.Trim() });
                }

                if (!String.IsNullOrWhiteSpace(searchClassId))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "ClassId", Value = searchClassId.Trim() });
                }

                if (!String.IsNullOrWhiteSpace(searchClassName))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "ClassName", Value = searchClassName.Trim() });
                }

                if (!String.IsNullOrWhiteSpace(searchTrainerId))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "TrainerId", Value = searchTrainerId.Trim() });
                }

                if (!String.IsNullOrWhiteSpace(searchTrainerName))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "TrainerName", Value = searchTrainerName.Trim() });
                }

                if (!String.IsNullOrWhiteSpace(searchStatus))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "ClassStatusID", Value = searchStatus });
                }

               

                UserRole role = SessionHelper.Session.User.Role;
                UserType userType = SessionHelper.Session.User.UserType;
                if (role == UserRole.ExternalTrainer && criterial.SearchFieldList.Count == 0)
                {
                    string trainerInitFilter = "";
                    if (userType == UserType.Window)
                    {
                        if (!string.IsNullOrEmpty(SessionHelper.Session.User.PersonalNum))
                        {
                            trainerInitFilter = SessionHelper.Session.User.PersonalNum;
                        }
                        else
                        {
                            trainerInitFilter = SessionHelper.Session.User.LoginName;
                        }
                    }
                    else
                    {
                        trainerInitFilter = SessionHelper.Session.User.LoginName;
                    }

                    if (!string.IsNullOrEmpty(trainerInitFilter))
                    {
                        criterial.SearchFieldList.Add(new KeyDto { Key = "TrainerInitFilter", Value = trainerInitFilter });
                    }
                }


                if (!String.IsNullOrWhiteSpace(sortColumn))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "SortColumn", Value = sortColumn });
                }

                if (!String.IsNullOrWhiteSpace(sortOrder))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "SortOrder", Value = sortOrder });
                }
                //Get Data
                ClassListViewModel vm = GetClassListViewModel(criterial);
                vm.SearchDate = searchDate;
                vm.SearchStartDate = searchStartDate;
                vm.SearchEndtDate = searchEndDate;
                vm.SearchCourseId = searchCourseId;
                vm.SearchCourseName = searchCourseName;
                vm.SearchClassId = searchClassId;
                vm.SearchClassName = searchClassName;
                vm.SearchTrainerId = searchTrainerId;
                vm.SearchTrainerName = searchTrainerName;
                vm.SearchStatus = searchStatus;
                vm.PageSize = criterial.PageSize.Value;
                vm.SortColumn = sortColumn;
                vm.SortOrder = sortOrder;

                return View("ClassList", vm);
            }
            catch (Exception ex)
            {
                LogHelper.Error("ServiceCall - : " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        // GET: Class/Time/5
        public ActionResult Timing(string id, string lnsort = "")
        {
            string learnerSoft = "LearnerName";
            string learnerSoftDir = "ASC";

            if(!string.IsNullOrEmpty(lnsort))
            {
                string[] lnSortValues = lnsort.Split(',');
                if(lnSortValues.Length == 2)
                {
                    string str1 = lnSortValues[0];
                    string str2 = lnSortValues[1];

                    if(str1 == "LearnerName" || str1 == "StaffID")
                    {
                        learnerSoft = str1;
                        if(str2.ToUpper() == "DESC")
                        {
                            learnerSoftDir = "DESC";
                        }
                    }
                }
            }
            ClassTimingViewModel vm = GetClassTimingViewModel(id, learnerSoft, learnerSoftDir);
            return View(vm);
        }


        public ClassTimingViewModel GetClassTimingViewModel(string classId, string learnerSoft, string learnerSoftDir)
        {
            ClassTimingViewModel vm = ClassService.Instance.GetClassTimingViewModel(classId);

            if (learnerSoftDir.Trim().ToUpper() == "ASC")
            {
                vm.Class.LearnerList = vm.Class.LearnerList.OrderBy(x =>
                (learnerSoft == "LearnerName") ? x.LearnerName : x.StaffID).ToList();
            }
            else
            {
                vm.Class.LearnerList = vm.Class.LearnerList.OrderByDescending(x =>
                 (learnerSoft == "LearnerName") ? x.LearnerName : x.StaffID).ToList();
            }

            vm.LearnerSort = learnerSoft;
            vm.LearnerSoftDir = learnerSoftDir;

            return vm;
        }
    }
}
