﻿using AAT.Business;
using AAT.Model;
using AAT.Model.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Principal;
using AAT.Utils;
using System.Net;
using System.IO;
using System.Text;
using System.Web.Security;

namespace AAT.Controllers
{
    public class AccountController : Controller
    {
        [AllowAnonymous]
        public ActionResult Login()
        {
            try
            {
                string queryString = Newtonsoft.Json.JsonConvert.SerializeObject(Request.QueryString);
                string requestHeader = Newtonsoft.Json.JsonConvert.SerializeObject(Request.Headers);
                LogHelper.Info(DateTime.Now.ToString() + "queryString:" + queryString);
                LogHelper.Info(DateTime.Now.ToString() + "requestHeader" + requestHeader);

                //W2K Testing
                //if (Request.QueryString["SMUNIVERSALID"] != null)
                //{
                //    string username = Request.QueryString["SMUNIVERSALID"];
                //    string personalnum = Request.QueryString["personalnum"];

                //    LogHelper.Info(DateTime.Now.ToString() + "Win2K login: " + username);
                //    LogHelper.Info(DateTime.Now.ToString() + "personalnum: " + personalnum);

                //    UserDto dto = UserService.Instance.GetUserLogin(username);
                //    if (dto != null && dto.Status == StatusType.Active)
                //    {
                //        SessionHelper.Session.User = dto;
                //        SessionHelper.Session.User.PersonalNum = personalnum;
                //        return RedirectToAction("ClassList", "Class");
                //    }
                //}

                //Single sign login
                if (Request.Headers["SMUNIVERSALID"] != null)
                {
                    string username = Request.Headers["SMUNIVERSALID"];
                    string personalnum = Request.Headers["personalnum"];

                    LogHelper.Info(DateTime.Now.ToString() + "Win2K login: " + username);
                    LogHelper.Info(DateTime.Now.ToString() + "personalnum: " + personalnum);

                    UserDto dto = UserService.Instance.GetUserLogin(username);
                    if (dto != null && dto.Status != StatusType.Locked)
                    {
                        SessionHelper.Session.User = dto;
                        SessionHelper.Session.User.PersonalNum = personalnum;
                        return RedirectToAction("ClassList", "Class");
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
           
            return View();
        }

        public ActionResult Logout()
        {
            try
            {
                SessionHelper.ClearSession();
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }
            return View("BasicLogin");
        }

        public ActionResult BasicLogin()
        {
            return View();
        }

        public JsonResult CheckLogin(string username, string password)
        {
            string msg = string.Empty;

            try
            {
                UserDto dto = UserService.Instance.GetUserLogin(username);
                if (dto == null )
                {
                    msg = "Invalid Username and Password!";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }

                if (dto.UserType == UserType.Window)
                {
                    msg = "Unable to login by W2K ID!";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }

                string sha1Pass = SHA1Helper.GetPasswordHash(password);

                if (dto.Password == sha1Pass)
                {
                    SessionHelper.Session.User = dto;
                    dto.PassIncorrectCount = 0;
                    UserService.Instance.UpdateUser(dto);

                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    dto.PassIncorrectCount += 1;
                    if (dto.PassIncorrectCount == 3)
                    {
                        dto.Status = StatusType.Locked;
                    }

                    UserService.Instance.UpdateUser(dto);
                }

            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }

            msg = "Invalid Username and Password!";
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        // GET: Account/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

       
    }
}
