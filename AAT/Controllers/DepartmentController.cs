﻿using AAT.Business;
using AAT.Model;
using AAT.Model.Utils;
using AAT.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAT.Controllers
{
    public class DepartmentController : BaseController
    {
        // GET: Department
        public ActionResult DepartmentList()
        {
            if (SessionHelper.Session.User.Role == UserRole.ExternalTrainer ||
                SessionHelper.Session.User.Role == UserRole.Staff)
            {
                return RedirectToAction("ClassList", "Class");
            }

            SearchCriterialDto criterial = new SearchCriterialDto
            {
                Page = 1,
                PageSize = 999
            };
            DepartmentListViewModel vm = GetDepartmentListViewModel(criterial);
            return View(vm);
        }

        public ActionResult DepartmentListAction(FormCollection collection)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(collection["ActionId"]))
                {
                    return null;
                }

                int actionId = int.Parse(collection["ActionId"]);
                string searchDepartmentId = string.Empty;
                string searchDepartmentName = string.Empty;
                SearchCriterialDto criterial = new SearchCriterialDto();

                if (actionId == (int)PageAction.Search)
                {
                    criterial.Page = 1;
                    criterial.PageSize = ConfigHelper.PageSize;
                    searchDepartmentId = collection["txtDepartmentId"];
                    searchDepartmentName = collection["txtDepartmentName"];
                }

                if (!String.IsNullOrWhiteSpace(searchDepartmentId))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "DepartmentId", Value = searchDepartmentId });
                }

                if (!String.IsNullOrWhiteSpace(searchDepartmentName))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "DepartmentName", Value = searchDepartmentName });
                }

                //Get Data
                DepartmentListViewModel vm = GetDepartmentListViewModel(criterial);
                vm.SearchId = searchDepartmentId;
                vm.SearchName = searchDepartmentName;

                return View("DepartmentList", vm);
            }
            catch (Exception ex)
            {
                LogHelper.Error(System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        private DepartmentListViewModel GetDepartmentListViewModel(SearchCriterialDto criterial)
        {
            DepartmentListViewModel vm = new DepartmentListViewModel();
            DepartmentService srv = new DepartmentService();

            int totalRecord = 0;
            vm.DepartmentList = srv.GetDepartmentList(criterial, ref totalRecord);

            //if (criterial.Page != null && criterial.PageSize != null)
            //{
            //    PageViewModel paging = new PageViewModel(criterial.Page.Value,
            //        criterial.PageSize.Value, totalRecord, "DepartmentListPaging");
            //    vm.PagingModel = paging;
            //}

            return vm;
        }



        // GET: Department/Create
        [HttpPost]
        public ActionResult Update(FormCollection collection)
        {
            try
            {
                if (SessionHelper.Session.User.Role == UserRole.ExternalTrainer ||
                SessionHelper.Session.User.Role == UserRole.Staff)
                {
                    return RedirectToAction("ClassList", "Class");
                }

                var oldDepartmentId = collection["hidDepartmentId"];
                DepartmentDto dto = new DepartmentDto();

                if (!String.IsNullOrEmpty(oldDepartmentId)) //Update
                {
                    SearchCriterialDto criterial = new SearchCriterialDto { Page = 1, PageSize = 1 };
                    criterial.SearchFieldList.Add(new KeyDto { Key = "DepartmentIds", Value = oldDepartmentId });
                    int totalRecord = 0;

                    dto = DepartmentService.Instance.GetDepartmentList(criterial, ref totalRecord).FirstOrDefault();
                    if (dto == null)
                    {
                        RedirectToAction("Login", "Account");
                    }

                }
                dto.DepartmentId = collection["txtDepartmentID"];
                dto.DepartmentName = collection["txtDepartmentName"];


                if (!String.IsNullOrEmpty(oldDepartmentId))
                {
                    //If departmentId change -> Check whether deparment have any existed course
                    if (!string.IsNullOrEmpty(oldDepartmentId) && oldDepartmentId != dto.DepartmentId)
                    {
                        int existedCourse = DepartmentService.Instance.GetNumberDepartmentCourse(oldDepartmentId);

                        if (existedCourse > 0)
                        {
                            RedirectToAction("Login", "Account");
                        }
                    }

                    DepartmentService.Instance.UpdateDepartment(dto, oldDepartmentId);
                }
                else
                {
                    DepartmentService.Instance.CreateNewDepartment(dto);
                }
                return RedirectToAction("DepartmentList");
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                return View();
            }
        }

        // GET: Department/Edit/5
        public ActionResult DepartmentUpdate(string id = null)
        {
            DepartmentDto vm = new DepartmentDto();

            if (!string.IsNullOrEmpty(id)) //Update 
            {
                SearchCriterialDto criterial = new SearchCriterialDto { Page = 1, PageSize = 1 };
                criterial.SearchFieldList.Add(new KeyDto { Key = "DepartmentIds", Value = id });
                int totalRecord = 0;

                var department = DepartmentService.Instance.GetDepartmentList(criterial, ref totalRecord).FirstOrDefault();

                if (department != null)
                {
                    vm = department;
                }
            }

            return View(vm);
        }

        public JsonResult CheckExistedDepartmentId(string departmentId, string originalId)
        {
            try
            {
                if (originalId == string.Empty || originalId != departmentId)
                {
                    //If departmentId change -> Check whether deparment have any existed course
                    if (!string.IsNullOrEmpty( originalId) && originalId != departmentId)
                    {
                        int existedCourse = DepartmentService.Instance.GetNumberDepartmentCourse(originalId);

                        if(existedCourse > 0)
                        {
                            string msg = "Unable edit Department ID. There are existed Courses under This Department!";
                            return Json(msg, JsonRequestBehavior.AllowGet);
                        }
                    }

                    SearchCriterialDto criterial = new SearchCriterialDto { Page = 1, PageSize = 1 };
                    criterial.SearchFieldList.Add(new KeyDto { Key = "DepartmentIds", Value = departmentId });
                    int totalRecord = 0;

                    var dto = DepartmentService.Instance.GetDepartmentList(criterial, ref totalRecord).FirstOrDefault();

                    if (dto != null && !string.IsNullOrEmpty(dto.DepartmentId) )
                    {
                        string msg = "Department ID already existed!";
                        return Json(msg, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }

            return Json("1", JsonRequestBehavior.AllowGet);
        }
    }
}
