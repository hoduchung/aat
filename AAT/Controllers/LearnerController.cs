﻿using AAT.Business;
using AAT.Model;
using AAT.Model.Utils;
using AAT.Model.ViewModel;
using AAT.UnisonService;
using AAT.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace AAT.Controllers
{
    public class LearnerController : BaseController
    {
        public ActionResult ExportPDF(string classID, string staffId)
        {
            try
            {
                //Step 1: GetData 
                LearnerPresentViewModel vm = LearnerService.Instance.GetLearnerPresentViewModel(classID, staffId);

                //S2: Export Excel
                PdfExport helper = new PdfExport();
                Byte[] fileBytes = helper.LearnExportPdf(vm);
                string docName = classID + "_" + staffId + ".pdf";
                string mimeType = MimeMapping.GetMimeMapping(docName);

                return File(fileBytes, mimeType, docName);

            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }

            return null;
        }

        public ActionResult LearnerList_ExportPDF(List<LearnerDto> learnerList, SearchCriterialDto criterial)
        {
            try
            {

                ExportHelper export = new ExportHelper();
                string html_string = export.GetLearnerHistoryHTML(learnerList, criterial, FileExtenstion.PDF);

                //S2: Export Excel
                PdfExport helper = new PdfExport();
                Byte[] fileBytes = helper.GetPDFBytes(html_string);
                string docName = "Learner_" + learnerList[0].StaffID + ".pdf";
                string mimeType = MimeMapping.GetMimeMapping(docName);

                return File(fileBytes, mimeType, docName);

            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }

            return null;
        }

        public ActionResult LearnerList_ExportExcel(List<LearnerDto> learnerList, SearchCriterialDto criterial)
        {
            try
            {
                //S2: Export Excel
                ExcelExport helper = new ExcelExport();
                Byte[] fileBytes = helper.ExportLearnerHistory(learnerList, criterial);
                string docName = "Learner_" + learnerList[0].StaffID + ".xlsx";

                string mimeType = MimeMapping.GetMimeMapping(docName);
                return File(fileBytes, mimeType, docName);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }

            return null;
        }

        public ActionResult LearnerList_ExportWord(List<LearnerDto> learnerList, SearchCriterialDto criterial)
        {
            try
            {

                ExportHelper export = new ExportHelper();
                string strBody = export.GetLearnerHistoryHTML(learnerList, criterial, FileExtenstion.WORD);

                // Force this content to be downloaded 
                // as a Word document with the name of your choice
                string docName = "Learner_" + learnerList[0].StaffID + ".doc";
                string mimeType = MimeMapping.GetMimeMapping(docName);
                Response.AppendHeader("Content-Type", "application/msword");

                Response.AppendHeader("Content-disposition", "attachment; filename=" + docName);
                Response.Write(strBody);

            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }

            return null;
        }

        public ActionResult ExportExcel(string classID, string staffId)
        {
            try
            {
                //Step 1: GetData 
                LearnerPresentViewModel vm = LearnerService.Instance.GetLearnerPresentViewModel(classID, staffId);

                //S2: Export Excel
                ExcelExport helper = new ExcelExport();
                Byte[] fileBytes = helper.ExportLearner(vm);
                string docName = classID + "_" + staffId + ".xlsx";
                string mimeType = MimeMapping.GetMimeMapping(docName);

                return File(fileBytes, mimeType, docName);
                 
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }

            return null;
        }

        public ActionResult ExportWord(string classID, string staffId)
        {
            try
            {
                ExportHelper export = new ExportHelper();
                //Step 1: GetData 
                LearnerPresentViewModel vm = LearnerService.Instance.GetLearnerPresentViewModel(classID, staffId);
                string strBody = export.GetLearnerHTML(vm, FileExtenstion.WORD);

                // Force this content to be downloaded 
                // as a Word document with the name of your choice
                string docName = classID + "_" + staffId + ".doc";
                string mimeType = MimeMapping.GetMimeMapping(docName);
                Response.AppendHeader("Content-Type", "application/msword");

                Response.AppendHeader("Content-disposition", "attachment; filename=" + docName);
                Response.Write(strBody);

            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }

            return null;
        }

        // GET: Learner
        public ActionResult LearnerList()
        {
            if (SessionHelper.Session.User.Role == UserRole.ExternalTrainer ||
                SessionHelper.Session.User.Role == UserRole.Staff)
            {
                return RedirectToAction("ClassList", "Class");
            }

            SearchCriterialDto criterial = new SearchCriterialDto
            {
                Page = 1,
                PageSize = ConfigHelper.PageSize
            };

            criterial.SearchFieldList.Add(new KeyDto { Key = "SortColumn", Value = "StaffID" });
            criterial.SearchFieldList.Add(new KeyDto { Key = "SortOrder", Value = "ASC" });

            LearnerListViewModel vm = GetLearnerListViewModel(criterial);

            vm.SortColumn = "StaffID";
            vm.SortOrder = "ASC";
            return View(vm);
        }

        private LearnerListViewModel GetLearnerListViewModel(SearchCriterialDto criterial)
        {
            LearnerListViewModel vm = new LearnerListViewModel();
            LearnerService srv = new LearnerService();

            int totalRecord = 0;
            vm.LearnerList = srv.GetLearnerList(criterial, ref totalRecord);

            if (criterial.Page != null && criterial.PageSize != null)
            {
                PageViewModel paging = new PageViewModel(criterial.Page.Value,
                    criterial.PageSize.Value, totalRecord, "LearnerListPaging");
                vm.PagingModel = paging;
            }

            return vm;
        }

        public ActionResult LearnerListAction(FormCollection collection)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(collection["ActionId"]))
                {
                    return null;
                }

                int actionId = int.Parse(collection["ActionId"]);
                string searchStartDate = string.Empty;
                string searchEndDate = string.Empty;
                string searchCourseId = string.Empty;
                string searchCourseName = string.Empty;
                string searchClassId = string.Empty;
                string searchStaffId = string.Empty;
                string searchLearnerName = string.Empty;
                string searchStatus = string.Empty;
                string sortColumn = string.Empty;
                string sortOrder = string.Empty;

                SearchCriterialDto criterial = new SearchCriterialDto();
                #region Action Type
                if (actionId == (int)PageAction.Pagging)
                {
                    if (String.IsNullOrWhiteSpace(collection["Page"]))
                    {
                        return null;
                    }

                    criterial.Page = Int16.Parse(collection["Page"]);
                    criterial.PageSize = ConfigHelper.PageSize;
                    searchStartDate = collection["hidStartDate"];
                    searchEndDate = collection["hidEndDate"];
                    searchCourseId = collection["hidCourseId"];
                    searchCourseName = collection["hidCourseName"];
                    searchClassId = collection["hidClassId"];
                    searchStaffId = collection["hidStaffId"];
                    searchLearnerName = collection["hidLearnerName"];
                    searchStatus = collection["hidSearchStatus"];
                    sortColumn = collection["hidSortColumn"];
                    sortOrder = collection["hidSortOrder"];
                }
                else if (actionId == (int)PageAction.Search)
                {
                    criterial.Page = 1;
                    criterial.PageSize = ConfigHelper.PageSize;
                    searchStartDate = collection["txtStartDate"];
                    searchEndDate = collection["txtEndDate"];
                    searchCourseId = collection["txtCourseId"];
                    searchCourseName = collection["txtCourseName"];
                    searchClassId = collection["txtClassId"];
                    searchStaffId = collection["txtStaffId"];
                    searchLearnerName = collection["txtLearnerName"];
                    searchStatus = collection["ddlStatus"];
                }
                else if (actionId == (int)PageAction.Sorting)
                {
                    criterial.Page = 1;
                    criterial.PageSize = ConfigHelper.PageSize;
                    searchStartDate = collection["hidStartDate"];
                    searchEndDate = collection["hidEndDate"];
                    searchCourseId = collection["hidCourseId"];
                    searchCourseName = collection["hidCourseName"];
                    searchClassId = collection["hidClassId"];
                    searchStaffId = collection["hidStaffId"];
                    searchLearnerName = collection["hidLearnerName"];
                    searchStatus = collection["hidSearchStatus"];
                    sortColumn = collection["hidSortColumn"];
                    sortOrder = collection["hidSortOrder"];
                }
                else if (actionId == (int)PageAction.Export)
                {
                    criterial.Page = 1;
                    criterial.PageSize = 99999;
                    searchStartDate = collection["hidStartDate"];
                    searchEndDate = collection["hidEndDate"];
                    searchCourseId = collection["hidCourseId"];
                    searchCourseName = collection["hidCourseName"];
                    searchClassId = collection["hidClassId"];
                    searchStaffId = collection["hidStaffId"];
                    searchLearnerName = collection["hidLearnerName"];
                    searchStatus = collection["hidSearchStatus"];
                    sortColumn = collection["hidSortColumn"];
                    sortOrder = collection["hidSortOrder"];
                }
                #endregion Action Type

                #region Criterial Field
                if (!String.IsNullOrWhiteSpace(searchStartDate))
                {
                    DateTime startDate = DateHelper.ParseDate(searchStartDate);
                    criterial.SearchFieldList.Add(new KeyDto { Key = "StartDate", Value = startDate });
                }

                if (!String.IsNullOrWhiteSpace(searchEndDate))
                {
                    DateTime endDate = DateHelper.ParseDate(searchEndDate);
                    criterial.SearchFieldList.Add(new KeyDto { Key = "EndDate", Value = endDate });
                }

                if (!String.IsNullOrWhiteSpace(searchCourseId))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "CourseId", Value = searchCourseId.Trim() });
                }

                if (!String.IsNullOrWhiteSpace(searchCourseName))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "CourseName", Value = searchCourseName.Trim() });
                }

                if (!String.IsNullOrWhiteSpace(searchClassId))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "ClassId", Value = searchClassId.Trim() });
                }

                if (!String.IsNullOrWhiteSpace(searchStaffId))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "StaffId", Value = searchStaffId.Trim() });
                }

                if (!String.IsNullOrWhiteSpace(searchLearnerName))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "LearnerName", Value = searchLearnerName.Trim() });
                }

                if (!String.IsNullOrWhiteSpace(searchStatus))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "ClassStatusID", Value = searchStatus });
                }

                if (actionId != (int)PageAction.Export)
                {
                    if (!String.IsNullOrWhiteSpace(sortColumn))
                    {
                        criterial.SearchFieldList.Add(new KeyDto { Key = "SortColumn", Value = sortColumn });
                    }

                    if (!String.IsNullOrWhiteSpace(sortOrder))
                    {
                        criterial.SearchFieldList.Add(new KeyDto { Key = "SortOrder", Value = sortOrder });
                    }
                }
                #endregion Criterial Field

                List<LearnerDto> learnerExportList = null;
                if (actionId == (int)PageAction.Export)
                {
                     learnerExportList = LearnerService.Instance.GetLearnerHistory(criterial);

                    if(learnerExportList.Any())
                    {
                        string exportType = string.Empty;
                        exportType = collection["ExportType"];

                        switch (exportType)
                        {
                            case "PDF":
                                return LearnerList_ExportPDF(learnerExportList, criterial);
                            case "WORD":
                                return LearnerList_ExportWord(learnerExportList, criterial);
                            case "EXCEL":
                                return LearnerList_ExportExcel(learnerExportList, criterial);
                        }
                    }

                }

                //Get Data
                LearnerListViewModel vm = GetLearnerListViewModel(criterial);
                vm.SearchStartDate = searchStartDate;
                vm.SearchEndtDate = searchEndDate;
                vm.SearchCourseId = searchCourseId;
                vm.SearchCourseName = searchCourseName;
                vm.SearchClassId = searchClassId;
                vm.SearchStafffId = searchStaffId;
                vm.SearchLearnerName = searchLearnerName;
                vm.SearchStatus = searchStatus;
                vm.PageSize = ConfigHelper.PageSize;
                vm.SortColumn = sortColumn;
                vm.SortOrder = sortOrder;

                if (actionId == (int)PageAction.Export && !learnerExportList.Any())
                {
                    vm.ErrorMessage = "No data found!";
                }
                return View("LearnerList", vm);
            }
            catch (Exception ex)
            {
                LogHelper.Error("ServiceCall - : " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        // GET: Learner/Details?classId=1&learnerId=1
        public ActionResult Details(string classId, string staffId)
        {
            try
            {
                LearnerPresentViewModel vm = LearnerService.Instance.GetLearnerPresentViewModel(classId, staffId);
               
                if(ConfigHelper.AppEviroment != "debug")
                {
                    try
                    {
                        UserInfo user = GlobalData.UnisonService.GetUserById(staffId);
                        UserImagesInfo image = GlobalData.UnisonService.GetUserImagesByUserKey(user.Key);
                        if (image != null)
                        {
                            byte[] photo = image.Photo;
                            vm.Learner64ImageString = "data:image/gif;base64," + ImageHelper.ConvertToBase64FromByteArr(photo);
                        }
                    }
                    catch (Exception ex)
                    {
                        LogHelper.Error("ServiceCall - : " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                    }
                }

                return View(vm);
            }
            catch (Exception ex)
            {
                LogHelper.Error("ServiceCall - : " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
           
        }

        public ActionResult EditLearnerSuccessfulStatusAction(FormCollection collection)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(collection["ActionId"]))
                {
                    return null;
                }

                int actionId = int.Parse(collection["ActionId"]);

                LearnerClassDto dto = new LearnerClassDto();
                dto.StaffId = collection["hidStaffID"].ToString();
                dto.Success = collection["hidLearnerSuccess"].ToString();
                dto.ClassId = collection["hidClassID"].ToString();
                dto.Remark = collection["hidLearnerSuccessRemark"].ToString();
                dto.UpdateBy = SessionHelper.Session.User.LoginName;

                if (actionId == PageAction.Update.GetHashCode())
                {
                    if (String.IsNullOrWhiteSpace(collection["hidEditLearnerStatusAction"]))
                    {
                        return null;
                    }

                    string remarkAction = collection["hidEditLearnerStatusAction"];
                    if (remarkAction == "EDIT")
                    {
                        LearnerService.Instance.UpdateLearnerClassSuccessfulStatus(dto);
                    }
                }

                //LearnerPresentViewModel vm = LearnerService.Instance.GetLearnerPresentViewModel(dto.ClassId, dto.StaffId);
                return RedirectToAction("Details", new { classId = dto.ClassId, staffId = dto.StaffId });

            }
            catch (Exception ex)
            {
                LogHelper.Error("ServiceCall - : " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

    }
}
