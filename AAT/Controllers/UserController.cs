﻿using AAT.Business;
using AAT.Model;
using AAT.Model.Utils;
using AAT.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAT.Controllers
{
    public class UserController : BaseController
    {
        // GET: User
        public ActionResult UserList()
        {
            try
            {
                if (SessionHelper.Session.User.Role == UserRole.ExternalTrainer ||
                SessionHelper.Session.User.Role == UserRole.Staff)
                {
                    return RedirectToAction("ClassList", "Class");
                }

                SearchCriterialDto criterial = new SearchCriterialDto { Page = 1, PageSize = ConfigHelper.PageSize };
                criterial.SearchFieldList.Add(new KeyDto { Key = "SortColumn", Value = "LoginName" });
                criterial.SearchFieldList.Add(new KeyDto { Key = "SortOrder", Value = "ASC" });
                UserListViewModel vm = GetUserListViewModel(criterial);

                vm.SortColumn = "LoginName";
                vm.SortOrder = "ASC";
                return View(vm);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       
        private UserListViewModel GetUserListViewModel(SearchCriterialDto criterial)
        {
            UserListViewModel vm = new UserListViewModel();
            UserService srv = new UserService();

            int totalRecord = 0;
            vm.UserList = srv.GetUserList(criterial, ref totalRecord);

            if (criterial.Page != null && criterial.PageSize != null)
            {
                PageViewModel paging = new PageViewModel(criterial.Page.Value,
                    criterial.PageSize.Value, totalRecord, "UserListPaging");
                vm.PagingModel = paging;
            }

            criterial = new SearchCriterialDto { Page = 1, PageSize = 999 };
            vm.DepartmentList = DepartmentService.Instance.GetDepartmentList(criterial, ref totalRecord);

            return vm;
        }


        public JsonResult CheckExistedLoginName(string loginname, string originalname)
        {
            try
            {
                if(originalname == string.Empty || originalname != loginname)
                {
                    UserService srv = new UserService();
                    UserDto dto = srv.GetUserLogin(loginname);
                    if (dto != null && dto.LoginName.Trim().Length > 0)
                    {
                        string msg = "Login name already existed!";
                        return Json(msg, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }

            return Json("1", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UserListAction(FormCollection collection)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(collection["ActionId"]))
                {
                    return null;
                }

                int actionId = int.Parse(collection["ActionId"]);
                string searchLoginName = string.Empty;
                string searchType = string.Empty;
                string searchRole = string.Empty;
                string searchPhone = string.Empty;
                string searchName = string.Empty;
                string searchStatus = string.Empty;
                string searchDepartment = string.Empty;
                string searchEmail = string.Empty;
                string sortColumn = string.Empty;
                string sortOrder = string.Empty;

                SearchCriterialDto criterial = new SearchCriterialDto();

                if (actionId == (int)PageAction.Pagging)
                {
                    if (String.IsNullOrWhiteSpace(collection["Page"]))
                    {
                        return null;
                    }

                    criterial.Page = Int16.Parse(collection["Page"]);
                    criterial.PageSize = ConfigHelper.PageSize;
                    searchLoginName = collection["hidLoginName"];
                    searchType = collection["hidType"];
                    searchRole = collection["hidRole"];
                    searchPhone = collection["hidPhone"];
                    searchName = collection["hidName"];
                    searchStatus = collection["hidStatus"];
                    searchDepartment = collection["hidDepartment"];
                    searchEmail = collection["hidEmail"];
                    sortColumn = collection["hidSortColumn"];
                    sortOrder = collection["hidSortOrder"];
                }
                else if (actionId == (int)PageAction.Search)
                {
                    criterial.Page = 1;
                    criterial.PageSize = ConfigHelper.PageSize;
                    searchLoginName = collection["txtLoginName"];
                    searchType = collection["ddlType"];
                    searchRole = collection["ddlRole"];
                    searchPhone = collection["txtPhone"];
                    searchName = collection["txtName"];
                    searchStatus = collection["ddlStatus"];
                    searchDepartment = collection["ddlDepartment"];
                    searchEmail = collection["txtEmail"];

                }
                else if (actionId == (int)PageAction.Sorting)
                {
                    criterial.Page = 1;
                    criterial.PageSize = ConfigHelper.PageSize;
                    searchLoginName = collection["hidLoginName"];
                    searchType = collection["hidType"];
                    searchRole = collection["hidRole"];
                    searchPhone = collection["hidPhone"];
                    searchName = collection["hidName"];
                    searchStatus = collection["hidStatus"];
                    searchDepartment = collection["hidDepartment"];
                    searchEmail = collection["hidEmail"];
                    sortColumn = collection["hidSortColumn"];
                    sortOrder = collection["hidSortOrder"];
                }

                if (!String.IsNullOrWhiteSpace(searchLoginName))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "LoginName", Value = searchLoginName });
                }

                if (!String.IsNullOrWhiteSpace(searchType))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "UserTypeId", Value = searchType });
                }

                if (!String.IsNullOrWhiteSpace(searchRole))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "RoleId", Value = searchRole });
                }

                if (!String.IsNullOrWhiteSpace(searchPhone))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "Phone", Value = searchPhone });
                }

                if (!String.IsNullOrWhiteSpace(searchName))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "Name", Value = searchName });
                }

                if (!String.IsNullOrWhiteSpace(searchStatus))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "StatusId", Value = searchStatus });
                }

                if (!String.IsNullOrWhiteSpace(searchDepartment))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "DepartmentId", Value = searchDepartment });
                }

                if (!String.IsNullOrWhiteSpace(searchEmail))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "Email", Value = searchEmail });
                }

                if (!String.IsNullOrWhiteSpace(sortColumn))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "SortColumn", Value = sortColumn });
                }

                if (!String.IsNullOrWhiteSpace(sortOrder))
                {
                    criterial.SearchFieldList.Add(new KeyDto { Key = "SortOrder", Value = sortOrder });
                }

                //Get Data
                UserListViewModel vm = GetUserListViewModel(criterial);
                vm.SearchLoginName = searchLoginName;
                vm.SearchTypeId = searchType;
                vm.SearchRoleId = searchRole;
                vm.SearchPhone = searchPhone;
                vm.SearchName = searchName;
                vm.SearchStatusId = searchStatus;
                vm.SearchDepartmentId = searchDepartment;
                vm.SearchEmail = searchEmail;
                vm.PageSize = criterial.PageSize.Value;
                vm.SortColumn = sortColumn;
                vm.SortOrder = sortOrder;

                return View("UserList", vm);
            }
            catch (Exception ex)
            {
                LogHelper.Error("UserControler - : " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        // GET: User/Create
        public ActionResult UserUpdate(int? id)
        {
            if (SessionHelper.Session.User.Role == UserRole.ExternalTrainer ||
                SessionHelper.Session.User.Role == UserRole.Staff)
            {
                return RedirectToAction("ClassList", "Class");
            }

            UserViewModel vm = new UserViewModel();

            SearchCriterialDto criterial = new SearchCriterialDto { Page = 1, PageSize = 999 };
            criterial.SearchFieldList.Add(new KeyDto { Key = "IsActive", Value = 1 });
            int totalRecord = 0;

            vm.DepartmentList = DepartmentService.Instance.GetDepartmentList(criterial, ref totalRecord);
            vm.RoleList = EnumsHelper.GetUserRoleList();
            vm.Title = "User: Create";

            //Set Default value on Create
            vm.User.Role = UserRole.ExternalTrainer;
            vm.User.UserType = UserType.Basic;


            if(id != null) //Update 
            {
                criterial = new SearchCriterialDto { Page = 1, PageSize = 999 };
                criterial.SearchFieldList.Add(new KeyDto { Key = "UserId", Value = id });

                vm.User = UserService.Instance.GetUserList(criterial, ref totalRecord).FirstOrDefault();

                if(vm.User != null)
                {
                    vm.Title = "User: Update";
                    vm.User.Password = SHA1Helper.PasswordMark;
                }
            }

            return View(vm);
        }

        public JsonResult UpdateStatus(string userId, int userStatus)
        {
            if (SessionHelper.Session.User.Role != UserRole.SuperAdmin &&
                SessionHelper.Session.User.Role != UserRole.Admin)
            {
                return Json("Unauthorize Action!", JsonRequestBehavior.AllowGet);
            }

            try
            {
                SearchCriterialDto criterial = new SearchCriterialDto { Page = 1, PageSize = 999 };
                criterial.SearchFieldList.Add(new KeyDto { Key = "UserId", Value = userId });
                int totalRecord = 0;

                var dto = UserService.Instance.GetUserList(criterial, ref totalRecord).FirstOrDefault();

                if (dto == null)
                {
                    RedirectToAction("Login", "Account");
                }
                dto.Status = (StatusType)userStatus;

                UserService.Instance.UpdateUser(dto);

                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ResetPassword(string resetUserId, string userPass)
        {
            if (SessionHelper.Session.User.Role != UserRole.SuperAdmin &&
                SessionHelper.Session.User.Role != UserRole.Admin)
            {
                return Json("Unauthorize Action!", JsonRequestBehavior.AllowGet);
            }

            try
            {
                SearchCriterialDto criterial = new SearchCriterialDto { Page = 1, PageSize = 999 };
                criterial.SearchFieldList.Add(new KeyDto { Key = "UserId", Value = resetUserId });
                int totalRecord = 0;

                var dto = UserService.Instance.GetUserList(criterial, ref totalRecord).FirstOrDefault();

                if (dto == null)
                {
                    RedirectToAction("Login", "Account");
                }
                dto.Password = SHA1Helper.GetPasswordHash(userPass);

                UserService.Instance.UpdateUser(dto);

                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: CustomerController - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult Update(FormCollection collection)
        {
            try
            {
                var UserId = collection["UserId"];
                UserDto dto = new UserDto();
                dto.Password = SHA1Helper.GetPasswordHash(collection["Password"].ToString()); //Must in sequent

                if (!String.IsNullOrEmpty(UserId) && UserId != "0") //Update
                {
                    SearchCriterialDto criterial = new SearchCriterialDto { Page = 1, PageSize = 999 };
                    criterial.SearchFieldList.Add(new KeyDto { Key = "UserId", Value = UserId });
                    int totalRecord = 0;
                    dto = UserService.Instance.GetUserList(criterial, ref totalRecord).FirstOrDefault();

                    if(dto == null)
                    {
                        RedirectToAction("Login", "Account");
                    }

                    string password = collection["Password"].ToString();
                    if(password != SHA1Helper.PasswordMark) 
                    {
                        dto.Password = SHA1Helper.GetPasswordHash(collection["Password"].ToString());
                    }

                }
                dto.UserType = (UserType)int.Parse(collection["UserTypeId"]);
                dto.Name = collection["FullName"];
                dto.LoginName = collection["Loginname"];
                dto.Email = collection["Email"];
                dto.Phone = collection["Phone"];
                

                dto.Role = (UserRole)int.Parse(collection["RoleId"]);
                if (dto.Role ==  UserRole.SuperAdmin)
                {
                    dto.DepartmentId = string.Empty; 
                }
                else
                {
                    dto.DepartmentId = collection["Department"];
                }


                //int statusId = 0;   //Inactive
                //if(collection["IsActive"] != null && collection["IsActive"].ToString().ToLower() == "on")
                //{
                //    statusId = 1; //Active
                //}
                //dto.Status = (StatusType)statusId;

                if (!String.IsNullOrEmpty(UserId) && UserId != "0")
                {
                    UserService.Instance.UpdateUser(dto);
                }
                else
                {
                    UserService.Instance.CreateNewUser(dto);
                }
                return RedirectToAction("UserList");
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
