function setCookie(name, value) {
	var days = 7;
	var expires = "";
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		expires = "; expires=" + date.toUTCString();
	}
	document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
	}
	return null;
}


getServerData = function(urlController, parameter, callBack) {
	return $.ajax({
		url: urlController,
		data: JSON.stringify(parameter),
		async: true,
		dataType: "json",
		traditional: true,
		contentType: 'application/json, charset=utf-8',
		type: "POST",
		success: function (result) {
			var ret;
			ret = result;
			return callBack(ret);
		},
		error: function (errorThrown) {
			return false;
		}
	});
};

function IsValidEmail(email) {
	var atpos = email.indexOf("@");
	var dotpos = email.lastIndexOf(".");
	if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
		return false;
	}
	return true;
}

function GoURL(urlstr) {
	window.location.href = urlstr;
}