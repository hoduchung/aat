﻿using AAT.Model;
using AAT.Model.Utils;
using AAT.Model.ViewModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace AAT.Utils
{
    public class ExcelExport
    {
        public byte[] ExportClassExcel(ClassReportDto classReport)
        {
            try
            {
                Byte[] bytes;
                ClassDto oClass = classReport.Class;
                ExportHelper exportHelper = new ExportHelper();
                string fileSource = exportHelper.GetExportFileTeamplex(true, FileExtenstion.EXCEL);

                using (var templateXls = new ExcelPackage(new FileInfo(fileSource)))
                {
                    var sheet = templateXls.Workbook.Worksheets["Course"];

                    #region Header
                    sheet.Cells["A2"].Value = "Generated on: " + DateHelper.FormatDateTime(DateTime.Now);
                    sheet.Cells["A6"].Value = "Course Title : " + oClass.CourseName;
                    sheet.Cells["A7"].Value = "Course Duration : " + classReport.Duration.ToString() + " day(s)";
                    sheet.Cells["A8"].Value = "Learners Registered : " + classReport.TotalRegister.ToString();

                    sheet.Cells["A6:C6"].Merge = true;
                    sheet.Cells["A6"].Style.WrapText = true;

                    if (oClass.CourseName.Length > 45)
                    {
                        sheet.Row(6).Height = 34;
                    }

                    int index=12;
                    if (oClass.CourseId.StartsWith("CCT"))
                    {
                        sheet.Cells["A9:C9"].Merge = true;
                        sheet.Cells["A9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        sheet.Cells["A9"].Style.Font.Bold = true;
                        sheet.Cells["A9"].Style.Font.Size = 14;
                        sheet.Cells["A9"].Value = "Daily Training starts @ 0845 hrs & ends @ 1715 hrs";
                        
                    }
                    else
                    {
                        sheet.Row(9).Height = 0;
                        sheet.Row(9).Hidden = true;
                    }

                    sheet.Cells["D6"].Value = "Class ID : " + classReport.Class.ClassID;
                    sheet.Cells["D7"].Value = "Start Date : " + DateHelper.FormatDateMonthInName(oClass.StartDate);
                    sheet.Cells["D8"].Value = "End Date : " + DateHelper.FormatDateMonthInName(oClass.EndDate);
                    #endregion Header

                    #region Learner Body
                    Color rowbg;
                    sheet.Row(11).Height = 20;
                    ExcelBorderStyle cellstyle = ExcelBorderStyle.Thin;
                    string previous_row_date_value;
                    int leaner_row_count=1;
                    for (int i = 0; i < classReport.LearnerClassDateList.Count; i++)
                    {
                        var item = classReport.LearnerClassDateList;
                        if (i != 0)
                        {
                            previous_row_date_value = DateHelper.FormatDateMonthInName(item[i - 1].ClassDate);
                            if (previous_row_date_value != DateHelper.FormatDateMonthInName(item[i].ClassDate)){

                                //header row
                                sheet.Cells["A" + index].Value = "Sr.";
                                sheet.Cells["B" + index].Value = "Name";
                                sheet.Cells["C" + index].Value = "Staff ID";
                                sheet.Cells["D" + index].Value = "Scan Date";
                                sheet.Cells["E" + index].Value = "Time In";

                                //style
                                sheet.Row(index).Style.Font.Color.SetColor(Color.White);
                                sheet.Row(index).Height = 19.8;
                                sheet.Row(index).Style.Font.Bold = true;
                                sheet.Row(index).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                sheet.Cells["A" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                sheet.Cells["A" + index].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));
                                sheet.Cells["B" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                sheet.Cells["B" + index].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));
                                sheet.Cells["B" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                sheet.Cells["C" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                sheet.Cells["C" + index].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));
                                sheet.Cells["D" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                sheet.Cells["D" + index].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));
                                sheet.Cells["E" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                sheet.Cells["E" + index].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));

                                index++;
                                leaner_row_count = 1;
                            }
                        }
                        sheet.Cells["A" + index].Value = leaner_row_count;
                        sheet.Cells["B" + index].Value = item[i].LearnerName;
                        sheet.Cells["C" + index].Value = item[i].StaffId;
                        sheet.Cells["D" + index].Value = DateHelper.FormatDateMonthInName(item[i].ClassDate);
                        sheet.Cells["E" + index].Value = DateHelper.FormatTime(item[i].CheckInTime);
                        //Stype
                        rowbg = (leaner_row_count % 2 == 1) ? Color.FromArgb(255, 255, 255) : Color.FromArgb(208, 233, 247);
                        sheet.Cells["A" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //sheet.Cells["A" + index].Style.Border.Right.Style = cellstyle;
                        sheet.Cells["A" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                        sheet.Cells["B" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells["B" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                        sheet.Cells["C" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells["C" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                        sheet.Cells["D" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells["D" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                        sheet.Cells["E" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells["E" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                        sheet.Row(index).Height = 17;
                        index++;
                        leaner_row_count++;
                    }

                    //Format Index column
                    #endregion Learner Body

                    index += 2;
                    //Trainer
                    sheet.Cells["A" + index + ":D" + index].Style.Font.Bold = true;
                    sheet.Cells["A" + index + ":D" + index].Style.Font.Color.SetColor(Color.White);
                    sheet.Cells["A" + index].Value = "Sr.";
                    sheet.Cells["B" + index].Value = "Instructor Name";
                    sheet.Cells["C" + index].Value = "Acknowledgement";
                    sheet.Cells["D" + index].Value = "Date Acknowledged";
                    sheet.Cells["D" + index + ":E" + index].Merge = true;

                    //Style
                    sheet.Row(index).Height = 20;
                    rowbg = Color.FromArgb(79, 129, 189);
                    sheet.Row(index).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    sheet.Cells["A" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["A" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["B" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["B" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["C" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["C" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["D" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["D" + index].Style.Fill.BackgroundColor.SetColor(rowbg);

                    index++;
                    int value = int.Parse("252", System.Globalization.NumberStyles.HexNumber);
                    string checkSymbol = string.Empty;
                    int rowNum = 0;
                    int trainerRowNo = 0;
                    foreach (var item in classReport.Class.TrainerList)
                    {
                        rowNum = index + 1;
                        if (item.AcknowledgementDate != null)
                        {
                            checkSymbol = ((char)0x221A).ToString();
                        }

                        trainerRowNo++;
                        sheet.Cells["A" + index].Value = trainerRowNo;
                        sheet.Cells["B" + index].Value = item.TrainerName;
                        sheet.Cells["C" + index].Value = checkSymbol;//252
                        sheet.Cells["D" + index].Value = DateHelper.FormatDateMonthInName(item.AcknowledgementDate);

                        //Style
                        sheet.Row(index).Height = 17;
                        sheet.Cells["D" + index + ":E" + index].Merge = true;
                        rowbg = (trainerRowNo % 2 == 1) ? Color.FromArgb(255, 255, 255) : Color.FromArgb(208, 233, 247);
                        sheet.Cells["A" + index].Style.Font.Bold = false;
                        sheet.Cells["A" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells["A" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                        sheet.Cells["B" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells["B" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                        sheet.Cells["C" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells["C" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                        sheet.Cells["D" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells["D" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                        index++;
                    }

                    foreach (var item in classReport.Class.AcknowledgementExtraList)
                    {
                        rowNum = index + 1;
                        trainerRowNo++;
                        if (item.AcknowledgementDate != null)
                        {
                            checkSymbol = ((char)0x221A).ToString();
                        }

                        sheet.Cells["A" + index].Value = trainerRowNo;
                        sheet.Cells["B" + index].Value = item.TrainerName;
                        sheet.Cells["C" + index].Value = checkSymbol;//252
                        sheet.Cells["D" + index].Value = DateHelper.FormatDateMonthInName(item.AcknowledgementDate);

                        //Style
                        sheet.Row(index).Height = 17;
                        sheet.Cells["D" + index + ":E" + index].Merge = true;
                        rowbg = (trainerRowNo % 2 == 1) ? Color.FromArgb(255, 255, 255) : Color.FromArgb(208, 233, 247);
                        sheet.Cells["A" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells["A" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                        sheet.Cells["B" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells["B" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                        sheet.Cells["C" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells["C" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                        sheet.Cells["D" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells["D" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                        index++;
                    }

                    bytes = templateXls.GetAsByteArray();
                }

                return bytes;
            }
            catch(Exception ex)
            {
                if (ex.InnerException != null)
                {
                    LogHelper.Error(DateTime.Now.ToString() +
                        " Export Excel - " + ex.InnerException.StackTrace, ex);
                }

                LogHelper.Error("Method: Export Excel - " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }

        }

        public byte[] ExportLearnerLate(List<LearnerDto> learnerList, SearchCriterialDto criterial, DateTime classDate)
        {
            Byte[] bytes = null;
            LearnerDto learner = learnerList[0];
            ExportHelper exportHelper = new ExportHelper();
            string fileSource = exportHelper.GetLearnerLateTeamplex(FileExtenstion.EXCEL);


            using (var templateXls = new ExcelPackage(new FileInfo(fileSource)))
            {
                var sheet = templateXls.Workbook.Worksheets["Learner"];
                #region Header
                sheet.Cells["A2"].Value = "Generated on: " + DateHelper.FormatDateTime(DateTime.Now);
                string headerRemark = "(Learners for classes on " + DateHelper.FormatDate(classDate) + " with time in after " + DateHelper.FormatTime(classDate) + " hrs and absent)";
                sheet.Cells["A3"].Value = headerRemark;

                #endregion Header

                #region Learner Body
                int index = 6;
                Color rowbg;

                foreach (var item in learnerList)
                {
                    sheet.Cells["A" + index].Value = item.RowNum;
                    sheet.Cells["B" + index].Value = item.LearnerName;
                    sheet.Cells["C" + index].Value = item.StaffID;
                    sheet.Cells["D" + index].Value = item.Class.CourseName;
                    sheet.Cells["E" + index].Value = item.Class.CourseId;
                    sheet.Cells["F" + index].Value = item.Class.ClassID;
                    sheet.Cells["G" + index].Value = DateHelper.FormatDateMonthInName(item.Class.ClassDate);
                    sheet.Cells["H" + index].Value = DateHelper.FormatTime(item.CheckInTime);
                    sheet.Cells["I" + index].Value = item.Remark;

                    //Stype
                    sheet.Row(index).Height = 20;
                    rowbg = (index % 2 == 1) ? Color.FromArgb(255, 255, 255) : Color.FromArgb(208, 233, 247);
                    sheet.Cells["B" + index].Style.WrapText = true;
                    sheet.Cells["D" + index].Style.WrapText = true;
                    sheet.Cells["I" + index].Style.WrapText = true;
                    sheet.Cells["A" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["A" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["B" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["B" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["C" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["C" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["D" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["D" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["E" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["E" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["F" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["F" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["G" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["G" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["H" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["H" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["I" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["I" + index].Style.Fill.BackgroundColor.SetColor(rowbg);

                    //var matches = Regex.Matches(item.Remark, "\r\n");
                    //int default_row_height = 20;


                    //if (matches.Count > 0)
                    //{
                    //    sheet.Row(index).Height = default_row_height + (15* (matches.Count + 1));
                    //}
                    if ((item.Class.CourseName.Length > 160) || (item.LearnerName.Length > 200) || (item.Remark.Length > 90))
                    {
                        sheet.Row(index).Height = 90;
                    }
                    else if ((item.Class.CourseName.Length > 128) || (item.LearnerName.Length > 160) || (item.Remark.Length > 72))
                    {
                        sheet.Row(index).Height = 75;
                    }
                    else if ((item.Class.CourseName.Length > 96) || (item.LearnerName.Length > 120) || (item.Remark.Length > 54))
                    {
                        sheet.Row(index).Height = 60;
                    }
                    else if ((item.Class.CourseName.Length > 64) || (item.LearnerName.Length > 80) || (item.Remark.Length > 36))
                    {
                        sheet.Row(index).Height = 45;
                    }
                    else if ((item.Class.CourseName.Length > 32) || (item.LearnerName.Length > 40) || (item.Remark.Length > 18))
                    {
                        sheet.Row(index).Height = 30;
                    }
                    else
                    {
                        sheet.Row(index).Height = 20;
                    }

                    index++;
                }
                #endregion Learner Body

                bytes = templateXls.GetAsByteArray();
            }

            return bytes;
        }
        public byte[] ExportLearnerHistory(List<LearnerDto> learnerList, SearchCriterialDto criterial)
        {
            Byte[] bytes = null;

            LearnerDto learner = learnerList[0];

            ExportHelper exportHelper = new ExportHelper();
            string fileSource = exportHelper.GetLearnerHistoryTeamplex(FileExtenstion.EXCEL);

            DateTime? startDate;
            DateTime? endDate;

            var startDateObj = criterial.SearchFieldList.FirstOrDefault(x => x.Key.ToString().ToLower() == "startdate");
            if (startDateObj != null)
            {
                startDate = (DateTime)startDateObj.Value;
            }
            else
            {
                startDate = (from d in learnerList select d.Class.ClassDate).Min();
            }

            var endDateObj = criterial.SearchFieldList.FirstOrDefault(x => x.Key.ToString().ToLower() == "enddate");
            if (endDateObj != null)
            {
                endDate = (DateTime)endDateObj.Value;
            }
            else
            {
                endDate = (from d in learnerList select d.Class.ClassDate).Max();
            }

            using (var templateXls = new ExcelPackage(new FileInfo(fileSource)))
            {
                var sheet = templateXls.Workbook.Worksheets["Learner"];
                bool isHasCabinCrewClass = false;
                var carbinCrewClass = learnerList.FirstOrDefault(x => !string.IsNullOrEmpty(x.Class.CourseId) && x.Class.CourseId.StartsWith("CCT"));
                if (carbinCrewClass != null)
                {
                    isHasCabinCrewClass = true;
                }


                #region Header
                sheet.Cells["A6"].Value = "Learner's Name : " + learner.LearnerName;
                sheet.Cells["E6"].Value = "Staff ID : " + learner.StaffID;
                sheet.Cells["A7"].Value = "Start Date : " + DateHelper.FormatDateMonthInName(startDate);
                sheet.Cells["E7"].Value = "End Date : " + DateHelper.FormatDateMonthInName(endDate);

                if (isHasCabinCrewClass==true)
                {
                    sheet.Cells["A8:D8"].Merge = true;
                    sheet.Cells["A8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    sheet.Cells["A8"].Style.Font.Bold = true;
                    sheet.Cells["A8"].Style.Font.Size = 14;
                    sheet.Cells["A8"].Value = "Daily Training starts @ 0845 hrs & ends @ 1715 hrs";

                }
                else
                {
                    sheet.Row(8).Height = 0;
                    sheet.Row(8).Hidden = true;
                }
                #endregion Header

                #region Learner Body
                int index = 11;
                Color rowbg;
                //sheet.Row(11).Height = 20;
                string ackSymbol = ((char)0x221A).ToString();

                foreach (var item in learnerList)
                {

                    sheet.Cells["A" + index].Value = DateHelper.FormatDateMonthInName(item.Class.ClassDate);
                    sheet.Cells["B" + index].Value = item.Class.CourseName;
                    sheet.Cells["C" + index].Value = DateHelper.FormatTime(item.CheckInTime);
                    sheet.Cells["D" + index].Value = item.Class.Trainers;

                    if(item.Class.ClassStatusID == ClassStatus.Completed.GetHashCode())
                    {
                        sheet.Cells["E" + index].Value = ackSymbol;
                    }

                    //Stype
                    rowbg = (index % 2 == 1) ? Color.FromArgb(255, 255, 255) : Color.FromArgb(208, 233, 247);
                    sheet.Cells["A" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["A" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["B" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["B" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["C" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["C" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["B" + index].Style.WrapText = true;
                    sheet.Cells["D" + index].Style.WrapText = true;
                    sheet.Row(index).Height = 20;

                    if ((item.Class.CourseName.Length > 150) || (item.Class.Trainers.Length > 130))
                    {
                        sheet.Row(index).Height = 90;
                    }
                    else if ((item.Class.CourseName.Length > 120) || (item.Class.Trainers.Length > 104))
                    {
                        sheet.Row(index).Height = 75;
                    }
                    else if ((item.Class.CourseName.Length > 90) || (item.Class.Trainers.Length > 78))
                    {
                        sheet.Row(index).Height = 60;
                    }
                    else if ((item.Class.CourseName.Length > 60) || (item.Class.Trainers.Length > 52))
                    {
                        sheet.Row(index).Height = 45;
                    }
                    else if ((item.Class.CourseName.Length > 30) || (item.Class.Trainers.Length > 26))
                    {
                        sheet.Row(index).Height = 30;
                    }
                    else
                    {
                        sheet.Row(index).Height = 20;
                    }

                    sheet.Cells["D" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["D" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["E" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["E" + index].Style.Fill.BackgroundColor.SetColor(rowbg);

                    index++;
                }
                #endregion Learner Body

                bytes = templateXls.GetAsByteArray();
            }

            return bytes;
        }

        public byte[] ExportLearner(LearnerPresentViewModel vm)
        {
            Byte[] bytes;
            ClassDto oClass = vm.Class;
            LearnerDto oLearner = vm.Learner;

            ExportHelper exportHelper = new ExportHelper();
            string fileSource = exportHelper.GetExportFileTeamplex(false, FileExtenstion.EXCEL);

            using (var templateXls = new ExcelPackage(new FileInfo(fileSource)))
            {
                var sheet = templateXls.Workbook.Worksheets["Course"];

                #region Header
                sheet.Cells["A2"].Value = "Generated on: " + DateHelper.FormatDateTime(DateTime.Now);
                sheet.Cells["A6"].Value = "Course Title : " + oClass.CourseName;
                sheet.Cells["A7"].Value = "Name : " + oLearner.LearnerName;
                sheet.Cells["A8"].Value = "Start Date : " + DateHelper.FormatDateMonthInName(oClass.StartDate);
                sheet.Cells["D6"].Value = "Class ID : " + oClass.ClassID;
                sheet.Cells["D7"].Value = "Staff ID : " + oLearner.StaffID;
                sheet.Cells["D8"].Value = "End Date : " + DateHelper.FormatDateMonthInName(oClass.EndDate);

                sheet.Cells["A6:C6"].Merge = true;
                sheet.Cells["A6"].Style.WrapText = true;

                if (oClass.CourseName.Length > 45)
                {
                    sheet.Row(6).Height = 34;
                }
                #endregion Header

                #region Learner Body
                int index = 12;
                Color rowbg;
                sheet.Row(11).Height = 20;
                ExcelBorderStyle cellstyle = ExcelBorderStyle.Thin;
                string presentSymbol = string.Empty;
                string absentSymbol = ((char)0x221A).ToString();

                foreach (var item in vm.LearnerClassDates)
                {
                    if (item.CheckInTime != null)
                    {
                        presentSymbol = ((char)0x221A).ToString();
                        absentSymbol = string.Empty;
                    }
                    else
                    {
                        presentSymbol = string.Empty;
                        absentSymbol = ((char)0x221A).ToString();
                    }

                    sheet.Cells["A" + index].Value = DateHelper.FormatDateMonthInName(item.ClassDate);
                    sheet.Cells["B" + index].Value = presentSymbol;
                    sheet.Cells["C" + index].Value = absentSymbol;
                    sheet.Cells["D" + index].Value = DateHelper.FormatTime(item.CheckInTime);
                    //Stype
                    rowbg = (item.RowNum % 2 == 1) ? Color.FromArgb(255, 255, 255) : Color.FromArgb(208, 233, 247);
                    sheet.Cells["A" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    //sheet.Cells["A" + index].Style.Border.Right.Style = cellstyle;
                    sheet.Cells["A" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["B" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["B" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["C" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["C" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["D" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["D" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                
                    sheet.Row(index).Height = 17;
                    index++;
                }
                #endregion Learner Body

                index += 2;
                //Trainer
                sheet.Cells["A" + index + ":D" + index].Style.Font.Bold = true;
                sheet.Cells["A" + index + ":D" + index].Style.Font.Color.SetColor(Color.White);
                sheet.Cells["A" + index].Value = "Sr.";
                sheet.Cells["B" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                sheet.Cells["B" + index].Value = "Instructor Name";
                sheet.Cells["C" + index].Value = "Acknowledgement";
                sheet.Cells["D" + index].Value = "Date Acknowledged";
                //sheet.Cells["D" + index + ":E" + index].Merge = true;

                //Style
                sheet.Row(index).Height = 20;
                rowbg = Color.FromArgb(79, 129, 189);
                sheet.Cells["A" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                sheet.Cells["A" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                sheet.Cells["B" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                sheet.Cells["B" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                sheet.Cells["C" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                sheet.Cells["C" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                sheet.Cells["D" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                sheet.Cells["D" + index].Style.Fill.BackgroundColor.SetColor(rowbg);

                index++;
                int value = int.Parse("252", System.Globalization.NumberStyles.HexNumber);
                string checkSymbol = string.Empty;
                int trainerRowNo = 0;

                foreach (var item in vm.Class.TrainerList)
                {
                    if (item.AcknowledgementDate != null)
                    {
                        checkSymbol = ((char)0x221A).ToString();
                    }

                    trainerRowNo++;
                    sheet.Cells["A" + index].Value = trainerRowNo;
                    sheet.Cells["B" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    sheet.Cells["B" + index].Value = item.TrainerName;
                    sheet.Cells["C" + index].Value = checkSymbol;//252
                    sheet.Cells["D" + index].Value = DateHelper.FormatDateMonthInName(item.AcknowledgementDate);

                    //Style
                    sheet.Row(index).Height = 17;
                    //sheet.Cells["D" + index + ":E" + index].Merge = true;
                    rowbg = (trainerRowNo % 2 == 1) ? Color.FromArgb(255, 255, 255) : Color.FromArgb(208, 233, 247);
                    sheet.Cells["A" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["A" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["B" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["B" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["C" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["C" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["D" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["D" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    index++;
                }

                foreach (var item in vm.Class.AcknowledgementExtraList)
                {
                    if (item.AcknowledgementDate != null)
                    {
                        checkSymbol = ((char)0x221A).ToString();
                    }

                    trainerRowNo++;
                    sheet.Cells["A" + index].Value = trainerRowNo;
                    sheet.Cells["B" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    sheet.Cells["B" + index].Value = item.TrainerName;
                    sheet.Cells["C" + index].Value = checkSymbol;//252
                    sheet.Cells["D" + index].Value = DateHelper.FormatDateMonthInName(item.AcknowledgementDate);

                    //Style
                    sheet.Row(index).Height = 17;
                    //sheet.Cells["D" + index + ":E" + index].Merge = true;
                    rowbg = (trainerRowNo % 2 == 1) ? Color.FromArgb(255, 255, 255) : Color.FromArgb(208, 233, 247);
                    sheet.Cells["A" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["A" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["B" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["B" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["C" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["C" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    sheet.Cells["D" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells["D" + index].Style.Fill.BackgroundColor.SetColor(rowbg);
                    index++;
                }

                bytes = templateXls.GetAsByteArray();
            }

            return bytes;

        }
    }
}