﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.pipeline;
using AAT.Model;
using AAT.Model.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using AAT.Model.ViewModel;

namespace AAT.Utils
{
    public class PdfLearnerHeaderFooter : PdfPageEventHelper
    {
        private LearnerPresentViewModel viewModel;
        public PdfLearnerHeaderFooter(string html, LearnerPresentViewModel vm)
        {
            this.html = html;
            viewModel = vm;
        }


        private void AddText(string text, PdfContentByte cb, Font font, float left, float right)
        {
            Phrase myText = new Phrase(text, font);
            ColumnText ct = new ColumnText(cb);
            ct.SetSimpleColumn(myText, left, right, 558, 317, 36, Element.ALIGN_LEFT);
            ct.Go();
        }

        private void AddLine(float height, Document document, PdfContentByte cb, BaseColor baseColor, float y)
        {
            cb.SetLineWidth(height);   // Make a bit thicker than 1.0 default
            //cb.SetGrayStroke(0.95f); // 1 = black, 0 = white
            cb.SetColorStroke(baseColor);
            cb.MoveTo(document.LeftMargin, y);
            cb.LineTo(document.PageSize.Width - document.RightMargin, y);
            cb.Stroke();
        }

        public override void OnEndPage(PdfWriter writer, Document document)
        {
            base.OnEndPage(writer, document);

            ClassDto oClass = viewModel.Class;
            LearnerDto oLearner = viewModel.Learner;
            var GreenColour = new BaseColor(54, 95, 145);
            var BrownColour = new BaseColor(98, 36, 35);

            float Position_Y = 830;
            float Col1_X = document.LeftMargin;
            float Col2_X = 385;
            string text = "";

            //HEADER
            PdfContentByte cb = writer.DirectContent;
            Font font = new Font(Font.FontFamily.TIMES_ROMAN, 16f, Font.BOLD , GreenColour);
            AddText("LEARNER REPORT", cb, font, Col1_X, Position_Y);

            font.Size = 13;
            Position_Y -= 20;
            AddText("Generated on:" + DateHelper.FormatDateTime(DateTime.Now), cb, font, Col1_X, Position_Y);

            //LOGO
            string dir = HttpContext.Current.Server.MapPath("~");
            string path = dir.ToString() + "\\Images\\logo.png";
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(path);
            image.SetAbsolutePosition(document.RightMargin, 25);
            image.ScaleAbsolute(175, 60); //Image Width, Height
            image.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;
            PdfTemplate tp = cb.CreateTemplate(273, 95);
            tp.AddImage(image);
            cb.AddTemplate(tp, 350, 842 - 95); //SET Image Location

            float line_y = 765f;
            AddLine(5.0f, document, cb, GreenColour, line_y);

            Position_Y -= 35;
            font.Color = BaseColor.BLACK;
            AddText("Course : " + oClass.CourseName, cb, font, Col1_X, Position_Y);
            AddText("Class ID : " + oClass.ClassID, cb, font, Col2_X, Position_Y);

            line_y -= 35;
            AddLine(1.0f, document, cb, BaseColor.BLACK, line_y);
            //-----------------------------

            Position_Y -= 25;
            font.Color = BaseColor.BLACK;
            text = "Name : " + oLearner.LearnerName;
            AddText(text, cb, font, Col1_X, Position_Y);

            text = "Staff ID  : " + oLearner.StaffID;
            AddText(text, cb, font, Col2_X, Position_Y);
            line_y -= 25;
            AddLine(1.0f, document, cb, BaseColor.BLACK, line_y);
            //-----------------------------

            Position_Y -= 25;
            font.Color = BaseColor.BLACK;
            text = "Start Date : " + DateHelper.FormatDateMonthInName(oClass.StartDate);
            AddText(text, cb, font, Col1_X, Position_Y);

            text = "End Date : " + DateHelper.FormatDateMonthInName(oClass.EndDate);
            AddText(text, cb, font, Col2_X, Position_Y);
            line_y -= 25;
            AddLine(1.0f, document, cb, BaseColor.BLACK, line_y);

            //Position_Y -= 25;
            //font.Color = BaseColor.BLACK;
            //text = "Learners Unsuccessful : " + mClassReport.TotalUnsuccess;
            //AddText(text, cb, font, Col1_X, Position_Y);

            //text = "End Date : " + DateHelper.FormatDateMonthInName(oClass.EndDate);
            //AddText(text, cb, font, Col2_X, Position_Y);

            //line_y -= 25;
            //AddLine(1.0f, document, cb, BaseColor.BLACK, line_y);
            //ct.SetSimpleColumn(document.Left, document.Top, document.Right, document.GetTop(-20), 10, Element.ALIGN_MIDDLE);


            //Footer
            line_y = 65f;
            AddLine(3.0f, document, cb, BrownColour, line_y);
            font.Color = GreenColour;
            Position_Y = 31;
            //AddText("This is computer generated report, no signature required.", cb, font, Col1_X, Position_Y);

            text = "Page " + document.PageNumber;// + " of " + document.PageSize;
            //AddText(text, cb, font, 520, Position_Y);


            Phrase myText = new Phrase("This is computer generated report, no signature required.", font);
            ColumnText ct = new ColumnText(cb);
            ct.SetSimpleColumn(myText, 36, Position_Y, 500, 88, 36, Element.ALIGN_LEFT);
            ct.Go();

            myText = new Phrase("Page " + document.PageNumber, font);
            ct = new ColumnText(cb);
            ct.SetSimpleColumn(myText, 520, Position_Y, 560, 88, 36, Element.ALIGN_LEFT);
            ct.Go();
        }

        string html = null;
    }

   
}