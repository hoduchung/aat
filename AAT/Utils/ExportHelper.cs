﻿using AAT.Model;
using AAT.Model.Utils;
using AAT.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace AAT.Utils
{
    public class ExportHelper
    {

        public static string GetClassExportFilename(string classID, FileExtenstion fileExtension)
        {
            string re = "AAT Class " + classID;
            switch (fileExtension)
            {
                case FileExtenstion.PDF:
                    return re + ".pdf";
                case FileExtenstion.WORD:
                    return re + ".doc";
                case FileExtenstion.EXCEL:
                    return re + ".xlsx";
            }
            return re;
        }

        public string GetLearnerLateTeamplex(FileExtenstion fileExtension)
        {
            string dir = HttpContext.Current.Server.MapPath("~");
            string fileSource = string.Empty;

            switch (fileExtension)
            {
                case FileExtenstion.PDF:
                    fileSource = dir.ToString() + ConfigHelper.PdfLearnerLateTemplex;
                    break;
                case FileExtenstion.WORD:
                    fileSource = dir.ToString() + ConfigHelper.DocLearnerLateTemplex;
                    break;
                case FileExtenstion.EXCEL:
                    fileSource = dir.ToString() + ConfigHelper.ExcelLearnerLateTemplex;
                    break;
            }

            return fileSource;
        }

        public string GetLearnerHistoryTeamplex(FileExtenstion fileExtension)
        {
            string dir = HttpContext.Current.Server.MapPath("~");
            string fileSource = string.Empty;

            switch (fileExtension)
            {
                case FileExtenstion.PDF:
                    fileSource = dir.ToString() +  ConfigHelper.PdfLearnerHistoryTemplex;
                    break;
                case FileExtenstion.WORD:
                    fileSource = dir.ToString() + ConfigHelper.DocLearnerHistoryTemplex;
                    break;
                case FileExtenstion.EXCEL:
                    fileSource = dir.ToString() + ConfigHelper.ExcelLearnerHistoryTemplex;
                    break;
            }

            return fileSource;
        }

        public string GetExportFileTeamplex(bool IsClass, FileExtenstion fileExtension)
        {
            string dir = HttpContext.Current.Server.MapPath("~");
            string fileSource = string.Empty;

            if (IsClass)
            {
                switch (fileExtension)
                {
                    case FileExtenstion.PDF:
                        fileSource = dir.ToString() + "\\" + ConfigHelper.ClassPdfTemplateURL;
                        break;
                    case FileExtenstion.WORD:
                        fileSource = dir.ToString() + "\\" + ConfigHelper.ClassDocTemplateURL;
                        break;
                    case FileExtenstion.EXCEL:
                        fileSource = dir.ToString() + "\\" + ConfigHelper.ExcelClassTemplateURL;
                        break;
                }
            }
            else
            {
                switch (fileExtension)
                {
                    case FileExtenstion.PDF:
                        fileSource = dir.ToString() + "\\" + ConfigHelper.LearnerPdfTemplateURL;
                        break;
                    case FileExtenstion.WORD:
                        fileSource = dir.ToString() + "\\" + ConfigHelper.LearnerDocTemplateURL;
                        break;
                    case FileExtenstion.EXCEL:
                        fileSource = dir.ToString() + "\\" + ConfigHelper.ExcelLearnerTemplateURL;
                        break;
                }
            }
            return fileSource;
        }

        public string GetClassHTMLContent(ClassReportDto classReport, FileExtenstion fileExtension)
        {
            string fileSource = GetExportFileTeamplex(true, fileExtension);
            //S1: Get templex
            string html_string = File.ReadAllText(fileSource);
            //S2: Replace Content
            StringBuilder tableContentData = new StringBuilder();

            int index = 0;
            string rowbg;
            string Previousrow_ClassDate;
            for (int i=0; i< classReport.LearnerClassDateList.Count; i++)
            {
                var item = classReport.LearnerClassDateList;
                string tableHeaderRowTemplate ="";
                
                if (i != 0)
                {
                    
                    Previousrow_ClassDate = DateHelper.FormatDateMonthInName(item[i-1].ClassDate);
                    if(Previousrow_ClassDate != DateHelper.FormatDateMonthInName(item[i].ClassDate))
                    {
                        tableHeaderRowTemplate =
                        "<tr class='headder-row background-header'>" +
                        "<td class=center style = 'width:7%;display:inline; word-break:keep-all'> Sr.</ td >" +
                        "<td style='width:48%;margin:auto;'>Name</td>" +
                        "<td class=center style = 'width:14%;' > Staff ID</td>" +
                        "<td class=center style = 'width:18%' > Scan Date</td>" +
                        "<td class=center style = 'width:13%; max-width:75px;' > Time In</td>" +
                        "</tr>";

                        tableContentData.Append(tableHeaderRowTemplate);
                        index = 0;
                    }
                }
                

                rowbg = (index % 2 == 0) ? "row-bg" : "row-alter-bg";
                string tableRowTemplate = "<tr class=" + rowbg + @" ><td class=""center"">" + (index+1) + "</td>"
                    + "<td>" + item[i].LearnerName + @"</td><td class=""center"">" + item[i].StaffId + "</td>"
                    + @"<td class=""center"">" + DateHelper.FormatDateMonthInName(item[i].ClassDate) + @"</td>"
                    + @"<td class=""center"">" + DateHelper.FormatTime(item[i].CheckInTime) + "</td>"
                    + "</tr>";
                tableContentData.Append(tableRowTemplate);
                index++;
            }

            //foreach (var item in classReport.LearnerClassDateList)
            //{

            //    rowbg = (index % 2 == 0) ? "row-bg" : "row-alter-bg";
            //    string tableRowTemplate = "<tr class=" + rowbg + @" ><td class=""center"">" + item.RowNum + "</td>"
            //        + "<td>" + item.LearnerName + @"</td><td class=""center"">" + item.StaffId + "</td>"
            //        + @"<td class=""center"">" + DateHelper.FormatDateMonthInName(item.ClassDate) + @"</td>"
            //        + @"<td class=""center"">" + DateHelper.FormatTime(item.CheckInTime) + "</td>"
            //        + "</tr>";
            //    tableContentData.Append(tableRowTemplate);
            //    index++;
            //}
            html_string = html_string.Replace("{tablecontent}", tableContentData.ToString());


            //{CabinCrewTrainingHours}
            if (classReport.Class.CourseId.StartsWith("CCT"))
            {
                string html_cabincrew = "<tr class='header-title-row vertical-up-align'><td colspan='2'>Daily Training starts @ 0845 hrs & ends @ 1715 hrs</td></tr>";
                html_string = html_string.Replace("{CabinCrewTrainingHours}",html_cabincrew);
            }
            else
            {
                html_string = html_string.Replace("{CabinCrewTrainingHours}", "");
            }

            //S3: Trainner 
            tableContentData = new StringBuilder();
            index = 0;
            string checkSymbol = string.Empty;

            int rowNum = 0;
            foreach (var item in classReport.Class.TrainerList)
            {
                if (item.AcknowledgementDate != null)
                {
                    checkSymbol = ((char)0x221A).ToString();
                }
                rowNum = index + 1;
                rowbg = (index % 2 == 0) ? "row-bg" : "row-alter-bg";
                string tableRowTemplate = "<tr class=" + rowbg + @" ><td class=""center"">" + rowNum.ToString() + "</td>"
                    + "<td>" + item.TrainerName + "</td>"
                    + @"<td class=""center"">" + checkSymbol + "</td>"
                    + @"<td class=""center"">" + DateHelper.FormatDateMonthInName(item.AcknowledgementDate) + @"</td>"
                    + "</tr>";
                tableContentData.Append(tableRowTemplate);
                index++;
            }

            //Extra Trainer
            foreach (var item in classReport.Class.AcknowledgementExtraList)
            {
                if (item.AcknowledgementDate != null)
                {
                    checkSymbol = ((char)0x221A).ToString();
                }
                rowNum = index + 1;
                rowbg = (index % 2 == 0) ? "row-bg" : "row-alter-bg";
                string tableRowTemplate = "<tr class=" + rowbg + @" ><td class=""center"">" + rowNum.ToString() + "</td>"
                    + "<td>" + item.TrainerName + "</td>"
                    + @"<td class=""center"">" + checkSymbol + "</td>"
                    + @"<td class=""center"">" + DateHelper.FormatDateMonthInName(item.AcknowledgementDate) + @"</td>"
                    + "</tr>";
                tableContentData.Append(tableRowTemplate);
                index++;
            }
            html_string = html_string.Replace("{tabletrainer}", tableContentData.ToString());

            //if (fileExtension == FileExtenstion.WORD)
            //{
            ClassDto oClass = classReport.Class;
            html_string = html_string.Replace("{GeneratedOn}", DateHelper.FormatDateTime(DateTime.Now));
            html_string = html_string.Replace("{CourseTitle}", oClass.CourseName);
            html_string = html_string.Replace("{ClassID}", oClass.ClassID);
            html_string = html_string.Replace("{TotalRegistered}", classReport.TotalRegister.ToString());
            html_string = html_string.Replace("{CourseDuration}", classReport.Duration.ToString() + " day(s)");
            html_string = html_string.Replace("{TotalSuccess}", classReport.TotalSuccess.ToString());
            html_string = html_string.Replace("{StartDate}", DateHelper.FormatDateMonthInName(oClass.StartDate));
            html_string = html_string.Replace("{TotalUnsuccess}", classReport.TotalUnsuccess.ToString());
            html_string = html_string.Replace("{EndDate}", DateHelper.FormatDateMonthInName(oClass.EndDate));
            //}
            return html_string;
        }

        public string GetLearnerHistoryHTML(List<LearnerDto> learnerList, SearchCriterialDto criterial, FileExtenstion fileExtension)
        {

            string fileSource = GetLearnerHistoryTeamplex(fileExtension);
            //S1: Get templex
            string html_string = File.ReadAllText(fileSource);

            //S2: Replace Header
            LearnerDto learner = learnerList[0];
            DateTime? startDate;
            DateTime? endDate;
            bool isHasCabinCrewClass = false;
            var carbinCrewClass = learnerList.FirstOrDefault(x => !string.IsNullOrEmpty( x.Class.CourseId) &&  x.Class.CourseId.StartsWith("CCT"));
            if(carbinCrewClass != null)
            {
                isHasCabinCrewClass = true;
            }
            var startDateObj = criterial.SearchFieldList.FirstOrDefault(x => x.Key.ToString().ToLower() == "startdate");
            if (startDateObj != null)
            {
                startDate = (DateTime)startDateObj.Value;
            }
            else
            {
                startDate = (from d in learnerList select d.Class.ClassDate).Min();
            }

            var endDateObj = criterial.SearchFieldList.FirstOrDefault(x => x.Key.ToString().ToLower() == "enddate");
            if (endDateObj != null)
            {
                endDate = (DateTime)endDateObj.Value;
            }
            else
            {
                endDate = (from d in learnerList select d.Class.ClassDate).Max();
            }

            html_string = html_string.Replace("{GeneratedOn}", DateHelper.FormatDateTime(DateTime.Now));
            html_string = html_string.Replace("{LearnerName}", learner.LearnerName);
            html_string = html_string.Replace("{StaffID}", learner.StaffID);

            html_string = html_string.Replace("{StartDate}", DateHelper.FormatDateMonthInName(startDate));
            html_string = html_string.Replace("{EndDate}", DateHelper.FormatDateMonthInName(endDate));

            //{CabinCrewTrainingHours}
            if (isHasCabinCrewClass==true)
            {
                string html_cabincrew = "<tr class='header-title-row vertical-up-align'><td colspan='2'>Daily Training starts @ 0845 hrs & ends @ 1715 hrs</td></tr>";
                html_string = html_string.Replace("{CabinCrewTrainingHours}", html_cabincrew);
            }
            else
            {
                html_string = html_string.Replace("{CabinCrewTrainingHours}", "");
            }


            //S3: Replace Content
            StringBuilder tableContentData = new StringBuilder();

            int index = 0;
            string rowbg;
            string ackSymbol = string.Empty;

            foreach (var item in learnerList)
            {
                ackSymbol = string.Empty;
                if (item.Class.ClassStatusID == ClassStatus.Completed.GetHashCode())
                {
                    ackSymbol = ((char)0x221A).ToString();
                }

                rowbg = (index % 2 == 0) ? "row-bg" : "row-alter-bg";
                string tableRowTemplate = @"<tr class=""center " + rowbg + @""" ><td align='left' class='col_Date left'>" + @DateHelper.FormatDateMonthInName(item.Class.ClassDate) + "</td>"
                    + @"<td align='left' class='col_CourseTitle left'>" + item.Class.CourseName + @"</td><td align='left' class=""col_TimeIn left"">" + DateHelper.FormatTime(item.CheckInTime) + "</td>"
                    + @"<td align='left' class='col_InstructorName left'>" + item.Class.Trainers + @"</td>"
                    + @"<td class=""col_InstructorAcknowledgement center"">" + ackSymbol + @"</td>"         
                    + "</tr>";
                tableContentData.Append(tableRowTemplate);
                index++;
            }
            html_string = html_string.Replace("{tablecontent}", tableContentData.ToString());


            

            return html_string;
        }

        public string GetLearnerHTML(LearnerPresentViewModel vm, FileExtenstion fileExtension)
        {
            string fileSource = GetExportFileTeamplex(false, fileExtension);
            //S1: Get templex
            string html_string = File.ReadAllText(fileSource);
            //S2: Replace Content
            StringBuilder tableContentData = new StringBuilder();

            int index = 0;
            string rowbg;
            string presentSymbol = string.Empty;
            string absentSymbol = ((char)0x221A).ToString();

            foreach (var item in vm.LearnerClassDates)
            {
                if (item.CheckInTime != null)
                {
                    presentSymbol = ((char)0x221A).ToString();
                    absentSymbol = string.Empty;
                }
                else
                {
                    presentSymbol = string.Empty;
                    absentSymbol = ((char)0x221A).ToString();
                }

                rowbg = (index % 2 == 0) ? "row-bg" : "row-alter-bg";
                string tableRowTemplate = "<tr class=" + rowbg + @" ><td>" + @DateHelper.FormatDateMonthInName(item.ClassDate) + "</td>"
                    + @"<td class=""center"">" + presentSymbol + @"</td><td class=""center"">" + absentSymbol + "</td>"
                    + @"<td class=""center"">" + @DateHelper.FormatTime(item.CheckInTime) + @"</td>"
                    + "</tr>";
                tableContentData.Append(tableRowTemplate);
                index++;
            }
            html_string = html_string.Replace("{tablecontent}", tableContentData.ToString());

            //S3: Trainner 
            tableContentData = new StringBuilder();
            index = 0;
            string checkSymbol = string.Empty;
            int trainerRowNo = 0;

            List<AcknowledgementExtraDto> ackledgementList = new List<AcknowledgementExtraDto>();
            foreach (var item in vm.Class.TrainerList)
            {
                ackledgementList.Add(new AcknowledgementExtraDto
                {
                    AcknowledgementDate = item.AcknowledgementDate,
                    TrainerName = item.TrainerName
                });
            }

            ackledgementList.AddRange(vm.Class.AcknowledgementExtraList);

            foreach (var item in ackledgementList)
            {
                trainerRowNo++;
                if (item.AcknowledgementDate != null)
                {
                    checkSymbol = ((char)0x221A).ToString();
                }

                rowbg = (trainerRowNo % 2 == 0) ? "row-bg" : "row-alter-bg";
                string tableRowTemplate = "<tr class=" + rowbg + @" ><td style=""padding-left:5px"" class=""left"">" + trainerRowNo + "</td>"
                    + @"<td style=""padding-left:5px"" class=""left"">" + item.TrainerName + "</td>"
                    + @"<td class=""center"">" + checkSymbol + "</td>"
                    + @"<td class=""center"">" + DateHelper.FormatDateMonthInName(item.AcknowledgementDate) + @"</td>"
                    + "</tr>";
                tableContentData.Append(tableRowTemplate);
                index++;
            }
            html_string = html_string.Replace("{tabletrainer}", tableContentData.ToString());

            //if (fileExtension == FileExtenstion.WORD)
            //{
            ClassDto oClass = vm.Class;
            html_string = html_string.Replace("{GeneratedOn}", DateHelper.FormatDateTime(DateTime.Now));
            html_string = html_string.Replace("{CourseTitle}", oClass.CourseName);
            html_string = html_string.Replace("{LearnerName}", vm.Learner.LearnerName);
            html_string = html_string.Replace("{StaffID}", vm.Learner.StaffID);

            html_string = html_string.Replace("{ClassID}", oClass.ClassID);
            //html_string = html_string.Replace("{CourseDuration}", classReport.Duration.ToString() + " days");
            html_string = html_string.Replace("{StartDate}", DateHelper.FormatDateMonthInName(oClass.StartDate));
            html_string = html_string.Replace("{EndDate}", DateHelper.FormatDateMonthInName(oClass.EndDate));
            //}
            return html_string;
        }

        public string GetLearnerLateHTML(List<LearnerDto> learnerList, SearchCriterialDto criterial, FileExtenstion fileExtension,DateTime classDate)
        {
            string fileSource = GetLearnerLateTeamplex(fileExtension);
            //S1: Get templex
            string html_string = File.ReadAllText(fileSource);

            //S2: Replace Header

            html_string = html_string.Replace("{GeneratedOn}", DateHelper.FormatDateTime(DateTime.Now));
            html_string = html_string.Replace("{ClassDate}", DateHelper.FormatDate(classDate));
            html_string = html_string.Replace("{Time In}", DateHelper.FormatTime(classDate));

            //S3: Replace Content
            StringBuilder tableContentData = new StringBuilder();

            int index = 0;
            string rowbg;

            foreach (var item in learnerList)
            {

                rowbg = (index % 2 == 0) ? "row-bg" : "row-alter-bg";
                string tableRowTemplate = @"<tr class=" + rowbg + @" ><td align='center' class='col_Sr'>" + item.RowNum + "</td>"
                    + @"<td align='left' class='col_Name'>" + item.LearnerName + @"</td><td align='center' class='col_StaffID'>" + item.StaffID + "</td>"
                    + @"<td align='left' class='col_CourseTitle'>" + item.Class.CourseName + @"</td><td align='center' class='col_CourseId'>" + item.Class.CourseId + "</td>"
                    + @"<td align='center' class='col_ClassID'>" + item.Class.ClassID + "</td>"
                    + @"<td align='center' class='col_ScanDate'>" + DateHelper.FormatDateMonthInName(item.CheckInTime) + @"</td><td align='center' class='col_TimeIn'>" + DateHelper.FormatTime(item.CheckInTime) + "</td>"
                    + @"<td align='left' class='col_Remarks'>" + item.Remark + "</td>"
                    + "</tr>";
                tableContentData.Append(tableRowTemplate);
                index++;
            }
            html_string = html_string.Replace("{tablecontent}", tableContentData.ToString());
            return html_string;
        }
    }
}