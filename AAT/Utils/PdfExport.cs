﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using AAT.Model;
using AAT.Model.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using AAT.Model.ViewModel;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.pipeline.html;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.parser;

namespace AAT.Utils
{
    public class PdfExport
    {
        //public int m_TopMargin = 30;
        //public int m_BottomMargin = 30;
        public byte[] ClassExportPdf(ClassReportDto classReport)
        {
            Byte[] bytes;
            try
            {
                ExportHelper export = new ExportHelper();
                string html_string = export.GetClassHTMLContent(classReport, FileExtenstion.PDF);

                bytes = GetPDFBytes(html_string);
                //Create a byte array that will eventually hold our final PDF

                //Boilerplate iTextSharp setup here
                //Create a stream that we can write to, in this case a MemoryStream
                //using (var ms = new MemoryStream())
                //{

                //    //Create an iTextSharp Document which is an abstraction of a PDF but **NOT** a PDF
                //    using (var doc = new Document())
                //    {

                //        //Create a writer that's bound to our PDF abstraction and our stream
                //        using (var writer = PdfWriter.GetInstance(doc, ms))
                //        {

                //            doc.SetMargins(doc.LeftMargin, doc.RightMargin, doc.TopMargin, doc.BottomMargin);
                //            //string htmlHeader = GetClassHeaderHtml();
                //            //writer.PageEvent = new PdfClassHeaderFooter(htmlHeader, classReport);
                //            doc.Open();

                //            var tagProcessors = (DefaultTagProcessorFactory)Tags.GetHtmlTagProcessorFactory();
                //            tagProcessors.RemoveProcessor(HTML.Tag.IMG); // remove the default processor
                //            tagProcessors.AddProcessor(HTML.Tag.IMG, new CustomImageTagProcessor()); // use our new processor

                //            CssFilesImpl cssFiles = new CssFilesImpl();
                //            cssFiles.Add(XMLWorkerHelper.GetInstance().GetDefaultCSS());
                //            var cssResolver = new StyleAttrCSSResolver(cssFiles);
                //            cssResolver.AddCss(@"code { padding: 2px 4px; }", "utf-8", true);
                //            var charset = Encoding.UTF8;
                //            var hpc = new HtmlPipelineContext(new CssAppliersImpl(new XMLWorkerFontProvider()));
                //            hpc.SetAcceptUnknown(true).AutoBookmark(true).SetTagFactory(tagProcessors); // inject the tagProcessors
                //            var htmlPipeline = new HtmlPipeline(hpc, new PdfWriterPipeline(doc, writer));
                //            var pipeline = new CssResolverPipeline(cssResolver, htmlPipeline);
                //            var worker = new XMLWorker(pipeline, true);
                //            var xmlParser = new XMLParser(true, worker, charset);
                //            xmlParser.Parse(new StringReader(html_string));


                //            //Open the document for writing
                //            //using (var srHtml = new StringReader(html_string))
                //            //{
                //            //    // Parse the HTML
                //            //    iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, srHtml);
                //            //}

                //            ////Our sample HTML and CSS
                //            ////var example_html = @"<p>This <em>is </em><span class=""headline"" style=""text-decoration: underline;"">some</span> <strong>sample <em> text</em></strong><span style=""color: red;"">!!!</span></p>";
                //            //var example_css = @".headline{font-size:200%}";

                //            ////In order to read CSS as a string we need to switch to a different constructor
                //            ////that takes Streams instead of TextReaders.
                //            ////Below we convert the strings into UTF8 byte array and wrap those in MemoryStreams
                //            //using (var msCss = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(example_css)))
                //            //{
                //            //    using (var msHtml = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(html_string)))
                //            //    {
                //            //        //Parse the HTML
                //            //        iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, msHtml, msCss);
                //            //    }
                //            //}

                //            doc.Close();
                //        }
                //    }

                //    //After all of the PDF "stuff" above is done and closed but **before** we
                //    //close the MemoryStream, grab all of the active bytes from the stream
                //    bytes = ms.ToArray();
                //}

                return bytes;
                //Now we just need to do something with those bytes.
                //Here I'm writing them to disk but if you were in ASP.Net you might Response.BinaryWrite() them.
                //You could also write the bytes to a database in a varbinary() column (but please don't) or you
                //could pass them to another function for further PDF processing.
                //var testFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "test.pdf");
                //System.IO.File.WriteAllBytes(testFile, bytes);
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message, ex);
            }
            return null;
        }

        public byte[] GetPDFBytes(string html_string)
        {
            Byte[] bytes;

            try
            {
                using (var ms = new MemoryStream())
                {
                    //Create an iTextSharp Document which is an abstraction of a PDF but **NOT** a PDF
                    using (var doc = new Document())
                    {
                        //Create a writer that's bound to our PDF abstraction and our stream
                        using (var writer = PdfWriter.GetInstance(doc, ms))
                        {
                            doc.SetMargins(doc.LeftMargin, doc.RightMargin, doc.TopMargin, doc.BottomMargin);
                            PdfDestination pdfDest = new PdfDestination(PdfDestination.XYZ, 0, doc.PageSize.Height, 1f);
                            doc.Open();

                            var tagProcessors = (DefaultTagProcessorFactory)Tags.GetHtmlTagProcessorFactory();
                            tagProcessors.RemoveProcessor(HTML.Tag.IMG); // remove the default processor
                            tagProcessors.AddProcessor(HTML.Tag.IMG, new CustomImageTagProcessor()); // use our new processor
                            
                            CssFilesImpl cssFiles = new CssFilesImpl();
                            cssFiles.Add(XMLWorkerHelper.GetInstance().GetDefaultCSS());
                            var cssResolver = new StyleAttrCSSResolver(cssFiles);
                            cssResolver.AddCss(@"code { padding: 2px 4px; }", "utf-8", true);
                            var charset = Encoding.UTF8;
                            var hpc = new HtmlPipelineContext(new CssAppliersImpl(new XMLWorkerFontProvider()));
                            hpc.SetAcceptUnknown(true).AutoBookmark(true).SetTagFactory(tagProcessors); // inject the tagProcessors
                            var htmlPipeline = new HtmlPipeline(hpc, new PdfWriterPipeline(doc, writer));
                            var pipeline = new CssResolverPipeline(cssResolver, htmlPipeline);
                            var worker = new XMLWorker(pipeline, true);
                            var xmlParser = new XMLParser(true, worker, charset);
                            xmlParser.Parse(new StringReader(html_string));


                            //create a new action to send the document to our new destination.
                            PdfAction action = PdfAction.GotoLocalPage(1, pdfDest, writer);

                            //set the open action for our writer object
                            writer.SetOpenAction(action);

                            doc.Close();
                        }
                    }

                    //After all of the PDF "stuff" above is done and closed but **before** we
                    //close the MemoryStream, grab all of the active bytes from the stream
                    bytes = ms.ToArray();
                }

                return bytes;
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message, ex);
            }

            return null;
        }

        public byte[] GetPDFBytes_Landscape(string html_string)
        {
            Byte[] bytes;

            try
            {
                using (var ms = new MemoryStream())
                {
                    //Create an iTextSharp Document which is an abstraction of a PDF but **NOT** a PDF
                    using (var doc = new Document())
                    {
                        //Create a writer that's bound to our PDF abstraction and our stream
                        using (var writer = PdfWriter.GetInstance(doc, ms))
                        {
                            doc.SetMargins(doc.LeftMargin, doc.RightMargin, doc.TopMargin, doc.BottomMargin);
                            doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                            PdfDestination pdfDest = new PdfDestination(PdfDestination.XYZ, 0, doc.PageSize.Height, 1f);
                            doc.Open();

                            var tagProcessors = (DefaultTagProcessorFactory)Tags.GetHtmlTagProcessorFactory();
                            tagProcessors.RemoveProcessor(HTML.Tag.IMG); // remove the default processor
                            tagProcessors.AddProcessor(HTML.Tag.IMG, new CustomImageTagProcessor()); // use our new processor

                            CssFilesImpl cssFiles = new CssFilesImpl();
                            cssFiles.Add(XMLWorkerHelper.GetInstance().GetDefaultCSS());
                            var cssResolver = new StyleAttrCSSResolver(cssFiles);
                            cssResolver.AddCss(@"code { padding: 2px 4px; }", "utf-8", true);
                            var charset = Encoding.UTF8;
                            var hpc = new HtmlPipelineContext(new CssAppliersImpl(new XMLWorkerFontProvider()));
                            hpc.SetAcceptUnknown(true).AutoBookmark(true).SetTagFactory(tagProcessors); // inject the tagProcessors
                            var htmlPipeline = new HtmlPipeline(hpc, new PdfWriterPipeline(doc, writer));
                            var pipeline = new CssResolverPipeline(cssResolver, htmlPipeline);
                            var worker = new XMLWorker(pipeline, true);
                            var xmlParser = new XMLParser(true, worker, charset);
                            xmlParser.Parse(new StringReader(html_string));


                            //create a new action to send the document to our new destination.
                            PdfAction action = PdfAction.GotoLocalPage(1, pdfDest, writer);

                            //set the open action for our writer object
                            writer.SetOpenAction(action);

                            doc.Close();
                        }
                    }

                    //After all of the PDF "stuff" above is done and closed but **before** we
                    //close the MemoryStream, grab all of the active bytes from the stream
                    bytes = ms.ToArray();
                }

                return bytes;
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message, ex);
            }

            return null;
        }

        public byte[] LearnExportPdf(LearnerPresentViewModel vm)
        {
            Byte[] bytes;
            try
            {
                ExportHelper export = new ExportHelper();
                string html_string = export.GetLearnerHTML(vm, FileExtenstion.PDF);
                //Create a byte array that will eventually hold our final PDF

                bytes = GetPDFBytes(html_string);
                //using (var ms = new MemoryStream())
                //{
                //    //Create an iTextSharp Document which is an abstraction of a PDF but **NOT** a PDF
                //    using (var doc = new Document())
                //    {
                //        //Create a writer that's bound to our PDF abstraction and our stream
                //        using (var writer = PdfWriter.GetInstance(doc, ms))
                //        {
                //            doc.SetMargins(doc.LeftMargin, doc.RightMargin, doc.TopMargin, doc.BottomMargin);

                //            //string htmlHeader = GetClassHeaderHtml();
                //            //writer.PageEvent = new PdfLearnerHeaderFooter(htmlHeader, vm);

                //            doc.Open();


                //            var tagProcessors = (DefaultTagProcessorFactory)Tags.GetHtmlTagProcessorFactory();
                //            tagProcessors.RemoveProcessor(HTML.Tag.IMG); // remove the default processor
                //            tagProcessors.AddProcessor(HTML.Tag.IMG, new CustomImageTagProcessor()); // use our new processor

                //            CssFilesImpl cssFiles = new CssFilesImpl();
                //            cssFiles.Add(XMLWorkerHelper.GetInstance().GetDefaultCSS());
                //            var cssResolver = new StyleAttrCSSResolver(cssFiles);
                //            cssResolver.AddCss(@"code { padding: 2px 4px; }", "utf-8", true);
                //            var charset = Encoding.UTF8;
                //            var hpc = new HtmlPipelineContext(new CssAppliersImpl(new XMLWorkerFontProvider()));
                //            hpc.SetAcceptUnknown(true).AutoBookmark(true).SetTagFactory(tagProcessors); // inject the tagProcessors
                //            var htmlPipeline = new HtmlPipeline(hpc, new PdfWriterPipeline(doc, writer));
                //            var pipeline = new CssResolverPipeline(cssResolver, htmlPipeline);
                //            var worker = new XMLWorker(pipeline, true);
                //            var xmlParser = new XMLParser(true, worker, charset);
                //            xmlParser.Parse(new StringReader(html_string));

                //            //Open the document for writing
                //            //using (var srHtml = new StringReader(html_string))
                //            //{
                //            //    // Parse the HTML
                //            //    iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, srHtml);
                //            //}

                //            doc.Close();
                //        }
                //    }

                //    //After all of the PDF "stuff" above is done and closed but **before** we
                //    //close the MemoryStream, grab all of the active bytes from the stream
                //    bytes = ms.ToArray();
                //}

                return bytes;
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message, ex);
            }
            return null;
        }

        public string GetClassHeaderHtml()
        {
            //string dir = HttpContext.Current.Server.MapPath("~");
            //string logoPath = dir.ToString() + "\\Images\\logo.png";

            string htmlHeader = "<!DOCTYPE html><html><body>"
            //+ "<div style=\"position:relative; height:80px;\">" 
            //+  "<div style=\"float: left; font-size: 22pt; color: red; font-weight: bold; padding-top: 25px; \">" 
            //+ "  <span>ATTENDANCE REPORT </span> <br/>" 
            //+ " <span> " + DateHelper.FormatDateMonthInNameWithTime(DateTime.Now) + " </span>"
            //+ "</div>"
            //+ "<div style=\"height: 80px; float: right; \">"
            //+ "<img src=\"" + logoPath + "\" style=\"height:75px;padding-left:5px; \" />"
            //+ "</div> "
            + "</div></body></html>";

            //htmlHeader = htmlHeader.Replace("{ImageLogo}", logoPath);
            return htmlHeader;
        }
    }
}