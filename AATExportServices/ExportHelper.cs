﻿using AAT.Business;
using AAT.Model;
using AAT.Model.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AATExportServices
{
    public class ExportHelper
    {
        private void ExportProcessing()
        {
            List<LearnerClassDateDto> learnerClassDates = LearnerClassDateService.Instance.GetSkiesReport();
            if(learnerClassDates == null || learnerClassDates.Count == 0)
            {
                return;
            }

            LogService(DateTime.Now.ToString() + " Exported New Data");
            string skieExportPath = ConfigHelper.SkiesExportFolderPath;
            DirectoryInfo d = new DirectoryInfo(skieExportPath); //Assuming Test is your Folder
            string fileName = skieExportPath + "\\AAT_" + DateTime.Now.ToString("yyyy-MM-dd") + ".csv";
            bool isFileExisted = true;
            if (!File.Exists(fileName))
            {
                isFileExisted = false;
                File.Create(fileName).Dispose();
            }

            var csv = new StringBuilder();
            //Add Header to File
            if(!isFileExisted)
            {
                /*
                 Course ID
                Class ID
                Learner's Staff Name
                Learner's Staff No
                Scanned Date
                Scanned Time
                Successful / Not Successful (Y/N)
                Remarks
                 */
                string headerStr = "Course ID,Class ID,Learner's Staff Name,Learner's Staff No,Scanned Date," +
                    "Scanned Time,Successful / Not Successful (Y/N),Remarks";
                csv.AppendLine(headerStr);
            }

            string classDate = string.Empty;

            foreach (LearnerClassDateDto item in learnerClassDates)
            {
                classDate = string.Empty;
                if (item.CheckInTime != null)
                {
                    classDate = DateHelper.FormatSkiesDate(item.ClassDate);
                }

                var newLine = string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\"",
                    item.CourseId, item.ClassId, item.LearnerName, item.StaffId,
                    classDate, DateHelper.FormatSkiesTime(item.CheckInTime),
                    item.Success, item.Remark);

                csv.AppendLine(newLine);
            }
            //after your loop
            File.AppendAllText(fileName, csv.ToString());

            //Update data already export
            List<decimal> learnerDates = learnerClassDates.Select(x => x.LearnerClassDateId).ToList();

            List<string> classIds = learnerClassDates.Select(x => x.ClassId).Distinct().ToList();
            LearnerClassDateService.Instance.UpdateSkiesExported(learnerDates);
            ClassService.Instance.UpdateSkiesExported(classIds);

        }

        public void ExportSkiesData()
        {
            try
            {
                //Wait for finish prerious task
                if(ExportService.IsExportInProcessing)
                {
                    return;
                }
                LogService("Export Start: " +  DateTime.Now.ToString());
                ExportService.IsExportInProcessing = true;
                ExportProcessing();
                ExportService.IsExportInProcessing = false;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;

                if(ex.InnerException != null)
                {
                    msg += " DETAIL: " + ex.InnerException.InnerException.ToString();
                }
                LogService(DateTime.Now.ToString() + "ERRROR: " + msg );
            }

        }

        public static void LogError(string content)
        {
            try
            {
                content = DateTime.Now.ToString() + " " + content;
                string filePath = ConfigurationManager.AppSettings["ErrorFilePath"].ToString();
                string time = DateTime.Now.ToString("yyyyMMdd");
                filePath = filePath.Replace("{time}", time);

                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                sw.BaseStream.Seek(0, SeekOrigin.End);
                sw.WriteLine(content);
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {
            }
        }

        public static void LogService(string content)
        {

            try
            {
                string filePath = ConfigurationManager.AppSettings["LogFilePath"].ToString();

                string time = DateTime.Now.ToString("yyyyMM");
                filePath = filePath.Replace("{time}", time);

                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                sw.BaseStream.Seek(0, SeekOrigin.End);
                sw.WriteLine(content);
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {

            }
           
        }
    }
}
