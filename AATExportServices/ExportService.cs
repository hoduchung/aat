﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace AATExportServices
{
    public partial class ExportService : ServiceBase
    {
        public static bool IsExportInProcessing = false;
        System.Timers.Timer timeDelay;
        public ExportService()
        {
            InitializeComponent();
            timeDelay = new System.Timers.Timer();
            timeDelay.Interval = GetTimerInterval();
            timeDelay.Elapsed += new System.Timers.ElapsedEventHandler(WorkProcess);

            
        }

        public int GetTimerInterval()
        {
            try
            {
                int min = int.Parse(ConfigurationManager.AppSettings["IntervalMin"].ToString());
                return min * 60000;
            }
            catch (Exception ex)
            {

            }
            return 10 * 60000; //10 Minute;
        }

        public void WorkProcess(object sender, System.Timers.ElapsedEventArgs e)
        {
            ExportHelper export = new ExportHelper();
            export.ExportSkiesData();
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            ExportHelper.LogService("Service is Started");
            timeDelay.Enabled = true;

           // Call Services 1st
            ExportHelper export = new ExportHelper();
            export.ExportSkiesData();
        }


        protected override void OnStop()
        {
            ExportHelper.LogService("Service Stoped");
            timeDelay.Enabled = false;
        }
       
    }
}
