﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace AATExportServices
{
    static class Program
    {

        public static void Test()
        {
            ExportHelper export = new ExportHelper();
            export.ExportSkiesData();
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            //Test();
            //return;
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ExportService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
