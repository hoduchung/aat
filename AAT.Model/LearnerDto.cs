using System;
using System.Collections.Generic;
using System.Text;

namespace AAT.Model
{
    public class LearnerDto
    {
        public LearnerDto()
        {
            Class = new ClassDto();
        }

        public int LearnerId { get; set; }
        public string LearnerName { get; set; }
        public string StaffID { get; set; }
        public int RowNum { get; set; }
        public DateTime? CheckInTime { get; set; }
        public ClassDto Class { get; set; }
        public string Success { get; set; }
        public string SuccessRemark { get; set; }
        public string Remark { get; set; }
        public string UpdateBy { get; set; }
    }
}
