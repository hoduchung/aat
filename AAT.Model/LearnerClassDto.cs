using System;
using System.Collections.Generic;
using System.Text;

namespace AAT.Model
{
    public class LearnerClassDto
    {
        public string StaffId { get; set; }
        public string ClassId { get; set; }
        public string Success { get; set; }
        public string Remark { get; set; }
        public bool IsRemoved { get; set; }
        public string UpdateBy { get; set; }
        public int RowNum { get; set; }
    }
}
