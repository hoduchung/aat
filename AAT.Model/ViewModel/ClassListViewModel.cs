﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model.ViewModel
{
    public class ClassListViewModel
    {
        public ClassListViewModel()
        {
            ClassList = new List<ClassDto>();
        }
        public List<ClassDto> ClassList { get; set; }
        public PageViewModel PagingModel { get; set; }

        public string SearchDate { get; set; }
        public string SearchStartDate { get; set; }
        public string SearchEndtDate { get; set; }

        public string SearchCourseId { get; set; }
        public string SearchCourseName { get; set; }
        public string SearchClassId { get; set; }
        public string SearchClassName { get; set; }
        public string SearchTrainerId { get; set; }
        public string SearchTrainerName { get; set; }
        public string SearchStatus { get; set; }
        public string SortColumn { get; set; }
        public string SortOrder { get; set; }

        public int PageSize { get; set; }

        public List<ClassDto> GetDemoClass()
        {
            List<ClassDto> lst = new List<ClassDto>();

            for(int i = 1000; i< 1005; i++)
            {
                string id = (i * 10).ToString();
                ClassDto dto= new ClassDto
                {
                    ClassID = (i * 10).ToString(),
                    ClassName = "Class " + id,
                    CourseId = i.ToString(),
                    CourseName = "Course " + id.ToString(),
                    StartDate = DateTime.Now.AddDays(i),
                    EndDate = DateTime.Now.AddDays(i + 5)
                };

                for(int j = i + 5; j < i + 8; j++ )
                {
                    TrainerDto trainer = new TrainerDto
                    {
                        TrainerId = j.ToString(),
                        TrainerName = "Trainer " + j
                    };
                    dto.TrainerList.Add(trainer);
                }

                lst.Add(dto);
            }

            return lst;
        }
    }

    
}
