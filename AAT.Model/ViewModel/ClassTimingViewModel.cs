﻿using AAT.Model.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model.ViewModel
{
    public class ClassTimingViewModel
    {
        public ClassTimingViewModel()
        {
            Class = new ClassDto();
            LearnerList = new List<LearnerDto>();
        }

        public string LearnerSort { get; set; }
        public string LearnerSoftDir { get; set; }
        public ClassDto Class { get; set; }
        public List<LearnerDto> LearnerList { get; set; }
        public List<ClassDateSummaryDto> LearnerClassDateSummaries { get; set; }
    }
}
