﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model.ViewModel
{
    public class DepartmentListViewModel
    {

        public DepartmentListViewModel()
        {
            DepartmentList = new List<DepartmentDto>();
        }

        public List<DepartmentDto> DepartmentList { get; set; }
        public PageViewModel PagingModel { get; set; }

        public string SearchName { get; set; }
        public string SearchId { get; set; }

        public int PageSize { get; set; }
    }
}
