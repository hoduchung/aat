﻿using AAT.Model.Utils;
using AAT_SIA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model.ViewModel
{
    public class ClassViewModel
    {
        public ClassViewModel()
        {
            LearnerPresent = new List<LearnerClassDateDto>();
            LearnerNotPresent = new List<LearnerDto>();
            LearnerChecking = new LearnerDto();
            ActiveCard = new CardDto();
        }

        public CardDto ActiveCard { get; set; }
        public ClassDto Class { get; set; }
        public DateTime ClassDate { get; set; }

        public string LearnerPresentSort { get; set; }
        public string LearnerPresentSortDir { get; set; }
        public string LearnerNotPresentSort { get; set; }
        public string LearnerNotPresentSortDir { get; set; }

        public List<LearnerClassDateDto> LearnerPresent { get; set; }
        public List<LearnerDto> LearnerNotPresent { get; set; }
        public LearnerDto LearnerChecking { get; set; }

        public ClassViewModel GetDemoClass()
        {
            Class = new ClassDto
            {
                ClassID = "1",
                ClassName = "Class 1",
                ClassStatusID = ClassStatus.OnGoing.GetHashCode(),
                CourseId = "1",
                CourseName = "Course 1",
                EndDate = DateTime.Now.AddDays(10),
                StartDate = DateTime.Now
            };

            List <TrainerDto> trainerList = new List<TrainerDto>();
            trainerList.Add(new TrainerDto { TrainerId = "1", TrainerName = "Brody Davidson" });
            trainerList.Add(new TrainerDto { TrainerId = "1", TrainerName = "Raiden Schmidt" });
            trainerList.Add(new TrainerDto { TrainerId = "1", TrainerName = "Hugo Lucero" });
            Class.TrainerList = trainerList;

            List<LearnerDto> AttendingList = new List<LearnerDto>();
            List<string> nameList = new List<string>
            {
                "Rachael Harris", // 1
                "Paul Edwards",
                "Jane Carter",
                "Raiden Schmidt",
                "Tobias King", //5
                "Scarlet Miranda",
                "Tristan Singleton", //7
                "Nikhil Boyer",
                "Riley Garcia",
                "Hugo Lucero", //10
                "Luna Becker",
                "Liam James",
                "Brody Davidson",
                "Lisa Ritter", 
                "Kyla Conley" //15
            };

            for(int i = 1; i <= 15; i++)
            {
                LearnerDto dto = new LearnerDto();
                dto.RowNum = i;
                dto.StaffID = "A" + (i * 100).ToString();
                dto.LearnerId = i;
                dto.LearnerName = nameList[i - 1];

                AttendingList.Add(dto);
            }

            Class.LearnerList = AttendingList;
            LearnerNotPresent = AttendingList;
            return this;
        }

        public bool IsAcknowledgementDate()
        {
            if(this.Class.EndDate != null && DateTime.Now.Date >= this.Class.EndDate.Value.Date )
            {
                return true;
            }

            return false;
        }
    }
}
