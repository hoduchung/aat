﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model.ViewModel
{
    public class PageViewModel
    {
        public IEnumerable<string> Items { get; set; }
        public Pager Pager { get; set; }

        public PageViewModel(int page, int pageSize, int totalRecord, string callback)
        {
            //PageViewModel re = new PageViewModel();

            var pageItems = Enumerable.Range(1, totalRecord).Select(x => "Page " + x);
            var pager = new Pager(pageItems.Count(), page, pageSize);

            pager.Callback = callback;

            Items = pageItems.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize);
            Pager = pager;

            //var viewModel = new PageViewModel
            //{
            //    Items = pageItems.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize),
            //    Pager = pager
            //};

            //return re;
        }
    }

    public class Pager
    {
        public Pager(int totalItems, int? page, int pageSize)
        {
            // calculate total, start and end pages
            var totalPages = (int)Math.Ceiling((decimal)totalItems / (decimal)pageSize);
            var currentPage = page != null ? (int)page : 1;
            var startPage = currentPage - 5;
            var endPage = currentPage + 4;
            if (startPage <= 0)
            {
                endPage -= (startPage - 1);
                startPage = 1;
            }
            if (endPage > totalPages)
            {
                endPage = totalPages;
                if (endPage > 10)
                {
                    startPage = endPage - 9;
                }
            }

            TotalItems = totalItems;
            CurrentPage = currentPage;
            PageSize = pageSize;
            TotalPages = totalPages;
            StartPage = startPage;
            EndPage = endPage;
        }

        public int TotalItems { get; private set; }
        public int CurrentPage { get; private set; }
        public int PageSize { get; private set; }
        public int TotalPages { get; private set; }
        public int StartPage { get; private set; }
        public int EndPage { get; private set; }
        public string Url { get; set; }
        public string Callback { get; set; }
    }
}
