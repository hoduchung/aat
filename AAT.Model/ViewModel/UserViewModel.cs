﻿using AAT.Model;
using AAT.Model.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model.ViewModel
{
    public class UserViewModel
    {
        public UserViewModel()
        {
            User = new UserDto();
            RoleList = new List<KeyDto>();
            DepartmentList = new List<DepartmentDto>();
        }

        public string Title { get; set; }
        public UserDto User { get; set; }
        public List<KeyDto> RoleList { get; set; }
        public List<DepartmentDto> DepartmentList { get; set; }
        public PageAction PageActionType { get; set; }
    }
}
