﻿using AAT.Model.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model.ViewModel
{
    public class LearnerPresentViewModel
    {
        public LearnerPresentViewModel()
        {
            Class = new ClassDto();
            Learner = new LearnerDto();
            LearnerClassDates = new List<LearnerClassDateDto>();
            CardActivating = new CardDto();
        }

        public string Learner64ImageString { get; set; }
        public ClassDto Class { get; set; }
        public LearnerDto Learner { get; set; }
        public List<LearnerClassDateDto> LearnerClassDates { get; set; }
        public CardDto CardActivating { get; set; }
       
    }
}
