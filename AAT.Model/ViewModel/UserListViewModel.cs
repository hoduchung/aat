﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model.ViewModel
{
    public class UserListViewModel
    {
        public UserListViewModel()
        {
            UserList = new List<UserDto>();
            DepartmentList = new List<DepartmentDto>();
        }

        public List<UserDto> UserList { get; set; }
        public List<DepartmentDto> DepartmentList { get; set; }
        public PageViewModel PagingModel { get; set; }

        public string SearchLoginName { get; set; }
        public string SearchName { get; set; }
        public string SearchTypeId { get; set; }
        public string SearchRoleId { get; set; }
        public string SearchStatusId { get; set; }
        public string SearchDepartmentId { get; set; }
        public string SearchPhone { get; set; }
        public string SearchEmail { get; set; }
        public string SortColumn { get; set; }
        public string SortOrder { get; set; }
        public int PageSize { get; set; }
    }
}
