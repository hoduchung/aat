﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model.ViewModel
{
    public class LearnerListViewModel
    {
        public LearnerListViewModel()
        {
            LearnerList = new List<LearnerDto>();
            PagingModel = new PageViewModel(1, 1, 0, "");
        }
        public List<LearnerDto> LearnerList { get; set; }
        public PageViewModel PagingModel { get; set; }

        public string SearchDate { get; set; }
        public string SearchStartDate { get; set; }
        public string SearchEndtDate { get; set; }

        public string SearchCourseId { get; set; }
        public string SearchCourseName { get; set; }
        public string SearchClassId { get; set; }
        public string SearchClassName { get; set; }
        public string SearchTrainerId { get; set; }
        public string SearchTrainerName { get; set; }
        public string SearchStatus { get; set; }

        public string SearchStafffId { get; set; }
        public string SearchLearnerName { get; set; }
        public string SortColumn { get; set; }
        public string SortOrder { get; set; }
        public string ErrorMessage { get; set; }
        public int PageSize { get; set; }

    }
}
