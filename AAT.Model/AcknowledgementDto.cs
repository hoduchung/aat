using System;
using System.Collections.Generic;
using System.Text;

namespace AAT_SIA.Model
{
    public class AcknowledgementDto
    {
        public int AcknowledgementId { get; set; }
        public DateTime? AcknowledgementDate { get; set; }
        public string TrainerId { get; set; }
        public string ClassId { get; set; }
        public int RowNum { get; set; }
    }
}
