using AAT.Model.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace AAT.Model
{
    public class UserDto
    {
        public UserDto()
        {
        }

        public int RowNum { get; set; }
        public int UserId { get; set; }
        public string LoginName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public UserType UserType { get; set; }
        public StatusType Status { get; set; }
        public UserRole Role { get; set; }
        public string DepartmentId { get; set; }
        public string Department { get; set; }
        public int PassIncorrectCount { get; set; }
        public string PersonalNum { get; set; }
    }
}
