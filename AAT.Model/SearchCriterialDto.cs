﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model
{
    public class SearchCriterialDto
    {
        public SearchCriterialDto()
        {
            SearchFieldList = new List<KeyDto>();
        }

        public SearchCriterialDto(short page, short pageSize)
        {
            this.Page = page;
            this.PageSize = pageSize;
            SearchFieldList = new List<KeyDto>();
        }

        public short? Page { get; set; }
        public int? PageSize { get; set; }
        //public string SearchField { get; set; }
        //public object SearchValue { get; set; }

        public List<KeyDto> SearchFieldList { get; set; }

        public string GetSearchValueString()
        {
            if (SearchFieldList == null || SearchFieldList.Count == 0)
            {
                return string.Empty;
            }

            return SearchFieldList[0].Value.ToString();
        }

        public string GetSearchFieldString()
        {
            if (SearchFieldList == null || SearchFieldList.Count == 0)
            {
                return string.Empty;
            }

            return SearchFieldList[0].Key.ToString();
        }

        //public SqlDbType SqlSeacrchType { get; set; }
    }
}
