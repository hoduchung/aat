using System;
using System.Collections.Generic;
using System.Text;

namespace AAT.Model
{
    public class AcknowledgementExtraDto
    {
        public int AcknowledgementExtraId { get; set; }
        public string ClassId { get; set; }
        public string TrainerName { get; set; }
        public DateTime? AcknowledgementDate { get; set; }
        public int RowNum { get; set; }
    }
}
