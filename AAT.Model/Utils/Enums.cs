﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model.Utils
{
    public enum StatusType
    {
        Active = 1,
        Inactive = 0,
        Locked = 2
    }

    public enum UserRole
    {
        ExternalTrainer = 1,
        Staff = 2,
        Admin = 3,
        SuperAdmin = 4
    }

    public enum UserType
    {
        Basic = 1,
        Window = 2
    }

    public enum ClassStatus
    {
        New = 1,
        OnGoing = 2,
        PendingAcknowledge = 3,
        Completed = 4
    }

    public enum PageAction
    {
        Pagging = 1,
        Search = 2,
        Attending = 3,
        CreateNew = 4,
        Update = 5,
        Sorting = 6,
        Export = 7
    }

    public enum FileExtenstion
    {
        PDF = 1, 
        WORD = 2,
        EXCEL = 3
    }
}
