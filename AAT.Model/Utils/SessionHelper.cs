﻿using AAT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAT.Model.Utils
{
    public class SessionHelper
    {
        private const string SESSION_KEY = "crrowcrestkey";

        private static AATSession GetSession()
        {
            AATSession result = (AATSession)HttpContext.Current.Session[SESSION_KEY];
            if (result == null)
            {
                result = new AATSession();
                HttpContext.Current.Session[SESSION_KEY] = result;
            }
            return result;
        }

        public static void ClearSession()
        {
            Session.User = null;
            AATSession result = (AATSession)HttpContext.Current.Session[SESSION_KEY];
            result = null;
        }

        public static AATSession Session
        {
            get
            {
                return GetSession();
            }
        }
    }

    public class AATSession
    {
        public string UserName { get; set; }
        public UserDto User { get; set; }
        public DateTime AttendingCheckingClassDate { get; set; }
        public string AttendingCheckingClassID { get; set; }

    }
}