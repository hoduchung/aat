﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model.Utils
{
    public class SHA1Helper
    {
        public static string PasswordMark = "*******";
        public static string GetPasswordHash(string strPassword)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(strPassword);
            string result;
            using (HashAlgorithm hashAlgorithm = new SHA1Managed())
            {
                result = Convert.ToBase64String(hashAlgorithm.ComputeHash(bytes));
            }
            return result;
        }
    }
}
