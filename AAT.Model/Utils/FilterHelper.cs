﻿using AAT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAT.Model.Utils
{
    public class FilterHelper
    {
        #region Singleton
        private static readonly Lazy<FilterHelper> _instance = new Lazy<FilterHelper>(() => new FilterHelper());
        public static FilterHelper Instance
        {
            get { return _instance.Value; }
        }
        public FilterHelper() { }
        #endregion Singleton

        public List<KeyDto> GetFilterUserListPage()
        {
            List<KeyDto> re = new List<KeyDto>();
            UserRole role = SessionHelper.Session.User.Role;
            switch (role)
            {
                case UserRole.ExternalTrainer:
                case UserRole.Staff:
                case UserRole.Admin:
                    re.Add(new KeyDto { Key = "DepartmentID", Value = EnumsHelper.GetDepartmentCodes(SessionHelper.Session.User.DepartmentId) });
                    break;
            }

            return re;
        }

        public List<KeyDto> GetFilterAdtendancingPage()
        {
            List<KeyDto> re = new List<KeyDto>();
            UserRole role = SessionHelper.Session.User.Role;
            switch (role)
            {
                case UserRole.ExternalTrainer:
                case UserRole.Staff:
                case UserRole.Admin:
                    re.Add(new KeyDto { Key = "DepartmentID", Value = EnumsHelper.GetDepartmentCodes(SessionHelper.Session.User.DepartmentId) });
                    break;
            }

            return re;
        }

        public List<KeyDto> GetFilterLearnerListPage()
        {
            List<KeyDto> re = new List<KeyDto>();
            UserRole role = SessionHelper.Session.User.Role;
            switch (role)
            {
                case UserRole.ExternalTrainer:
                case UserRole.Staff:
                case UserRole.Admin:
                    re.Add(new KeyDto { Key = "DepartmentID", Value = EnumsHelper.GetDepartmentCodes(SessionHelper.Session.User.DepartmentId) });
                    break;
            }

            return re;
        }


        public List<KeyDto> GetFilterClassListPage(SearchCriterialDto criterial)
        {
            List<KeyDto> re = new List<KeyDto>();
            UserRole role = SessionHelper.Session.User.Role;
            KeyDto item = null;
            switch (role)
            {
                case UserRole.ExternalTrainer:
                    //item = new KeyDto { Key = "ClassID", Value = SessionHelper.Session.User.LoginName };
                    //if (criterial != null && criterial.SearchFieldList != null)
                    //{
                    //    var existClassId = criterial.SearchFieldList.FirstOrDefault(x => x.Key.ToString().ToLower() == "classid");
                    //    if(existClassId != null)
                    //    {
                    //        item = new KeyDto { Key = "DepartmentID", Value = EnumsHelper.GetDepartmentCodes(SessionHelper.Session.User.DepartmentId) };
                    //    }
                    //}

                    item = new KeyDto { Key = "DepartmentID", Value = EnumsHelper.GetDepartmentCodes(SessionHelper.Session.User.DepartmentId) };
                    break;
                case UserRole.Staff:
                case UserRole.Admin:
                    item = new KeyDto { Key = "DepartmentID", Value = EnumsHelper.GetDepartmentCodes(SessionHelper.Session.User.DepartmentId) };
                    break;
            }

            if(item != null)
            {
                re.Add(item);
            }
            return re;
        }
    }
}