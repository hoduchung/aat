﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model.Utils
{
    public class StringHelper
    {
        public static bool IsEquals(string str1, string str2)
        {
            if(str1.Trim().ToLower() == str2.Trim().ToLower())
            {
                return true;
            }
            return false;
        }
    }
}
