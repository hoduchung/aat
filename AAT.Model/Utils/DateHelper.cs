﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model.Utils
{
    public class DateHelper
    {
        public const double MilisecondsPerday = 24 * 60 * 60 * 1000;

        public static readonly string DefaultTimeFormat = "HH:mm";
        public static readonly string DefaultDateFormat = "dd/MM/yyyy";
        public static readonly string JqueryDatePickerFormat = "dd/mm/yy";
        public static readonly string DateMonthInNameFormat = "dd MMM yyyy";
        public static readonly string DateTimeFormat = DefaultDateFormat + " HH:mm:ss";
        public static readonly string DateTimeFormatWithoutSeconds = DefaultDateFormat + " HH:mm";
        public static readonly string SkiesFormatDate = "yyyy-MM-dd";
        public static readonly string SkiesFormatTime = "HH:mm";

        public static string FormatSkiesDate(DateTime? date)
        {
            if (date == null)
            {
                return string.Empty;
            }
            return date.Value.ToString(SkiesFormatDate);
        }

        public static string FormatSkiesTime(DateTime? date)
        {
            if (date == null)
            {
                return string.Empty;
            }
            return date.Value.ToString(SkiesFormatTime);
        }

        public static string FormatDateMonthInName(DateTime? date, string format = null)
        {
            if (date == null)
            {
                return string.Empty;
            }
            if (format == null)
            {
                return date.Value.ToString(DateMonthInNameFormat);
            }
            else
            {
                return date.Value.ToString(format);
            }
        }

        public static DateTime ConvertToLocalTime(DateTime UtcTime)
        {
            return UtcTime.ToLocalTime();
        }

        public static string FormatDateMonthInNameWithTime(DateTime? date, string format = null)
        {
            if (date == null)
            {
                return string.Empty;
            }
            if (format == null)
            {
                return date.Value.ToString(DateTimeFormatWithoutSeconds);
            }
            else
            {
                return date.Value.ToString(format);
            }
        }

        public static string FormatDateWithTime(DateTime? date, string format = null)
        {
            if (date == null)
            {
                return string.Empty;
            }
            if (format == null)
            {
                return date.Value.ToString(DateTimeFormatWithoutSeconds);
            }
            else
            {
                return date.Value.ToString(format);
            }
        }

        public static DateTime? ConvertToDate(object date)
        {
            if(date != null && !String.IsNullOrEmpty(date.ToString()))
            {
                return (DateTime?)date;
            }
            return null;
        }
        public static int getTotalHours(DateTime StartDate, DateTime EndDate)
        {
            var hours = (EndDate - StartDate).Hours;
            return hours;
        }

        public static string FormatTime(DateTime? dt)
        {
            if (dt == null)
            {
                return string.Empty;
            }
            return dt.Value.ToString(DefaultTimeFormat);
        }

        public static string FormatDate(DateTime? date, string format = null)
        {
            if (date == null)
            {
                return string.Empty;
            }

            if (format == null)
            {
                return date.Value.ToString(DefaultDateFormat);
            }
            else
            {
                return date.Value.ToString(format);
            }
        }

        public static string FormatDate(DateTime date, string format = null)
        {
            if (format == null)
            {
                return date.ToString(DefaultDateFormat);
            }
            else
            {
                return date.ToString(format);
            }
        }

        public static string FormatDateTime(DateTime date, string format = null)
        {
            if (format == null)
            {
                return date.ToString(DateTimeFormat);
            }
            else
            {
                return date.ToString(format);
            }
        }

        public static DateTime ParseDateTimeWithoutSecond(string dateString, string format = null)
        {
            if (format == null)
            {
                return DateTime.ParseExact(dateString, DateTimeFormatWithoutSeconds, null);
            }
            else
            {
                return DateTime.ParseExact(dateString, format, null);
            }
        }

        public static DateTime ParseDate(string dateString, string format = null)
        {
            if (format == null)
            {
                return DateTime.ParseExact(dateString, DefaultDateFormat, null);
            }
            else
            {
                return DateTime.ParseExact(dateString, format, null);
            }
        }

        public static DateTime? ParseSkiesDate(string dateString)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(dateString))
                {
                    return null;
                }
                return DateTime.Parse(dateString);
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public static DateTime ParseDateTime(string dateString, string format = null)
        {
            if (format == null)
            {
                return DateTime.ParseExact(dateString, DateTimeFormat, null);
            }
            else
            {
                return DateTime.ParseExact(dateString, format, null);
            }
        }

    }


}
