﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model.Utils
{
    public class ConfigHelper
    {
        public static string ConnectionStr
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionStr"].ToString();
            }
        }

        public static short PageSize
        {
            get
            {
                try
                {
                    string pageSize = ConfigurationManager.AppSettings["PageSize"].ToString();
                    pageSize = String.IsNullOrWhiteSpace(pageSize) ? "30" : pageSize;
                    return Int16.Parse(pageSize);
                }
                catch(Exception ex)
                {
                    string msg = ex.Message;
                    throw ex;
                }
            }
        }

        public static string DocLearnerLateTemplex
        {
            get { return ConfigurationManager.AppSettings["DocLearnerLateTemplex"].ToString(); }
        }

        public static string ExcelLearnerLateTemplex
        {
            get { return ConfigurationManager.AppSettings["ExcelLearnerLateTemplex"].ToString(); }
        }

        public static string PdfLearnerLateTemplex
        {
            get { return ConfigurationManager.AppSettings["PdfLearnerLateTemplex"].ToString(); }
        }

        public static string PdfLearnerHistoryTemplex
        {
            get { return ConfigurationManager.AppSettings["PdfLearnerHistoryTemplex"].ToString(); }
        }

        public static string ClassPdfTemplateURL
        {
            get { return ConfigurationManager.AppSettings["PdfClassTemplateURL"].ToString(); }
        }

        public static string DocLearnerHistoryTemplex
        {
            get { return ConfigurationManager.AppSettings["DocLearnerHistoryTemplex"].ToString(); }
        }

        public static string ClassDocTemplateURL
        {
            get { return ConfigurationManager.AppSettings["DocClassTemplateURL"].ToString(); }
        }

        public static string ExcelClassTemplateURL
        {
            get { return ConfigurationManager.AppSettings["ExcelClassTemplateURL"].ToString(); }
        }

        public static string ExcelLearnerHistoryTemplex
        {
            get { return ConfigurationManager.AppSettings["ExcelLearnerHistoryTemplex"].ToString(); }
        }

        public static string ExcelLearnerTemplateURL
        {
            get { return ConfigurationManager.AppSettings["ExcelLearnerTemplateURL"].ToString(); }
        }

        public static string LearnerPdfTemplateURL
        {
            get { return ConfigurationManager.AppSettings["PdfLearnerTemplateURL"].ToString(); }
        }

        public static string LearnerDocTemplateURL
        {
            get { return ConfigurationManager.AppSettings["DocLearnerTemplateURL"].ToString(); }
        }

        public static string SkiesImportFolderPath
        {
            get { return ConfigurationManager.AppSettings["SkiesImportFolderPath"].ToString(); }
        }

        public static string SkiesExportFolderPath
        {
            get { return ConfigurationManager.AppSettings["SkiesExportFolderPath"].ToString(); }
        }

        public static string AppEviroment
        {
            get { return ConfigurationManager.AppSettings["AppEviroment"].ToString(); }
        }

    }
}
