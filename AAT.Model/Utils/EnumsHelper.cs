﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model.Utils
{
    public class EnumsHelper
    {
        public static string GetClassStatusString(int ClassStatusID)
        {
            ClassStatus status = (ClassStatus)ClassStatusID;
            switch (status)
            {
                case ClassStatus.New: return "New";
                case ClassStatus.OnGoing: return "Ongoing";
                case ClassStatus.PendingAcknowledge: return "Pending Acknowledge";
                case ClassStatus.Completed: return "Completed";
            }
            return "Expired";
        }

        public static List<KeyDto> GetUserRoleList()
        {
            List<KeyDto> re = new List<KeyDto>();

            foreach (UserRole role in Enum.GetValues(typeof(UserRole)))
            {
                if (role != UserRole.SuperAdmin)
                {
                    re.Add(new KeyDto { Key = role.GetHashCode(), Value = role.ToString() });
                }
                else if (SessionHelper.Session.User.Role == UserRole.SuperAdmin)
                {
                    re.Add(new KeyDto { Key = role.GetHashCode(), Value = role.ToString() });
                }
            }
            return re;
        }

        public static string GetDepartmentCodes(string deparmentId)
        {
            string deparmentCode = deparmentId.ToUpper();

            switch (deparmentCode)
            {
                case "CCT":
                    return "CCT";
                case "CLC":
                    return "CLC,CTC,MDC"; 
                case "FFT":
                    return "FFT,FST";
                case "ITD":
                    return "ITD";
            }

            return deparmentId;
        }

    }
}
