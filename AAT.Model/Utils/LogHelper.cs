﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model.Utils
{
    public class LogHelper
    {
        
        public static void Error(string message, Exception ex)
        {
            string err = ex.Message;

            if(ex.InnerException != null)
            {
                err = ex.InnerException.Message;
            }
            WriteMessage(true, message + "Err: " + err);
        }

        public static void Info(string message)
        {
            WriteMessage(false, message);
        }

        private static void WriteMessage(bool errror, string message)
        {
            try
            {
                string error_pre = (errror) ? "Error_" : "";
                var currentLocation = System.AppDomain.CurrentDomain.BaseDirectory;
                string time = DateTime.Now.ToString("yyyyMMdd");
                string path = currentLocation + "\\AATLogs\\";
                string filePath = path + "AAT_Log_" + error_pre + time + ".txt";
                DirectoryInfo di = Directory.CreateDirectory(path);
                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                sw.BaseStream.Seek(0, SeekOrigin.End);
                sw.WriteLine(message);
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {
            }

        }
    }
}
