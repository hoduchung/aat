﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model
{
    public class CardDto
    {
        public CardDto()
        {
            LearnerClassDateId = -1;
            StaffId = string.Empty;
            Name = string.Empty;
            Status = string.Empty;
            Base64Image = string.Empty;
            TimeIn = string.Empty;
            LearnerClassDateId = -1;
        }

        public string StaffId { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string Base64Image { get; set; }
        public string TimeIn { get; set; }
        public decimal LearnerClassDateId { get; set; }
    }
}
