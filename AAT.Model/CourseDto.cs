using System;
using System.Collections.Generic;
using System.Text;

namespace AAT.Model
{
    public class CourseDto
    {
        public string CourseId { get; set; }
        public string CourseName { get; set; }
        public object DepartmentID { get; set; }
    }
}
