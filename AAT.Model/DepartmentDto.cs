using System;
using System.Collections.Generic;
using System.Text;

namespace AAT.Model
{
    public class DepartmentDto
    {
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public Boolean IsActive { get; set; }
        public int RowNum { get; set; }
    }
}
