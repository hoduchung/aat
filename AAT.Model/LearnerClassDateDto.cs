using System;
using System.Collections.Generic;
using System.Text;

namespace AAT.Model
{
    public class LearnerClassDateDto
    {

        public LearnerClassDateDto()
        {
            Success = string.Empty;
        }

        public Decimal LearnerClassDateId { get; set; }
        public string StaffId { get; set; }
        public string LearnerName { get; set; }
        public string ClassId { get; set; }
        public string ClassName { get; set; }
        public string CourseId { get; set; }
        public string Success { get; set; }
        public string Remark { get; set; }
        public DateTime? ClassDate { get; set; }
        public DateTime? CheckInTime { get; set; }
        public string UpdateBy { get; set; }
        public int RowNum { get; set; }
    }
}
