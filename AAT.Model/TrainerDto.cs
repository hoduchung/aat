using System;
using System.Collections.Generic;
using System.Text;

namespace AAT.Model
{
    public class TrainerDto
    {
        public string TrainerId { get; set; }
        public string TrainerName { get; set; }
        public int RowNum { get; set; }
        public DateTime? AcknowledgementDate { get; set; }
    }
}
