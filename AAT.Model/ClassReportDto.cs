﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAT.Model
{
    public class ClassReportDto
    {
        public ClassReportDto()
        {
            Class = new ClassDto();
            LearnerClassDateList = new List<LearnerClassDateDto>();
        }

        public ClassDto Class { get; set; }
        public List<LearnerClassDateDto> LearnerClassDateList { get; set; }
        public int TotalRegister { get; set; }
        public int TotalSuccess { get; set; }
        public int TotalUnsuccess { get; set; }
        public int Duration { get; set; }

    }
}
