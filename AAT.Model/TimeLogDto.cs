using System;
using System.Collections.Generic;
using System.Text;

namespace AAT.Model
{
    public class TimeLogDto
    {
        public int LogTimeId { get; set; }
        public DateTime? LogTime { get; set; }
        public int AttendtionId { get; set; }
        public int AttendtionType { get; set; }
    }
}
