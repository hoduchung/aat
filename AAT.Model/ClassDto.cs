using AAT_SIA.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace AAT.Model
{
    public class ClassDto
    {
        public ClassDto()
        {
            TrainerList = new List<TrainerDto>();
            LearnerList = new List<LearnerDto>();
            AcknowledgementExtraList = new List<AcknowledgementExtraDto>();
        }

        public string ClassID { get; set; }
        public string ClassName { get; set; }
        public string Location { get; set; }
        public string CourseId { get; set; }
        public string CourseName { get; set; }
        public DateTime? ClassDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<TrainerDto> TrainerList { get; set; }
        public string Trainers { get; set; }
        public string TrainerIDs { get; set; }
        public List<LearnerDto> LearnerList { get; set; }
        public List<AcknowledgementExtraDto> AcknowledgementExtraList { get; set; }
        public int ClassStatusID { get; set; }
        public int TotalRegister { get; set; }
        public bool IsSkiesExported { get; set; }
    }
}
