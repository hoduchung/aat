using System;
using System.Collections.Generic;
using System.Text;

namespace AAT.Model
{
    public class ClassDateSummaryDto
    {
        public ClassDateSummaryDto()
        {
            Class = new ClassDto();
        }

        public ClassDto Class { get; set; }
        public DateTime? ClassDate { get; set; }
        public int TotalRegister { get; set; }
        public int Absent { get; set; }
        public int Present { get; set; }
        public int RowNum { get; set; }
    }
}
