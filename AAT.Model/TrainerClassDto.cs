using System;
using System.Collections.Generic;
using System.Text;

namespace AAT.Model
{
    public class TrainerClassDto
    {
        public Decimal TrainerClassId { get; set; }
        public string TrainerId { get; set; }
        public string ClassId { get; set; }
    }
}
