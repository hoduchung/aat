using System;
using System.Collections.Generic;
using System.Text;

namespace AAT.Model
{
    public class UserLogDto
    {
        public string UserName { get; set; }
        public DateTime? LoginDate { get; set; }
    }
}
