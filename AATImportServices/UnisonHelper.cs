﻿using AAT.Model.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace AATImportServices
{
    public class UnisonHelper
    {
        /******** Method to send event & error to Unison Alarm Text Interface *******/

        public static void SendToUnison(string alarmText)
        {
            try {
                var alarmActive = ConfigurationManager.AppSettings["AlarmActive"].ToString();
                if(alarmActive == "0")
                {
                    return;
                }
                int serverPort = int.Parse(ConfigurationManager.AppSettings["AlarmServerPort"].ToString());
                string serverIP = ConfigurationManager.AppSettings["AlarmServerIP"].ToString();
                
                //---data to send to the server---
                string textToSend = "<" + alarmText + ";>";

                //---create a TCPClient object at the IP and port no.---
                using (TcpClient client = new TcpClient(serverIP, serverPort))
                {
                    try
                    {
                        NetworkStream nwStream = client.GetStream();
                        byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(textToSend);

                        //---send the text---

                        nwStream.Write(bytesToSend, 0, bytesToSend.Length);
                        client.Close();
                    }
                    catch (Exception ex)
                    {
                        ImportHelper.LogError("Send Unison Error : " + ex.Message);
                        ImportHelper.LogService("Send Unison Error : " + ex.Message);

                        //log.Error("Error sending alarm text to Unison - " + ex.Message.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                ImportHelper.LogService(DateTime.Now.ToString() + " Send Unison Aarm Error " + ex.Message);
                ImportHelper.LogError(DateTime.Now.ToString() + " Send Unison Aarm Error " + ex.Message);
                LogHelper.Error(DateTime.Now.ToString() + " Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }



        }
    }
}
