﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace AATImportServices
{
    public partial class ImportService : ServiceBase
    {
        public static bool IsImportInProcessing = false;
        System.Timers.Timer timeDelay;
        public ImportService()
        {
            InitializeComponent();
            timeDelay = new System.Timers.Timer();
            timeDelay.Interval = GetTimerInterval();
            timeDelay.Elapsed += new System.Timers.ElapsedEventHandler(WorkProcess);
        }

        public int GetTimerInterval()
        {
            try
            {
                int min = int.Parse(ConfigurationManager.AppSettings["IntervalMin"].ToString());
                return min * 60000;
            }
            catch (Exception ex)
            {

            }
            return 10 * 60000; //10 Minute;
        }

        public void WorkProcess(object sender, System.Timers.ElapsedEventArgs e)
        {
            ImportHelper import = new ImportHelper();
            import.ImportSkiesData();
            import.DeleteOldImportedFiles();
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            ImportHelper.LogService("Service is Started");
            timeDelay.Enabled = true;

            // Call Services 1st
            ImportHelper import = new ImportHelper();
            import.ImportSkiesData();
            import.DeleteOldImportedFiles();

            string textPrefix = ConfigurationManager.AppSettings["AlarmStartPrefix"].ToString();
            UnisonHelper.SendToUnison(textPrefix);
        }


        protected override void OnStop()
        {
            ImportHelper.LogService("Service Stoped");
            timeDelay.Enabled = false;

            string textPrefix = ConfigurationManager.AppSettings["AlarmStopPrefix"].ToString();
            UnisonHelper.SendToUnison(textPrefix);
        }
    }
}
