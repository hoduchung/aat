@echo off

SET PROG=%0\..\AATImportServices.exe
SET FIRSTPART=c:\windows\microsoft.net\framework\v4.0.30319\InstallUtil.exe

:install
  ECHO Found .NET Framework  
  ECHO Installing service %PROG%
  %FIRSTPART% %PROG%

  net start "AAT Import Services"

  ECHO DONE!!!
  Pause