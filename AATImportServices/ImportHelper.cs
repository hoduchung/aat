﻿using AAT.Business;
using AAT.Model;
using AAT.Model.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AATImportServices
{
    public class ImportHelper
    {
        public static int TotalRecord = 0;
        public static int TotalSuccess = 0;
        public static int TotalFail = 0;

        public void ImportSkiesData()
        {
            try {

                //Wait for finish prerious task
                if (ImportService.IsImportInProcessing)
                {
                    return;
                }

                LogService("Import Start: " + DateTime.Now.ToString());
                ImportService.IsImportInProcessing = true;
                string skieUploadPath = ConfigHelper.SkiesImportFolderPath;
                DirectoryInfo d = new DirectoryInfo(skieUploadPath); //Assuming Test is your Folder
                FileInfo[] Files = d.GetFiles("*.csv"); //Getting Text files

                foreach (FileInfo file in Files)
                {
                    string fileName = skieUploadPath + "\\" + file.Name;

                    var contents = File.ReadAllText(fileName).Split('\n');
                    IEnumerable<string[]> csv = from line in contents select line.Split(',').ToArray();

                    //Step 1: Parse data into Class List
                    List<ClassDto> classList = ParseClassListFromCsvData(csv);
                    UpdateClassList(classList);
                }

                #region Moving File
                string skieImportedPath = ConfigurationManager.AppSettings["SkiesImportedFolderPath"].ToString();
                string skieImportedSubPath = skieImportedPath + "\\" + DateTime.Now.ToString("yyyy-MM-dd"); // your code goes here

                if (!Directory.Exists(skieImportedSubPath))
                {
                    Directory.CreateDirectory(skieImportedSubPath);
                }
               
                foreach (FileInfo file in Files)
                {
                    string moveFrom = skieUploadPath + "\\" + file.Name;
                    string newFileName = file.Name.Replace(".csv", " " + DateTime.Now.ToString("HH.mm.ss") + ".csv");
                    string moveTo = skieImportedSubPath + "\\" + newFileName;
                    File.Move(moveFrom, moveTo); // Try to move
                }
                #endregion

                ImportService.IsImportInProcessing = false;

                

            }
            catch(Exception ex)
            {
                LogHelper.Error(DateTime.Now.ToString() +  " Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void DeleteOldImportedFiles()
        {
            try
            {
                string skieImportedPath = ConfigurationManager.AppSettings["SkiesImportedFolderPath"].ToString();
                string[] importedFolders = Directory.GetDirectories(skieImportedPath);
                foreach (string folder in importedFolders)
                {

                    DirectoryInfo getfolder = new DirectoryInfo(folder);
                    if (getfolder.CreationTime.Date < DateTime.Now.AddDays(-7))
                    {
                        Directory.Delete(folder,true);
                    }
                    //FileInfo fi = new FileInfo(file);
                    //if (fi.CreationTime < DateTime.Now.AddDays(-7))
                    //    fi.Delete();
                }
            }

            catch (Exception ex)
            {
                LogHelper.Error(DateTime.Now.ToString() + " Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        private void UpdateClassList(List<ClassDto> classList)
        {
            ClassDto DbClass;
            CourseDto courseObj;

            foreach (ClassDto classObj in classList)
            {
                try
                {
                    DbClass = ClassService.Instance.GetClassDetail(classObj.ClassID);
                    //Create New Class
                    if (DbClass == null || string.IsNullOrEmpty(DbClass.ClassID))
                    {
                        courseObj = new CourseDto();
                        courseObj.CourseId = classObj.CourseId;
                        courseObj.CourseName = classObj.ClassName;
                        courseObj.DepartmentID = classObj.CourseId.Substring(0, 3);
                        CourseService.Instance.CreateNewCourse(courseObj);
                        ClassService.Instance.CreateNewClass(classObj);

                        string classStr = Newtonsoft.Json.JsonConvert.SerializeObject(classObj);

                        Console.WriteLine("Class Created:");
                        Console.WriteLine(classStr);
                    }
                    else //Update Class
                    {
                        //Update Class Name
                        if(classObj.ClassName != DbClass.ClassName || classObj.StartDate != DbClass.StartDate
                            || classObj.EndDate != DbClass.EndDate)
                        {
                            classObj.ClassStatusID = DbClass.ClassStatusID;
                            ClassService.Instance.UpdateClass(classObj);
                        }
                        //STEP1 : Remove Learner
                        List<LearnerClassDto> learnerClassRemoves = new List<LearnerClassDto>();
                        foreach (LearnerDto learner in DbClass.LearnerList)
                        {
                            LearnerDto matchObj = classObj.LearnerList.FirstOrDefault(x => x.StaffID == learner.StaffID);
                            //If new list not include learner => Learner will be removed
                            if (matchObj == null)
                            {
                                LearnerClassDto remove = new LearnerClassDto();
                                remove.ClassId = classObj.ClassID;
                                remove.StaffId = learner.StaffID;
                                learnerClassRemoves.Add(remove);
                            }
                        }
                        LearnerService.Instance.UpdateLearnerClass(learnerClassRemoves, true);

                        //STEP 2 : ADD/Update LEARNER 
                        foreach (LearnerDto learner in classObj.LearnerList)
                        {
                            LearnerDto matchObj = DbClass.LearnerList.FirstOrDefault(x => x.StaffID == learner.StaffID);

                            if (matchObj == null)
                            {
                                LearnerService.Instance.CreateNewLearner(learner);
                                LearnerClassDto learnerClass = new LearnerClassDto
                                {
                                    ClassId = classObj.ClassID,
                                    StaffId = learner.StaffID,
                                    IsRemoved = false
                                };
                                LearnerClassService.Instance.CreateNewLearnerClass(learnerClass);
                            }
                            else if(learner.LearnerName != matchObj.LearnerName)  //Update learner name
                            {
                                LearnerService.Instance.UpdateLearner(learner);
                            }
                        }

                        //STEP 3: ADD TRAINER
                        //STEP 3.1 : Remove trainer class
                        List<TrainerClassDto> removeTrainerList = new List<TrainerClassDto>();
                        foreach (TrainerDto trainer in DbClass.TrainerList)
                        {
                            TrainerDto matchObj = classObj.TrainerList.FirstOrDefault(x => x.TrainerId == trainer.TrainerId);
                            if (matchObj == null)
                            {
                                TrainerClassService.Instance.DeleteTrainerClass(classObj.ClassID, trainer.TrainerId);
                            }
                        }

                        foreach (TrainerDto trainer in classObj.TrainerList)
                        {
                            TrainerDto matchObj = DbClass.TrainerList.FirstOrDefault(x => x.TrainerId == trainer.TrainerId);

                            if (matchObj == null)
                            {
                                TrainerService.Instance.CreateNewTrainer(trainer);
                                TrainerClassDto trainerClass = new TrainerClassDto
                                {
                                    ClassId = classObj.ClassID,
                                    TrainerId = trainer.TrainerId
                                };
                                TrainerClassService.Instance.CreateNewTrainerClass(trainerClass);
                            }
                            else if (trainer.TrainerName != matchObj.TrainerName) //Update Trainner Name
                            {
                                TrainerService.Instance.UpdateTrainer(trainer);
                            }
                        }
                    } //End ELSE - Update Class
                }
                catch (Exception ex)
                {
                    LogHelper.Error(DateTime.Now.ToString() + " Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                }

               
            }
        }

        public static void LogService(string content)
        {
            try
            {
                string filePath = ConfigurationManager.AppSettings["LogFilePath"].ToString();
                string time = DateTime.Now.ToString("yyyy-MM-dd");
                filePath = filePath.Replace("{time}", time);

                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                sw.BaseStream.Seek(0, SeekOrigin.End);
                sw.WriteLine(content);
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {
            }
        }

        public static void LogError(string content)
        {
            try
            {
                content = DateTime.Now.ToString() + " " + content;
                string filePath = ConfigurationManager.AppSettings["ErrorFilePath"].ToString();
                string time = DateTime.Now.ToString("yyyyMMdd");
                filePath = filePath.Replace("{time}", time);

                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                sw.BaseStream.Seek(0, SeekOrigin.End);
                sw.WriteLine(content);
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {
            }
        }

        public string getDataError(string[] row)
        {
            if(string.IsNullOrEmpty(row[SkiesCsvIndex.ClassID]))
            {
                return "ClassID Empty";
            }

            if (string.IsNullOrEmpty(row[SkiesCsvIndex.CourseID]))
            {
                return "CourseID Empty";
            }

            if (string.IsNullOrEmpty(row[SkiesCsvIndex.StartDate]))
            {
                return "StartDate Empty";
            }

            if (string.IsNullOrEmpty(row[SkiesCsvIndex.EndDate]))
            {
                return "EndDate Empty";
            }

            DateTime tempDate;
            if(! DateTime.TryParse(row[SkiesCsvIndex.StartDate], out tempDate))
            {
                return "StartDate Invalid Format";
            }

            if (!DateTime.TryParse(row[SkiesCsvIndex.EndDate], out tempDate))
            {
                return "EndDate Invalid Format";
            }

            return string.Empty;
        }

        private List<ClassDto> ParseClassListFromCsvData(IEnumerable<string[]> csv)
        {
            List<ClassDto> classList = new List<ClassDto>();
            StringBuilder importRecordFail = new StringBuilder();

            int headerRows = 1;

            TotalRecord = 0;
            TotalSuccess = 0;
            TotalFail = 0;

            try
            {
                string rowError = string.Empty;

                foreach (var row in csv.Skip(headerRows).TakeWhile(r => r.Length > 1 && r.Last().Trim().Length > 0))
                {

                    try
                    {
                        TotalRecord++;
                        rowError = getDataError(row);

                        if (!string.IsNullOrEmpty(rowError))
                        {
                            TotalFail++;
                            importRecordFail.AppendLine(rowError + " " + String.Join(", ", row));
                            continue;
                        }

                        string classId = row[SkiesCsvIndex.ClassID].Trim();
                        ClassDto classObj = classList.FirstOrDefault(x => x.ClassID == classId);
                        if (classObj == null)
                        {
                            classObj = new ClassDto();
                            classObj.ClassID = classId;
                            classObj.CourseId = row[SkiesCsvIndex.CourseID].Trim();
                            classObj.ClassName = row[SkiesCsvIndex.ClassName].Trim();
                            classObj.Location = row[SkiesCsvIndex.Location].Trim();
                            classObj.StartDate = DateHelper.ParseSkiesDate(row[SkiesCsvIndex.StartDate].Trim());
                            classObj.EndDate = DateHelper.ParseSkiesDate(row[SkiesCsvIndex.EndDate].Trim());
                            classList.Add(classObj);
                            classObj = classList.FirstOrDefault(x => x.ClassID == classId);
                        }

                        //Trainner
                        string instructorId = string.Empty;
                        string trainerName = string.Empty;

                        for (int i = 0; i <= 2; i++)
                        {
                            if(i == 0)
                            {
                                trainerName = row[SkiesCsvIndex.Instructor1].Trim();
                                instructorId = row[SkiesCsvIndex.Instructor1_Username].Trim();
                            }
                            else if (i == 1)
                            {
                                trainerName = row[SkiesCsvIndex.Instructor2].Trim();
                                instructorId = row[SkiesCsvIndex.Instructor2_Username].Trim();
                            }
                            else
                            {
                                trainerName = row[SkiesCsvIndex.Instructor3].Trim();
                                instructorId = row[SkiesCsvIndex.Instructor3_Username].Trim();
                            }

                            //int instructorNameIndex = SkiesCsvIndex.Instructor1 + (i*2);
                            //string[] instrustor = row[SkiesCsvIndex.Instructor1 + i].Trim().Split(' ');
                            //string instructorId;
                            if (trainerName.Length > 1)
                            {
                                //instructorId = instrustor[instrustor.Length - 1];
                                TrainerDto trainer = classObj.TrainerList.FirstOrDefault(x => x.TrainerId == instructorId);
                                if (trainer == null)
                                {
                                    //Create New Trainer
                                    trainer = new TrainerDto();
                                    trainer.TrainerId = instructorId;
                                    trainer.TrainerName = trainerName;
                                    classObj.TrainerList.Add(trainer);
                                }
                            }
                        }

                        //Learner
                        string staffId = row[SkiesCsvIndex.StaffId].Trim();
                        if (!string.IsNullOrWhiteSpace(staffId))
                        {
                            LearnerDto learner = classObj.LearnerList.FirstOrDefault(x => x.StaffID == staffId);
                            if (learner == null)
                            {
                                //Create New Trainer
                                string learnerName = row[SkiesCsvIndex.Learner].Replace(staffId, "").Trim();

                                learner = new LearnerDto();
                                learner.StaffID = staffId;
                                learner.LearnerName = learnerName;
                                classObj.LearnerList.Add(learner);
                            }
                        }

                        TotalSuccess++;
                    }
                    catch (Exception ex1)
                    {
                        string inputData = Newtonsoft.Json.JsonConvert.SerializeObject(row);
                        TotalFail++;

                        importRecordFail.AppendLine(ex1.Message + " " + inputData);
                        LogHelper.Error("ERROR Method: ParseClassListFromCsvData, Input:  " + inputData, ex1);
                        continue;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(DateTime.Now.ToString() + " Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
            }
            finally
            {
                LogService(DateTime.Now.ToString() + "Total Record:" + TotalRecord);
                LogService(DateTime.Now.ToString() + "Total Success:" + TotalSuccess);
                LogService(DateTime.Now.ToString() + "Total Fail:" + TotalFail);

                if (TotalFail > 0)
                {
                    LogService("Record Fail:");
                    LogService(importRecordFail.ToString());
                    string textPrefix = ConfigurationManager.AppSettings["AlarmTextPrefix"].ToString();
                    UnisonHelper.SendToUnison(textPrefix);
                }
            }
            return classList;
        }
    }
}
