﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AATImportServices
{
    public class SkiesCsvIndex
    {
        public static int DepartMentId = 0;
        public static int DataType = 1;

        public static int CourseID = 0;
        public static int ClassID = 1;
        public static int ClassName = 2;
        public static int Location = 3;
        public static int Instructor1 = 4;
        public static int Instructor1_Username = 5;
        public static int Instructor2 = 6;
        public static int Instructor2_Username = 7;
        public static int Instructor3 = 8;
        public static int Instructor3_Username = 9;
        public static int StartDate = 10;
        public static int EndDate = 11;
        public static int ClassDuration = 12;
        public static int Learner = 13;
        public static int StaffId = 14;
    }
}
