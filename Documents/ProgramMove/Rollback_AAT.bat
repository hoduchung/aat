rem unzip the deployment package placed in the rollback folder into the rollback folder itself
powershell.exe -nologo -noprofile -command "& { $shell = New-Object -COM Shell.Application; $target = $shell.NameSpace('D:\WebSite\Rollback'); $zip = $shell.NameSpace('D:\WebSite\Rollback\AAT_Product.zip'); $target.CopyHere($zip.Items(), 16); }"
rem copy files inside Rollback folder to the deployment folder
XCOPY D:\WebSite\Rollback\AAT_Product\* D:\WebSite\AAT_Product\ /b /v /y /S 
rem delete unzipped folders
RMDIR /Q /S D:\WebSite\Rollback\AAT_Product