rem Unzip the deployment package(placed in rollforward folder) into the rollforward folder
Powershell.exe -nologo -noprofile -command "& { $shell = New-Object -COM Shell.Application; $target = $shell.NameSpace('D:\WebSite\Rollforward'); $zip = $shell.NameSpace('D:\WebSite\Rollforward\AAT_Publish.zip'); $target.CopyHere($zip.Items(), 16); }"
rem Copy the unzipped deployment package to the deployment folder
XCOPY D:\WebSite\Rollforward\AAT_Publish\* D:\WebSite\AAT_Product /b /v /y /S
rem Delete the unzipped package from Rollforward folder
RMDIR /Q /S D:\WebSite\Rollforward\AAT_Publish