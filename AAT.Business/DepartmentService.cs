using System;
using AAT.Data;
using AAT.Model;
using AAT.Model.Utils;
using System.Collections.Generic;

namespace AAT.Business
{
    public class DepartmentService
    {
        #region Singleton
        private static readonly Lazy<DepartmentService> _instance = new Lazy<DepartmentService>(() => new DepartmentService());
        public static DepartmentService Instance
        {
            get { return _instance.Value; }
        }
        public DepartmentService() { }
        #endregion Singleton

        public bool CreateNewDepartment(DepartmentDto dto)
        {
            try
            {
                DepartmentRepository dataSrv = new DepartmentRepository();
                return dataSrv.CreateNewDepartment(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateDepartment(DepartmentDto dto, string oldDepartmentId)
        {
            try
            {
                DepartmentRepository dataSrv = new DepartmentRepository();
                return dataSrv.UpdateDepartment(dto, oldDepartmentId);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public int GetNumberDepartmentCourse(string departmentId)
        {
            try
            {
                DepartmentRepository dataSrv = new DepartmentRepository();
                return dataSrv.GetNumberDepartmentCourse(departmentId);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }
        public List<DepartmentDto> GetDepartmentList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                DepartmentRepository dataSrv = new DepartmentRepository();
                List<DepartmentDto> re = dataSrv.GetDepartmentList(criterial, ref totalRecord);

                if (re == null)
                {
                    re = new List<DepartmentDto>();
                }

                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }


    }
}
