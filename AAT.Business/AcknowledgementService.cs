using AAT.Model;
using AAT.Model.Utils;
using AAT_SIA.Data;
using AAT_SIA.Model;
using System;
using System.Collections.Generic;

namespace AAT_SIA.Business
{
    public class AcknowledgementService
    {
        #region Singleton
        private static readonly Lazy<AcknowledgementService> _instance = new Lazy<AcknowledgementService>(() => new AcknowledgementService());
        public static AcknowledgementService Instance
        {
            get { return _instance.Value; }
        }
        public AcknowledgementService() { }
        #endregion Singleton

        public int CreateNewAcknowledgement(string classId, string TrainerId)
        {
            try
            {
                AcknowledgementRepository dataSrv = new AcknowledgementRepository();
                return dataSrv.CreateNewAcknowledgement(classId, TrainerId);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateAcknowledgement(AcknowledgementDto dto)
        {
            try
            {
                AcknowledgementRepository dataSrv = new AcknowledgementRepository();
                return dataSrv.UpdateAcknowledgement(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<AcknowledgementDto> GetAcknowledgementList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                AcknowledgementRepository dataSrv = new AcknowledgementRepository();
                List<AcknowledgementDto> re = dataSrv.GetAcknowledgementList(criterial, ref totalRecord);

                if (re == null)
                {
                    re = new List<AcknowledgementDto>();
                }

                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }


    }
}
