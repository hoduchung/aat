using System;
using AAT.Data;
using AAT.Model;
using AAT.Model.Utils;
using System.Collections.Generic;

namespace AAT.Business
{
    public class LearnerClassService
    {
        #region Singleton
        private static readonly Lazy<LearnerClassService> _instance = new Lazy<LearnerClassService>(() => new LearnerClassService());
        public static LearnerClassService Instance
        {
            get { return _instance.Value; }
        }
        public LearnerClassService() { }
        #endregion Singleton

        public bool CreateNewLearnerClass(LearnerClassDto dto)
        {
            try
            {
                LearnerClassRepository dataSrv = new LearnerClassRepository();
                return dataSrv.CreateNewLearnerClass(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool IsValidLearner(string classId, string staffId)
        {
            try
            {
                LearnerClassRepository dataSrv = new LearnerClassRepository();
                return dataSrv.IsValidLearner(classId,staffId);

            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateLearnerClass(LearnerClassDto dto)
        {
            try
            {
                LearnerClassRepository dataSrv = new LearnerClassRepository();
                return dataSrv.UpdateLearnerClass(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }


    }
}
