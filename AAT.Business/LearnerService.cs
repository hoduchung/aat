using System;
using AAT.Data;
using AAT.Model;
using AAT.Model.Utils;
using System.Collections.Generic;
using AAT.Model.ViewModel;
using System.Data;
using System.Linq;

namespace AAT.Business
{
    public class LearnerService
    {
        #region Singleton
        private static readonly Lazy<LearnerService> _instance = new Lazy<LearnerService>(() => new LearnerService());
        public static LearnerService Instance
        {
            get { return _instance.Value; }
        }
        public LearnerService() { }
        #endregion Singleton

        public int CreateNewLearner(LearnerDto dto)
        {
            try
            {
                LearnerRepository dataSrv = new LearnerRepository();
                return dataSrv.CreateNewLearner(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateLearner(LearnerDto dto)
        {
            try
            {
                LearnerRepository dataSrv = new LearnerRepository();
                return dataSrv.UpdateLearner(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public LearnerDto GetLearnerByStaffID(string staffID)
        {
            try
            {
                LearnerRepository dataSrv = new LearnerRepository();
                return dataSrv.GetLearnerByStaffID(staffID);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<LearnerDto> GetLearnerHistory(SearchCriterialDto criterial)
        {
            try
            {
                LearnerRepository dataSrv = new LearnerRepository();
                List<LearnerDto> re = dataSrv.GetLearnerHistory(criterial);

                if (re == null)
                {
                    re = new List<LearnerDto>();
                }

                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }


        public List<LearnerDto> LearnerGetLateReport(SearchCriterialDto criterial)
        {
            try
            {
                LearnerRepository dataSrv = new LearnerRepository();
                List<LearnerDto> re = dataSrv.LearnerGetLateReport(criterial);

                if (re == null)
                {
                    re = new List<LearnerDto>();
                }

                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<LearnerDto> GetLearnerList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                LearnerRepository dataSrv = new LearnerRepository();
                List<LearnerDto> re = dataSrv.GetLearnerList(criterial, ref totalRecord);

                if (re == null)
                {
                    re = new List<LearnerDto>();
                }

                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public void UpdateLearnerClass(List<LearnerClassDto> learnerClasses, bool isRemove)
        {
            LearnerClassRepository srv = new LearnerClassRepository();
            foreach (LearnerClassDto dto in learnerClasses)
            {
                dto.IsRemoved = isRemove;
                try
                {
                    srv.UpdateLearnerClass(dto);
                }
                catch (Exception ex)
                {
                    var inputObj = Newtonsoft.Json.JsonConvert.SerializeObject(dto);
                    LogHelper.Error("UpdateLearnerClass ERROR, Input: " + inputObj, ex);
                }
            }
        }

        public bool UpdateLearnerClassSuccessfulStatus(LearnerClassDto dto)
        {
            try
            {
                LearnerClassRepository dataSrv = new LearnerClassRepository();
                return dataSrv.UpdateLearnerClassSuccessfulStatus(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public LearnerPresentViewModel GetLearnerPresentViewModel(string classId, string staffId)
        {
            LearnerPresentViewModel re = new LearnerPresentViewModel();
            ClassRepository classSrv = new ClassRepository();
            TrainerRepository trainerSrv = new TrainerRepository();
            LearnerRepository learnerSrv = new LearnerRepository();
            LearnerClassDateRepository learnerClassDateSrv = new LearnerClassDateRepository();
            AcknowledgementExtraRepository ackExtraSrv = new AcknowledgementExtraRepository();

            DataSet ds = learnerClassDateSrv.GetLearnerClassPresents(classId, staffId);

            if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {
                //S1: Class Detail
                re.Class = classSrv.ConvertDataTableToClass(ds.Tables[0])[0];

                //S2: Trainer List
                if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                {
                    re.Class.TrainerList = trainerSrv.ConvertDataTableToTrainer(ds.Tables[1]);
                }

                //S3: Learner 
                if (ds.Tables[2] != null && ds.Tables[2].Rows.Count > 0)
                {
                    re.Learner = learnerSrv.ConvertDataTableToLearner(ds.Tables[2])[0];
                }

                //S4: Learner Presents
                if (ds.Tables[3] != null && ds.Tables[3].Rows.Count > 0)
                {
                    re.LearnerClassDates = learnerClassDateSrv.ConvertDataTableToLearnerClassDate(ds.Tables[3]);
                }

                //S5: Acknowledgement Extra
                if (ds.Tables[4] != null && ds.Tables[4].Rows.Count > 0)
                {
                    re.Class.AcknowledgementExtraList = ackExtraSrv.ConvertDataTableToAcknowledgementExtra(ds.Tables[4]);
                }
            }

            return re;
        }
    }
}
