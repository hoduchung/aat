using System;
using AAT.Data;
using AAT.Model;
using AAT.Model.Utils;
using System.Collections.Generic;

namespace AAT.Business
{
    public class UserService
    {
        #region Singleton
        private static readonly Lazy<UserService> _instance = new Lazy<UserService>(() => new UserService());
        public static UserService Instance
        {
            get { return _instance.Value; }
        }
        public UserService() { }
        #endregion Singleton

        public int CreateNewUser(UserDto dto)
        {
            try
            {
                UserRepository dataSrv = new UserRepository();
                return dataSrv.CreateNewUser(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public UserDto GetUserLogin(string loginName)
        {
            try
            {
                UserRepository dataSrv = new UserRepository();
                return dataSrv.GetUserLogin(loginName);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateUser(UserDto dto)
        {
            try
            {
                UserRepository dataSrv = new UserRepository();
                return dataSrv.UpdateUser(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<UserDto> GetUserList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                UserRepository dataSrv = new UserRepository();
                List<UserDto> re = dataSrv.GetUserList(criterial, ref totalRecord);

                if (re == null)
                {
                    re = new List<UserDto>();
                }

                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }


    }
}
