using System;
using AAT.Data;
using AAT.Model;
using AAT.Model.Utils;
using System.Collections.Generic;

namespace AAT.Business
{
    public class TrainerClassService
    {
        #region Singleton
        private static readonly Lazy<TrainerClassService> _instance = new Lazy<TrainerClassService>(() => new TrainerClassService());
        public static TrainerClassService Instance
        {
            get { return _instance.Value; }
        }
        public TrainerClassService() { }
        #endregion Singleton

        public bool CreateNewTrainerClass(TrainerClassDto dto)
        {
            try
            {
                TrainerClassRepository dataSrv = new TrainerClassRepository();
                return dataSrv.CreateNewTrainerClass(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool DeleteTrainerClass(string ClassId, string trainerId)
        {
            try
            {
                TrainerClassRepository dataSrv = new TrainerClassRepository();
                return dataSrv.DeleteTrainerClass(ClassId, trainerId);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateTrainerClass(TrainerClassDto dto)
        {
            try
            {
                TrainerClassRepository dataSrv = new TrainerClassRepository();
                return dataSrv.UpdateTrainerClass(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

    }
}
