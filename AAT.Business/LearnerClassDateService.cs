using System;
using AAT.Data;
using AAT.Model;
using AAT.Model.Utils;
using System.Collections.Generic;
using AAT.Model.ViewModel;
using System.Data;
using System.Linq;

namespace AAT.Business
{
    public class LearnerClassDateService
    {
        #region Singleton
        private static readonly Lazy<LearnerClassDateService> _instance = new Lazy<LearnerClassDateService>(() => new LearnerClassDateService());
        public static LearnerClassDateService Instance
        {
            get { return _instance.Value; }
        }
        public LearnerClassDateService() { }
        #endregion Singleton


        public ClassViewModel GetClassViewDetailByDate(string classId, DateTime classDate)
        {
            try
            {
                ClassViewModel re = new ClassViewModel();
                ClassRepository classSrv = new ClassRepository();
                TrainerRepository trainerSrv = new TrainerRepository();
                LearnerRepository learnerSrv = new LearnerRepository();
                AcknowledgementExtraRepository ackExtraSrv = new AcknowledgementExtraRepository();

                DataSet ds = classSrv.GetClassDetailByDate(classId, classDate);

                if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    //S1: Class Detail
                    re.Class = classSrv.ConvertDataTableToClass(ds.Tables[0])[0];

                    //S2: Trainer List
                    if(ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                    {
                        re.Class.TrainerList = trainerSrv.ConvertDataTableToTrainer(ds.Tables[1]);
                    }
                    else
                    {
                        re.Class.TrainerList = new List<TrainerDto>();
                    }

                    //S3: Learner List
                    if (ds.Tables[2] != null && ds.Tables[2].Rows.Count > 0)
                    {
                        re.Class.LearnerList = learnerSrv.ConvertDataTableToLearner(ds.Tables[2]);
                    }
                    else
                    {
                        re.Class.LearnerList = new List<LearnerDto>();
                    }

                    //S4: Learner Present
                    if (ds.Tables[3] != null && ds.Tables[3].Rows.Count > 0)
                    {
                        LearnerClassDateRepository learnerClassDateSrv = new LearnerClassDateRepository();
                        re.LearnerPresent = learnerClassDateSrv.ConvertDataTableToLearnerClassDate(ds.Tables[3]);
                        //re.LearnerPresent = new List<LearnerDto>();

                        //foreach (LearnerClassDateDto present in learnerClass)
                        //{
                        //    LearnerDto leaner = re.Class.LearnerList.FirstOrDefault(x => x.StaffID == present.StaffId);

                        //    if(leaner!= null)
                        //    {
                        //        leaner.CheckInTime = DateHelper.ConvertToLocalTime( present.CheckInTime.Value);
                        //    }

                        //    re.LearnerPresent.Add(leaner);
                        //}

                        var staffIdPresents = re.LearnerPresent.Select(x=>x.StaffId);
                        //Learner Not Present
                        re.LearnerNotPresent = re.Class.LearnerList.Where(x => !staffIdPresents.Contains(x.StaffID)).ToList();
                    }
                    else
                    {
                        re.LearnerPresent = new List<LearnerClassDateDto>();
                        re.LearnerNotPresent = re.Class.LearnerList;
                    }

                    if (ds.Tables[4] != null && ds.Tables[4].Rows.Count > 0)
                    {
                        re.Class.AcknowledgementExtraList = ackExtraSrv.ConvertDataTableToAcknowledgementExtra(ds.Tables[4]);
                    }
                }

                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }

        }
        public decimal CreateNewLearnerClassDate(LearnerClassDateDto dto)
        {
            try
            {
                LearnerClassDateRepository dataSrv = new LearnerClassDateRepository();
                return dataSrv.CreateNewLearnerClassDate(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateLearnerClassDate(LearnerClassDateDto dto)
        {
            try
            {
                LearnerClassDateRepository dataSrv = new LearnerClassDateRepository();
                return dataSrv.UpdateLearnerClassDate(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateSkiesExported(List<decimal> learnerDates)
        {
            try
            {
                string learnerClassDateIds = string.Join(",", learnerDates); 
                LearnerClassDateRepository dataSrv = new LearnerClassDateRepository();
                return dataSrv.UpdateSkiesExported(learnerClassDateIds);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<LearnerClassDateDto> GetSkiesReport()
        {
            try
            {
                LearnerClassDateRepository dataSrv = new LearnerClassDateRepository();
                return dataSrv.GetSkiesReport();
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<LearnerClassDateDto> GetLearnerClassDateList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                LearnerClassDateRepository dataSrv = new LearnerClassDateRepository();
                List<LearnerClassDateDto> re = dataSrv.GetLearnerClassDateList(criterial, ref totalRecord);

                if (re == null)
                {
                    re = new List<LearnerClassDateDto>();
                }

                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

    }
}
