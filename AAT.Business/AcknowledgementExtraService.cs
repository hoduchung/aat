using System;
using AAT.Data;
using AAT.Model;
using System.Collections.Generic;
using AAT.Model.Utils;
using AAT.Model;

namespace AAT.Business
{
    public class AcknowledgementExtraService
    {
        #region Singleton
        private static readonly Lazy<AcknowledgementExtraService> _instance = new Lazy<AcknowledgementExtraService>(() => new AcknowledgementExtraService());
        public static AcknowledgementExtraService Instance
        {
            get { return _instance.Value; }
        }
        public AcknowledgementExtraService() { }
        #endregion Singleton

        public int CreateNewAcknowledgementExtra(AcknowledgementExtraDto dto)
        {
            try
            {
                AcknowledgementExtraRepository dataSrv = new AcknowledgementExtraRepository();
                return dataSrv.CreateNewAcknowledgementExtra(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<AcknowledgementExtraDto> GetAcknowledgementExtraList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                AcknowledgementExtraRepository dataSrv = new AcknowledgementExtraRepository();
                List<AcknowledgementExtraDto> re = dataSrv.GetAcknowledgementExtraList(criterial, ref totalRecord);

                if (re == null)
                {
                    re = new List<AcknowledgementExtraDto>();
                }

                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateAcknowledgementExtra(int AcknowledgementExtraId, string TrainerName)
        {
            try
            {
                AcknowledgementExtraRepository dataSrv = new AcknowledgementExtraRepository();
                return dataSrv.UpdateAcknowledgementExtra( AcknowledgementExtraId,  TrainerName);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }


    }
}
