using System;
using AAT.Data;
using AAT.Model;
using AAT.Model.Utils;
using System.Collections.Generic;

namespace AAT.Business
{
    public class CourseService
    {
        #region Singleton
        private static readonly Lazy<CourseService> _instance = new Lazy<CourseService>(() => new CourseService());
        public static CourseService Instance
        {
            get { return _instance.Value; }
        }
        public CourseService() { }
        #endregion Singleton

        public int CreateNewCourse(CourseDto dto)
        {
            try
            {
                CourseRepository dataSrv = new CourseRepository();
                return dataSrv.CreateNewCourse(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateCourse(CourseDto dto)
        {
            try
            {
                CourseRepository dataSrv = new CourseRepository();
                return dataSrv.UpdateCourse(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

    }
}
