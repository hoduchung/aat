using System;
using AAT.Data;
using AAT.Model;
using AAT.Model.Utils;
using System.Collections.Generic;
using AAT.Model.ViewModel;
using System.Data;
using System.Linq;

namespace AAT.Business
{
    public class ClassService
    {
        #region Singleton
        private static readonly Lazy<ClassService> _instance = new Lazy<ClassService>(() => new ClassService());
        public static ClassService Instance
        {
            get { return _instance.Value; }
        }
        public ClassService() { }
        #endregion Singleton

        public ClassTimingViewModel GetClassTimingViewModel(string classId)
        {
            ClassTimingViewModel re = new ClassTimingViewModel();
            ClassRepository classSrv = new ClassRepository();
            TrainerRepository trainerSrv = new TrainerRepository();
            LearnerRepository learnerSrv = new LearnerRepository();
            LearnerClassDateRepository learnerClassDateSrv = new LearnerClassDateRepository();
            AcknowledgementExtraRepository ackExtraSrv = new AcknowledgementExtraRepository();

            DataSet ds = classSrv.GetClassTimingDeail(classId);

            if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {
                //S1: Class Detail
                re.Class = classSrv.ConvertDataTableToClass(ds.Tables[0])[0];

                //S2: Trainer List
                if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                {
                    re.Class.TrainerList = trainerSrv.ConvertDataTableToTrainer(ds.Tables[1]);
                }
                else
                {
                    re.Class.TrainerList = new List<TrainerDto>();
                }

                //S3: Learner List
                if (ds.Tables[2] != null && ds.Tables[2].Rows.Count > 0)
                {
                    re.Class.LearnerList = learnerSrv.ConvertDataTableToLearner(ds.Tables[2]);
                }
                else
                {
                    re.Class.LearnerList = new List<LearnerDto>();
                }

                //S4: Class Date Summary
                if (ds.Tables[3] != null && ds.Tables[3].Rows.Count > 0)
                {
                    int totalRegister = re.Class.LearnerList.Count;

                    re.LearnerClassDateSummaries =
                        learnerClassDateSrv.ConvertTableToClassDateSummary(ds.Tables[3], classId, totalRegister);
                }
                else
                {
                    re.LearnerClassDateSummaries = new List<ClassDateSummaryDto>();
                }

                //S5: Extra Acknowlegement
                if (ds.Tables[4] != null && ds.Tables[4].Rows.Count > 0)
                {
                    re.Class.AcknowledgementExtraList = ackExtraSrv.ConvertDataTableToAcknowledgementExtra(ds.Tables[4]);
                }

            }


            return re;
        }
        public bool CreateNewClass(ClassDto dto)
        {
            try
            {
                ClassRepository dataSrv = new ClassRepository();
                TrainerRepository trainerData = new TrainerRepository();
                LearnerRepository learnerData = new LearnerRepository();
                TrainerClassRepository trainerClassData = new TrainerClassRepository();
                LearnerClassRepository learnerClassData = new LearnerClassRepository();

                dataSrv.CreateNewClass(dto);

                //Trainer
                TrainerClassDto trainerClassObj;
                foreach (TrainerDto trainer in dto.TrainerList)
                {
                    trainerData.CreateNewTrainer(trainer);

                    trainerClassObj = new TrainerClassDto();
                    trainerClassObj.ClassId = dto.ClassID;
                    trainerClassObj.TrainerId = trainer.TrainerId;
                    trainerClassData.CreateNewTrainerClass(trainerClassObj);
                }

                //Learner
                LearnerClassDto learnerClassObj;
                foreach (LearnerDto learner in dto.LearnerList)
                {
                    learnerData.CreateNewLearner(learner);

                    learnerClassObj = new LearnerClassDto();
                    learnerClassObj.ClassId = dto.ClassID;
                    learnerClassObj.StaffId = learner.StaffID;
                    learnerClassObj.IsRemoved = false;
                    learnerClassData.CreateNewLearnerClass(learnerClassObj);
                }

                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }



        public bool UpdateSkiesExported(List<string> ClassId)
        {
            try
            {
                string ClassIds = string.Join(",", ClassId);
                ClassRepository dataSrv = new ClassRepository();
                return dataSrv.UpdateClascsSkiesExported(ClassIds);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateClass(ClassDto dto)
        {
            try
            {
                ClassRepository dataSrv = new ClassRepository();
                return dataSrv.UpdateClass(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public ClassDto GetClassDetail(string classId)
        {
            ClassRepository dataSrv = new ClassRepository();
            return dataSrv.GetClassDetail(classId);
        }

        public List<ClassDateSummaryDto> GetClassDateSumList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                ClassRepository dataSrv = new ClassRepository();
                List<ClassDateSummaryDto> re = dataSrv.GetClassDateSumList(criterial, ref totalRecord);

                if (re == null)
                {
                    re = new List<ClassDateSummaryDto>();
                }

                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public ClassDto GetClassByClassId(string ClassID)
        {
            try
            {
                ClassRepository dataSrv = new ClassRepository();
                return dataSrv.GetClassByClassId(ClassID);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<ClassDto> GetClassList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                ClassRepository dataSrv = new ClassRepository();
                List<ClassDto> re = dataSrv.GetClassList(criterial, ref totalRecord);

                if (re == null)
                {
                    re = new List<ClassDto>();
                }

                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public ClassReportDto GetClassReport(string classId)
        {
            ClassReportDto re = new ClassReportDto();

            ClassRepository classSrv = new ClassRepository();
            TrainerRepository trainerSrv = new TrainerRepository();
            LearnerRepository learnerSrv = new LearnerRepository();
            LearnerClassDateRepository learnerClassDateSrv = new LearnerClassDateRepository();
            AcknowledgementExtraRepository ackExtraSrv = new AcknowledgementExtraRepository();

            DataSet ds = classSrv.GetClassReport(classId);

            if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {
                //S1: Class Detail
                re.Class = classSrv.ConvertDataTableToClass(ds.Tables[0])[0];

                //S2: Trainer List
                if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                {
                    re.Class.TrainerList = trainerSrv.ConvertDataTableToTrainer(ds.Tables[1]);
                }

                //S3: Learner List
                if (ds.Tables[2] != null && ds.Tables[2].Rows.Count > 0)
                {
                    re.LearnerClassDateList = learnerClassDateSrv.ConvertDataTableToLearnerClassDate(ds.Tables[2]);

                    //Duration, Total Success, Unsuccess
                    re.Duration = (from obj in re.LearnerClassDateList.Where(x => x.ClassDate != null)
                                   select DateHelper.FormatDateMonthInName(obj.ClassDate)).Distinct().Count();

                   
                }

                //Acknowledgement Extra
                if (ds.Tables[3] != null && ds.Tables[3].Rows.Count > 0)
                {
                    re.Class.AcknowledgementExtraList = ackExtraSrv.ConvertDataTableToAcknowledgementExtra(ds.Tables[3]);
                }

                //Total Register, Success, Unsuccess
                DataTable tbTotal = ds.Tables[4];
                if (tbTotal != null && tbTotal.Rows.Count > 0)
                {
                    re.TotalRegister = tbTotal.Rows.Count; //re.Class.TotalRegister;
                    re.TotalSuccess = tbTotal.AsEnumerable().Where(x => x.Field<string>("Success") == "Y").Count();
                    re.TotalUnsuccess = re.TotalRegister - re.TotalSuccess;

                    //ds.Tables[4].Rows.
                    //List<string> leanerStaffIds = (from item in re.LearnerClassDateList
                    //                               select item.StaffId).Distinct().ToList();

                    //int attendingCount;
                    //foreach (string staffId in leanerStaffIds)
                    //{
                    //    attendingCount = re.LearnerClassDateList.Where(x => x.StaffId == staffId && x.CheckInTime != null)
                    //        .Select(y => y.CheckInTime).Distinct().Count();
                    //    if (re.Duration > 0 && attendingCount >= re.Duration)
                    //    {
                    //        re.TotalSuccess++;
                    //    }
                    //    else
                    //    {
                    //        re.TotalUnsuccess++;
                    //    }
                    //}
                }
            }
            return re;
        }
    }
}
