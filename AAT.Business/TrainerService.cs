using System;
using AAT.Data;
using AAT.Model;
using AAT.Model.Utils;
using System.Collections.Generic;

namespace AAT.Business
{
    public class TrainerService
    {
        #region Singleton
        private static readonly Lazy<TrainerService> _instance = new Lazy<TrainerService>(() => new TrainerService());
        public static TrainerService Instance
        {
            get { return _instance.Value; }
        }
        public TrainerService() { }
        #endregion Singleton

        public int CreateNewTrainer(TrainerDto dto)
        {
            try
            {
                TrainerRepository dataSrv = new TrainerRepository();
                return dataSrv.CreateNewTrainer(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public bool UpdateTrainer(TrainerDto dto)
        {
            try
            {
                TrainerRepository dataSrv = new TrainerRepository();
                return dataSrv.UpdateTrainer(dto);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public List<TrainerDto> GetTrainerList(SearchCriterialDto criterial, ref int totalRecord)
        {
            try
            {
                TrainerRepository dataSrv = new TrainerRepository();
                List<TrainerDto> re = dataSrv.GetTrainerList(criterial, ref totalRecord);

                if (re == null)
                {
                    re = new List<TrainerDto>();
                }

                return re;
            }
            catch (Exception ex)
            {
                LogHelper.Error("Method: " + System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }


    }
}
